import 'dart:convert';
import 'dart:math';

bool esNumero ( String s) {

  if ( s.isEmpty) return false;
  final n = num.tryParse(s); //pregunto si se puedo parsear un número

  return ( n == null ) ? false : true;
}

String getUID ([int length = 16]) {
  final Random _random = Random.secure();
  var values = List<int>.generate(length, (i) => _random.nextInt(256));
  return base64Url.encode(values);
}