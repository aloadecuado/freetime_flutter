import 'package:flutter/cupertino.dart';

class Localization{
  final String locale;
  final String Ambient = "Dev";

  Localization(this.locale);

  static Map<String, Map<String, String>> _localizedValues = {
    "en":{},
    "es":{
      "beforeCall":"Quizas más tarde",
    }
  };



  String getString(String key){
    return ((_localizedValues[locale][key]) != null)?_localizedValues[locale][key]:"";
  }


}