import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:free_time/src/Repository/Repository/user_auth_respository.dart';
import 'package:free_time/src/ui/pages/home_page.dart';
import 'package:free_time/src/ui/pages/login_page.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http_parser/http_parser.dart';

import 'package:mime_type/mime_type.dart';
import 'package:http/http.dart' as http;

import '../config.dart';

enum Status { Uninitialized, Authenticated, Authenticating, Unauthenticated }

// https://free-time-98d04.firebaseapp.com/__/auth/handler

class UserProvider with ChangeNotifier {
  FirebaseAuth _auth;
  FirebaseUser _user;
  GoogleSignIn _googleSignIn;
  Status _status = Status.Uninitialized;
  Firestore _firestore = Firestore.instance;

  UserProvider.instance()
      : _auth = FirebaseAuth.instance,
        _googleSignIn = GoogleSignIn() {
    _auth.onAuthStateChanged.listen(_onStateChanged);
  }

  Status get status => _status;
  FirebaseUser get user => _user;

  Future<void> signInAnonymously(context) async {
    try {
      await FirebaseAuth.instance.signInAnonymously();
      changeScreen(context, HomePage());
    } catch (e) {
      print(e);
    }
  }

  Future<bool> signIn(
      String email, String password, BuildContext context) async {
    try {
      _status = Status.Authenticating;
      notifyListeners();
      final result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      if (result.user.email != null) {
        changeScreenReplacement(
            context,
            HomePage(
              user: result.user,
            ));
      }
      return true;
    } catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      print(e.toString());
      return false;
    }
  }

  Future<bool> signInWithGoogle(BuildContext context) async {
    try {
      _status = Status.Authenticating;
      notifyListeners();
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      var result = await _auth.signInWithCredential(credential);
      await _firestore.collection('users').document(result.user.uid).setData({
        'phone_validated': false,
        'name': result.user.displayName,
        'userName': result.user.displayName,
        'email': result.user.email,
        'newUser': true
      }, merge: true);
      changeScreenReplacement(context, HomePage());
      return true;
    } catch (e) {
      print('erorzazo');
      print(e);
      _status = Status.Unauthenticated;
      notifyListeners();
      return false;
    }
  }

  Future<bool> signInWithFacebook(BuildContext context) async {
    try {
      UserAuthRepository userAuthRepository = UserAuthRepository();
      FirebaseUser user = await userAuthRepository.signFicabook();
      await _firestore.collection('users').document(user.uid).setData({
        'phone_validated': false,
        'name': user.displayName,
        'userName': user.displayName,
        'email': user.email,
        'newUser': true
      }, merge: true);
      changeScreenReplacement(context, HomePage());
      return true;
    } catch (e) {
      print('erorzazo');
      print(e);
      _status = Status.Unauthenticated;
      notifyListeners();
      return false;
    }
  }

  Future<bool> signUp(
      String name,
      String userName,
      String identification,
      String email,
      String phone,
      String contry,
      String city,
      String password,
      String docType) async {
    try {
      _status = Status.Authenticating;
      notifyListeners();
      await _auth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((user) {
        _firestore.collection('users').document(user.user.uid).setData({
          'phone_validated': false,
          'name': name,
          'userName': userName,
          'identification': identification,
          'email': email,
          'phone': phone,
          'contry': contry,
          'city': city,
          'password': password,
          "type": docType,
          'newUser': true
        });
      });
      return true;
    } catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      print(e.toString());
      return false;
    }
  }

  Future signOut(BuildContext context) async {
    _auth.signOut();
    _googleSignIn.signOut();
    _status = Status.Unauthenticated;
    notifyListeners();
    changeScreenReplacement(context, LoginPage());
  }

  Future<void> _onStateChanged(FirebaseUser user) async {
    if (user == null) {
      _status = Status.Unauthenticated;
    } else {
      _user = user;
      _status = Status.Authenticated;
    }
    notifyListeners();
  }

  Future<String> subirImagen(File imagen) async {
    final url = Uri.parse(
        'https://res.cloudinary.com/dxo7crntw/image/upload/v1572780102/mjfsnk7kvwig88csasuk.jpg');
    final mimeType = mime(imagen.path).split('/');

    final imageUploadRequest = http.MultipartRequest('POST', url);
    final file = await http.MultipartFile.fromPath('file', imagen.path,
        contentType: MediaType(mimeType[0], mimeType[1]));
    imageUploadRequest.files.add(file);

    final streamResponse = await imageUploadRequest.send();
    final resp =
        await http.Response.fromStream(streamResponse); //convierte en htttp

    if (resp.statusCode != 200 && resp.statusCode != 201) {
      print(resp.body);
      return null;
    }

    final respData = json.decode(resp.body);
    print(respData);
    return respData['secure_url'];
  }

  /*
   * Validate phone number
   */
  Future<void> sendCodeToPhoneNumber(number, verificationCompleted,
      verificationFailed, codeSent, codeAutoRetrievalTimeout) async {
    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: number,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }
}
