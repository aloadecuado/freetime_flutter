
class UserModel {
  String name;
  String username;
  // List<String> identificationType;
  String identification;
  String email;
  String phone;
  String password;
  String image;

  UserModel({
    this.name,
    this.username,
    //  this.identificationType,
    this.identification,
    this.email,
    this.phone,
    this.password,
    this.image,
  });

  factory UserModel.fromMap(Map<String, dynamic> json) => UserModel(
        name: json["name"],
        username: json["username"],
        // identificationType:
        //     List<String>.from(json["identification_type"].map((x) => x)),
        identification: json["identification"],
        email: json["email"],
        phone: json["phone"],
        password: json["password"],
        image: json['image'],
      );

  Map<String, dynamic> toMap() => {
        "name": name,
        "username": username,
        // "identification_type":
        // List<dynamic>.from(identificationType.map((x) => x)),
        "identification": identification,
        "email": email,
        "phone": phone,
        "password": password,
        "image": image,
      };


      getImg(){

        if ( image == null){

          return 'https://via.placeholder.com/111x111';
        }else {
          return 'http//imagen.api.com/imagen/$image';
        }
       
      }
}