import 'SendEmailPanicResponse.dart';

class GeneralObjectResponse{
  String status = "";
  String message = "";
  SendEmailPanicResponse sendEmailPanicResponse;

  GeneralObjectResponse(Map<String,dynamic>json){
    status = (json["status"] != null)?json["status"]:"";
    message = (json["message"] != null)?json["message"]:"";
    sendEmailPanicResponse = SendEmailPanicResponse((json["data"] != null)?json["data"]:{"":""});//(json["message"] != null)?json["message"]:"";
  }
}