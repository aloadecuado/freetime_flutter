
extension on Map<String, dynamic>{

  String getString(key){

    if (this[key] != null){
      if (this[key] is String){
        return this[key];
      }
    }
    return "";
  }
}

class BannerModel {
  String urlImage = "";
  String urlSite = "";

  BannerModel(Map<String, dynamic> json){

    this.urlImage = json.getString("urlImage");
    this.urlSite = json.getString("urlSite");
  }
}




