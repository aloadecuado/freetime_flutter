import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:free_time/src/models/PhotoFavorite.dart';

extension on Map<String, dynamic>{

  String getString(key){

    if (this[key] != null){
      if (this[key] is String){
        return this[key];
      }
    }
    return "";
  }

}

class UserModel{
  String uid;
  String photo_1;
  String photo_2;
  String photo_3;
  String photo_4;
  String photo_5;
  String photo_6;

  List<PhotoFavorites> photoFavorites;

  UserModel(Map<String, dynamic> json, String DocumentId){
    List<PhotoFavorites> photosRefe = List<PhotoFavorites>();
    List<DocumentSnapshot> documents = json["photoFavorites"];
    documents.forEach((document){
      photosRefe.add(PhotoFavorites(document.data, document.documentID));
    });
    this.photoFavorites = photosRefe;
    this.uid = DocumentId;
    this.photo_1 = json.getString("photo_1");
    this.photo_2 = json.getString("photo_2");
    this.photo_3 = json.getString("photo_3");
    this.photo_4 = json.getString("photo_4");
    this.photo_5 = json.getString("photo_5");
    this.photo_6 = json.getString("photo_6");
  }


}
