import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Category {

  String name = '';
  String id = '';

  Category({this.id, this.name});

  factory Category.fromDocumentSnapShot(DocumentSnapshot categoryDoc) {
    return Category(
        id: categoryDoc.documentID,
        name: categoryDoc.data['name'] as String
    );
  }

  Widget renderCategory(removeCategory) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 2),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Colors.black,
            width: 1,
          )
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(name),
            SizedBox(width: 10.0,),
            GestureDetector(
              onTap: removeCategory, 
              child: Icon(Icons.close, size: 16,),
            )
          ],
        ),
      ),
    );
  }
}