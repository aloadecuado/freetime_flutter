import 'package:flutter/material.dart';

extension on Map<String, dynamic>{

  String getString(key){

    if (this[key] != null){
      if (this[key] is String){
        return this[key];
      }
    }
    return "";
  }

  bool getBool(key){

    if (this[key] != null){
      if (this[key] is bool){
        return this[key];
      }
    }
    return false;
  }
  DateTime getDateTime(key){

    if (this[key] != null){
      if (this[key] is DateTime){
        return this[key];
      }
    }
    return DateTime.now();
  }

  List<int>getListInt(key){
    if (this[key] != null){
      if (this[key] is List<int>){
        return this[key];
      }
    }
    return List<int>();
  }

}
class DateExceptionModel{
  DateTime registerDate;
  DateTime startDate;
  DateTime endDate;
  bool active;

  DateException(Map<String, dynamic> json){
    this.registerDate = json.getDateTime("registerDate");
    this.startDate = json.getDateTime("startDate");
    this.endDate = json.getDateTime("endDate");
    this.active = json.getBool("active");
  }

  Map<String, dynamic> getMap(){
    return {
      "registerDate":this.registerDate,
      "startDate":this.startDate,
      "endDate":this.endDate,
      "active":this.active,
    };
  }
}