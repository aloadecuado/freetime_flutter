import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Establishment {

  String name = '';
  String address = '';
  String adminName = '';
  String id = '';

  Establishment({this.id, this.name, this.address, this.adminName});

  factory Establishment.fromDocumentSnapShot(DocumentSnapshot establishmentDoc) {
    return Establishment(
        id: establishmentDoc.documentID,
        name: establishmentDoc.data['name'] as String,
        address: establishmentDoc.data['address'] as String,
        adminName: establishmentDoc.data['adminName'] as String,
    );
  }

  Widget renderEstablishment(removeEstablishment) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 2),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Colors.black,
            width: 1,
          )
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(name),
            SizedBox(width: 10.0,),
            GestureDetector(
              onTap: removeEstablishment, 
              child: Icon(Icons.close),
            )
          ],
        ),
      ),
    );
  }
}