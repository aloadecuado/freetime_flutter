import 'package:flutter/material.dart';

extension on Map<String, dynamic>{

  String getString(key){

    if (this[key] != null){
      if (this[key] is String){
        return this[key];
      }
    }
    return "";
  }

  bool getBool(key){

    if (this[key] != null){
      if (this[key] is bool){
        return this[key];
      }
    }
    return false;
  }

  List<int>getListInt(key){
    if (this[key] != null){
      if (this[key] is List<int>){
        return this[key];
      }
    }
    return List<int>();
  }

}

class EnabledHours{
  String dayOfWeek = "Monday";
  List<int> hoursForDay = List<int>();
  bool isPrograming = false;

  EnabledHours(Map<String, dynamic> json){
    this.dayOfWeek = json.getString("dayOfWeek");
    this.hoursForDay = json.getListInt("hoursForDay");
    this.isPrograming = json.getBool("isPrograming");
  }

  Map<String, dynamic> getJson(){
    return {
      "dayOfWeek":this.dayOfWeek,
      "hoursForDay":this.hoursForDay,
      "isPrograming":this.isPrograming
    };
  }
}