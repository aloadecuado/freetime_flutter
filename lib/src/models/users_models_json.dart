import 'dart:convert';

UsesJsonModel clientFromJson(String str) => UsesJsonModel.fromJson(json.decode(str));

String clientToJson(UsesJsonModel data) => json.encode(data.toJson());

class UsesJsonModel {
    String id;
    String name;
    String userName;
    int identi;
    String correo;
    int telefono;
    String fotoUrl;
    bool disponible;

    UsesJsonModel({
        this.id,
        this.name,
        this.userName,
        this.identi,
        this.correo,
        this.telefono,
        this.fotoUrl,
        this.disponible = true,
    });

    factory UsesJsonModel.fromJson(Map<String, dynamic> json) => UsesJsonModel(
        id    :   json["id"],
        name  :   json["name"],
        userName: json["user_name"],
        identi  : json["identi"],
        correo  : json["correo"],
        telefono: json["telefono"],
        fotoUrl : json["fotoUrl"],
        disponible: json["disponible"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "user_name": userName,
        "identi": identi,
        "correo": correo,
        "telefono": telefono,
        "fotoUrl": fotoUrl,
        "disponible": disponible,
    };
}
