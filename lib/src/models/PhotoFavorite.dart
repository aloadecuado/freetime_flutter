import 'package:cloud_firestore/cloud_firestore.dart';
extension on Map<String, dynamic>{

  String getString(key){

    if (this[key] != null){
      if (this[key] is String){
        return this[key];
      }
    }
    return "";
  }

}

class PhotoFavorites{
  DocumentReference serverUser;
  String photo1;
  String photo2;
  String photo3;
  String photo4;
  String photo5;
  String photo6;
  String uid;



  PhotoFavorites(Map<String, dynamic> json, String documentId){

    this.uid = documentId;
    this.serverUser = json["serverUser"];
    this.photo1 = json.getString("photo1");
    this.photo2 = json.getString("photo2");
    this.photo3 = json.getString("photo3");
    this.photo4 = json.getString("photo4");
    this.photo5 = json.getString("photo5");
    this.photo6 = json.getString("photo6");


  }







}