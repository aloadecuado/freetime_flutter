import 'package:flutter/material.dart';
import 'package:free_time/src/ui/pages/category_page.dart';

class BoxCategory extends StatelessWidget {
  final String _imageUrl;
  final String _texto;
  final String _categoryId;
  final user;

  BoxCategory(this._texto, this._imageUrl, this._categoryId, this.user);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Column(
        children: <Widget>[
          _crearContenedor(),
        ],
      ),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    CategoryPage(_texto, _imageUrl, _categoryId, user)));
      },
    );
  }

  Widget _crearContenedor() {
    return Container(
      height: 105,
      width: 105,
      margin: EdgeInsets.only(left: 15, right: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          fit: BoxFit.fill,
          image: NetworkImage(_imageUrl),
        ),
      ),
      child: _mensaje(),
    );
  }

  Widget _mensaje() {
    return Center(
      child: Container(
        alignment: Alignment.bottomCenter,
        margin: EdgeInsets.only(
          bottom: 5,
        ),
        child: Text(
          _texto,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.bold
          ),
        ),
      ),
    );
  }
}
