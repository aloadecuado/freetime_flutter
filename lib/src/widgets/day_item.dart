import 'package:flutter/material.dart';
import 'package:free_time/src/config.dart';

Widget dayItem(dayNum, day, isToday, setDay) {
  return Column(
    children: <Widget>[
      Text(day.toString()),
      SizedBox(
        height: 5.0,
      ),
      GestureDetector(
        onTap: () {
          setDay(day);
        },
        child: Container(
          width: 30.0,
          height: 30.0,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100.0),
              color: (isToday == day) ? MyColors.pink : Colors.transparent),
          child: Center(
            child: Text(
              dayNum.toString(),
              style: TextStyle(
                  color: (isToday == day) ? Colors.white : Color(0xff363636)),
            ),
          ),
        ),
      ),
    ],
  );
}
