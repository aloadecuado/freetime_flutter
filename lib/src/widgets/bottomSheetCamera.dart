import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonSheetCamera {
  List items = [];

  ButtonSheetCamera(List options) {
    items = options;
  }

  _getItems(BuildContext context) {
    var options = <Widget>[];
    if (Platform.isAndroid) {
      for (var i = 0; i < items.length; i++) {
        options.add(ListTile(
          leading: Icon(items[i]['icon']),
          title: Text(
            items[i]['label'],
            style: TextStyle(
              color: items[i]['color'] ?? Colors.black54,
            ),
          ),
          onTap: () {
            Navigator.pop(context, items[i]['value']);
          },
        ));
      }
    } else {
      for (var i = 0; i < items.length; i++) {
        if (!items[i]['isCancel']) {
          options.add(CupertinoActionSheetAction(
            isDefaultAction: items[i]['isCancel'] ?? false,
            child: Text(
              items[i]['label'],
              style: TextStyle(
                color: items[i]['color'] ?? Colors.blueAccent,
              ),
            ),
            onPressed: () {
              Navigator.pop(context, items[i]['value']);
            },
          ));
        }
      }
    }
    return options;
  }

  Future show(BuildContext context) async {
    if (Platform.isAndroid) {
      return showModalBottomSheet(
          context: context,
          builder: (BuildContext context) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[..._getItems(context)],
            );
          });
    } else {
      return showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) => CupertinoActionSheet(
          title: const Text('Choose Options'),
          message: const Text('Your options are '),
          actions: <Widget>[..._getItems(context)],
          cancelButton: CupertinoActionSheetAction(
            child: const Text('Cancelar'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
      );
    }
  }
}
