import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/pages/filters.dart';
import 'package:free_time/src/widgets/app_bar.dart';
import 'package:provider/provider.dart';

class AppBarHome extends StatefulWidget implements PreferredSizeWidget {
  @override
  _AppBarHomeState createState() => _AppBarHomeState();

  @override
  Size get preferredSize => Size.fromHeight(60.0);
}

class _AppBarHomeState extends State<AppBarHome> {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);
    final isAnonymous = user?.user?.isAnonymous;

    return CustomBar(
      elevation: 0,
      automaticallyImplyLeading: false,
      titleWidget: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 0),
        child: FutureBuilder(
          future: Firestore.instance
              .collection('users')
              .document(user.user.uid)
              .get(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return Padding(
                padding: const EdgeInsets.only(left: 2.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                          !isAnonymous && snapshot.data['image'] != null
                              ? snapshot.data['image']
                              : "https://f0.pngfuel.com/png/683/60/man-profile-illustration-png-clip-art-thumbnail.png",
                        ),
                      ),
                    ),
                    SizedBox(width: 10),
                    Flexible(
                      flex: 2,
                      fit: FlexFit.loose,
                      child: Text(
                        !isAnonymous && snapshot.data['userName'] != null
                            ? 'Hola, ${snapshot.data['userName']}'
                            : 'Hola invitado',
                        maxLines: 1,
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      fit: FlexFit.loose,
                      child: InkWell(
                        onTap: () {
                          changeScreen(context, Filters());
                        },
                        child: Container(
                          height: 45,
                          decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            gradient: LinearGradient(
                              colors: [
                                Color.fromRGBO(0, 188, 208, 1),
                                Color.fromRGBO(190, 124, 246, 1)
                              ],
                              begin: FractionalOffset.bottomLeft,
                              end: FractionalOffset.bottomRight,
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Text(
                                'Filtros',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Icon(
                                MaterialCommunityIcons.tune_vertical,
                                size: 22,
                                color: Colors.white,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }
}
