import 'package:flutter/material.dart';

class TextFieldSearch extends StatefulWidget{
  TextEditingController textEditingController;
  String place_holder;
  final void Function(String) onChangeText;
  VoidCallback onTextChangeComplete;


  TextFieldSearch(this.textEditingController, this.place_holder,
      this.onChangeText, this.onTextChangeComplete);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TextFieldSearch();
  }

}

class _TextFieldSearch extends State<TextFieldSearch>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: TextFormField(
        controller: widget.textEditingController,
        onEditingComplete: widget.onTextChangeComplete,
        onChanged: widget.onChangeText,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        decoration: new InputDecoration.collapsed(
          hintText: widget.place_holder,
        ),
      ),
      decoration: BoxDecoration (
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          color: Color(0xfff7f7f7)
      ),
      padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
    );
  }

}