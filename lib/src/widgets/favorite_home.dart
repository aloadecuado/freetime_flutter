import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/pages/favorite_detail.dart';
import 'package:free_time/src/ui/pages/user_profile.dart';
import 'package:provider/provider.dart';

import '../ui/Loby/Widgets/box_top_.dart';

class FavorieHome extends StatefulWidget {
  FavorieHome({Key key, this.user}) : super(key: key);
  final user;
  @override
  _FavorieHomeState createState() => _FavorieHomeState();
}

class _FavorieHomeState extends State<FavorieHome> {
  List _favorites = [];

  @override
  void initState() {
    _favorites = [];
    getFavorites();
    super.initState();
  }

  getFavorites() async {
    if (!widget.user.user.isAnonymous) {
      var result = await Firestore.instance
          .collection('users')
          .document(widget.user.user.uid)
          .get();
      if (result['favorites'] != null) {
        setState(() {
          _favorites = result['favorites'];
        });
      }
    }
  }

  TextGlobal _textGlobal = TextGlobal();

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 20.0),
          child: Container(
            child: Text(
              'Favoritos',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        Container(
          height: 160,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: (_favorites != null) ? renderFavUsers() : [],
          ),
        ),
      ],
    );
  }

  List<Widget> renderFavUsers() {
    return _favorites
        .map<Widget>(
          (fav) => FutureBuilder(
            future: fav.get(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return snapshot.connectionState == ConnectionState.done
                  ? GestureDetector(
                      onTap: () {
                        changeScreen(context, UserProfile(user: snapshot.data));
                      },
                      child: Container(
                        width: 115,
                        height: 140,
                        child: Padding(
                          padding: EdgeInsets.only(left: 20, bottom: 20),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: DecorationImage(
                                  image: NetworkImage(
                                      (snapshot.data['image'] != null)
                                          ? snapshot.data['image']
                                          : _textGlobal.placeImageNo),
                                  fit: BoxFit.cover),
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                gradient: LinearGradient(
                                    begin: Alignment.bottomCenter,
                                    end: Alignment.topCenter,
                                    stops: [
                                      0.1,
                                      0.7
                                    ],
                                    colors: [
                                      Color.fromRGBO(0, 0, 0, 0),
                                      Color.fromRGBO(0, 0, 0, 0.1),
                                    ]),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    snapshot.data['name'],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  : Text('Cargando');
            },
          ),
        )
        .toList();
  }
}
