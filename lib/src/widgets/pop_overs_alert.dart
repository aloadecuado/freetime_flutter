import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/ui/components/custom_dialog.dart';
import 'package:free_time/src/utils/color.dart';


class PopOversAlert{

  TextGlobal _textGlobal = TextGlobal();
  void showAlertTitleSubTitleYesOrNo(BuildContext context, String title, String subTitle, String messOk, String messnot, VoidCallback ok, VoidCallback not){

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) {

          return getCustomDialogOkAndNot(context, title, subTitle, messOk, messnot, ok, not);
        }
    );
  }

  void showAlertTitleSubTitleYes(BuildContext context, String title, String subTitle, String messOk, VoidCallback ok){

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) {

          return getCustomDialogOk(context, title, subTitle, messOk, ok,);
        }
    );
  }
  Key _keyRed;

  Widget getCustomDialogOkAndNot(BuildContext context, String title, String subTitle, String messOk, String messnot, VoidCallback ok, VoidCallback not){


    final Widget titleContainer = Container(
      key: _keyRed,
      width: double.infinity,
      height: 200.0,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/background/modal.png'),
              fit: BoxFit.cover
          ),
          borderRadius: BorderRadius.circular(20.0)
      ),
      child: Padding(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              SizedBox(height: 10.0,),
              Text(title, style:
              TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold
              ),
              ),
            ],
          )
      ),
    );
    return CustomAlertDialog(
      contentPadding: EdgeInsets.all(0.0),
      content: Container(
        height: 290.0,
        child: Stack(
          children: <Widget>[
            titleContainer,
            Container(

              child: Positioned(
                top: 80,
                child: Container(

                  width: 280,
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 30.0,),
                        Container(
                          child: Text(subTitle,
                            style: TextStyle(
                              fontSize: 16.0,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(height: 20.0,),
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)
                          ),
                          color: HexColor('#FF62A5'),
                          onPressed: ok,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                            child: Text(messOk, style: TextStyle(
                                color: Colors.white,
                                fontSize: 17.0
                            ),),
                          ),
                        ),
                        SizedBox(height: 10.0,),
                        GestureDetector(
                          onTap: not,
                          child: Text(messnot,
                            style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.grey
                            ),
                            textAlign: TextAlign.center,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }


  Widget getCustomDialogOk(BuildContext context, String title, String subTitle, String messOk, VoidCallback ok){


    final Widget titleContainer = Container(
      key: _keyRed,
      width: double.infinity,
      height: 200.0,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/background/modal.png'),
              fit: BoxFit.cover
          ),
          borderRadius: BorderRadius.circular(20.0)
      ),
      child: Padding(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              SizedBox(height: 10.0,),
              Text(title, style:
              TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold
              ),
              ),
            ],
          )
      ),
    );
    return CustomAlertDialog(
      contentPadding: EdgeInsets.all(0.0),
      content: Container(
        height: 290.0,
        child: Stack(
          children: <Widget>[
            titleContainer,
            Container(

              child: Positioned(
                top: 80,
                child: Container(

                  width: 280,
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 30.0,),
                        Container(
                          child: Text(subTitle,
                            style: TextStyle(
                              fontSize: 16.0,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(height: 20.0,),
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)
                          ),
                          color: HexColor('#FF62A5'),
                          onPressed: ok,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                            child: Text(messOk, style: TextStyle(
                                color: Colors.white,
                                fontSize: 17.0
                            ),),
                          ),
                        ),


                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}

