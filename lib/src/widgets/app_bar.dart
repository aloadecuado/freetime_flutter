import 'package:flutter/material.dart';

class CustomBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Widget leading;
  final double sizeTitle;
  final bool centerTitle;
  final Color background;
  final Color colorTitle;
  final double elevation;
  final Widget titleWidget;
  final List<Widget> actions;
  final bool automaticallyImplyLeading;

  const CustomBar({
    Key key,
    this.title,
    this.actions,
    this.leading,
    this.sizeTitle,
    this.elevation,
    this.background,
    this.colorTitle,
    this.centerTitle,
    this.titleWidget,
    this.automaticallyImplyLeading = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: elevation,
      automaticallyImplyLeading: automaticallyImplyLeading,
      iconTheme: IconThemeData(color: Colors.white),
      actionsIconTheme: IconThemeData(color: Colors.white),
      title: titleWidget != null
          ? titleWidget
          : Text(
              title,
              style: TextStyle(
                color: colorTitle ?? Colors.black,
                fontSize: sizeTitle ?? 16,
                fontWeight: FontWeight.w500,
              ),
            ),
      actions: actions,
      leading: leading,
      centerTitle: centerTitle,
      backgroundColor: background ?? Colors.white,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(60.0);
}
