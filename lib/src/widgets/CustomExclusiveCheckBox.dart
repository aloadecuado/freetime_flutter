import 'package:flutter/material.dart';

import 'CustomCheckBoxRow.dart';

class CustomExclusiveCheckBox extends StatefulWidget {
  List<String> listCheckBox;
  Function(bool, int) onTapSelect;
  int defaultTag = 0;

  CustomExclusiveCheckBox(
      {Key key, this.listCheckBox, this.onTapSelect, this.defaultTag});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CustomExclusiveCheckBox();
  }
}

class _CustomExclusiveCheckBox extends State<CustomExclusiveCheckBox> {
  bool isSelect = true;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return getChecksData();
  }

  Widget getChecksData() {
    return Column(
      children: getListCheckBox(),
    );
  }

  List<Widget> getListCheckBox() {
    List<Widget> listCheckBox = List<Widget>();

    int i = 0;
    widget.listCheckBox.forEach((data) {
      var custom = CustomCheckBoxRow(
        textCheck: data,
        state: (isSelect) ? (widget.defaultTag == i) : false,
        tag: i,
        onState: (state, tag) {
          setState(() {
            isSelect = state;
            widget.defaultTag = tag;
          });

          widget.onTapSelect(state, tag);
        },
      );
      listCheckBox.add(custom);
      i++;
    });

    return listCheckBox;
  }
}
