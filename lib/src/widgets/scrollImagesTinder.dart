import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ScrollImagesTinder extends StatefulWidget {

  dynamic item;
  ScrollController sc;

  ScrollImagesTinder({this.item, this.sc});

  @override
  _ScrollImagesTinderState createState() => _ScrollImagesTinderState();
}

class _ScrollImagesTinderState extends State<ScrollImagesTinder> {

  int _current = 0;

  @override
  Widget build(BuildContext context) {
    List<String> listImageByItem = [
      widget.item.data['photo_1'] ?? "https://via.placeholder.com/200x400/1585FF/ffffff?text=%22Sin%20imagen%22",
      widget.item.data['photo_2'] ?? "https://via.placeholder.com/200x400/1585FF/ffffff?text=%22Sin%20imagen%22",
      widget.item.data['photo_3'] ?? "https://via.placeholder.com/200x400/1585FF/ffffff?text=%22Sin%20imagen%22",
      widget.item.data['photo_4'] ?? "https://via.placeholder.com/200x400/1585FF/ffffff?text=%22Sin%20imagen%22",
      widget.item.data['photo_5'] ?? "https://via.placeholder.com/200x400/1585FF/ffffff?text=%22Sin%20imagen%22",
      widget.item.data['photo_6'] ?? "https://via.placeholder.com/200x400/1585FF/ffffff?text=%22Sin%20imagen%22",
    ];
    return Stack(
      children: <Widget>[
        CarouselSlider(
          options: CarouselOptions(
              height: MediaQuery.of(context).size.height,
              viewportFraction: 1.0,
              scrollDirection: Axis.vertical,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              }),
          items: listImageByItem.map((i) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(i), fit: BoxFit.cover)),
                );
              },
            );
          }).toList(),
        ),
        Container(
          alignment: Alignment.centerRight,
          margin: EdgeInsets.only(top: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: listImageByItem.map((url) {
              int index = listImageByItem.indexOf(url);
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.black,
                      width: 0.2
                  ),
                  shape: BoxShape.circle,
                  color: _current == index
                      ? Colors.white
                      : Colors.grey,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }
}