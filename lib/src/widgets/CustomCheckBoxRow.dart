import 'package:flutter/material.dart';
import 'package:free_time/src/utils/color.dart';

class CustomCheckBoxRow extends StatefulWidget {
  String textCheck;
  bool state;
  Function(bool, int) onState;
  int tag;

  CustomCheckBoxRow(
      {Key key, this.textCheck, this.state, this.onState, this.tag});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CustomCheckBoxRow();
  }
}

class _CustomCheckBoxRow extends State<CustomCheckBoxRow> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      children: <Widget>[
        Container(
          width: 50,
          height: 50,
          child: Checkbox(
            checkColor: HexColor('#5EAE57'),
            activeColor: HexColor('#FFFFFF'),
            value: widget.state,
            onChanged: (bool state) {
              setState(() {
                widget.state = state;
                widget.onState(widget.state, widget.tag);
              });
            },
          ),
        ),
        Flexible(
          child: Text(
            widget.textCheck,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        )
      ],
    );
  }
}
