import 'package:flutter/material.dart';

class CustomDecorationBox extends StatelessWidget {
  double x = 0;
  double y = 0;
  double width;
  double height;
  Alignment alignment;
  Widget child;
  BoxDecoration boxDecoration;

  CustomDecorationBox(
      {Key key,
      this.x,
      this.y,
      this.width,
      this.height,
      this.alignment,
      this.child,
      this.boxDecoration});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: width,
      height: height,
      alignment: alignment,
      margin: EdgeInsets.all(5.0),
      decoration: (boxDecoration == null)
          ? BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Color(0xFF00C3D2), Color(0xFFBE7CF6)]),
            )
          : boxDecoration,
      child: child,
    );
  }
}
