import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/Values/texts_globals.dart';

import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/utils/utils.dart';

import 'package:path/path.dart' as Path;

class AddDocumentFile extends StatefulWidget {
  String fileId;
  String labelString;
  String file_name;
  bool isValidate = false;

  AddDocumentFile(this.fileId, this.labelString, this.file_name);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AddDocumentFile();
  }
}

class _AddDocumentFile extends State<AddDocumentFile> {
  TextGlobal _textGlobal = TextGlobal();
  File _file;
  String file_name;
  bool _hasValidMime = false;
  FileType _pickingType;
  String _fileName = "";
  String _path;
  String _extension;

  bool _loading = false;
  bool primeraVezLoadData = true;
  bool primeraVezLoadData1 = true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    if (primeraVezLoadData) {
      file_name = widget.labelString;
      primeraVezLoadData = false;
    }
    if (widget.file_name.isNotEmpty && primeraVezLoadData1) {
      file_name = widget.file_name;
      primeraVezLoadData1 = false;
    }
    widget.file_name = file_name;

    return GestureDetector(
      onTap: () {
        _openFileExplorer(widget.fileId);
      },
      child: Container(
        alignment: Alignment.centerLeft,
        child: Text(file_name),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          border: Border.all(
            color: HexColor('#979797'),
          ),
        ),
        padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
      ),
    );
  }

  void _openFileExplorer(fileField) async {
    if (_pickingType != FileType.CUSTOM || _hasValidMime) {
      setState(() {
        _loading = true;
      });
      try {
        _path = null;
        _file = await FilePicker.getFile(
            type: _pickingType, fileExtension: _extension);
        if (_file != null) {
          _path = _file.path;
          setState(() {
            _fileName = _path.split('/').last ?? _path.split('/').last;
          });
          uploadFile(fileField);
        }
      } catch (e) {
        print("Unsupported operation" + e.toString());
      }
    }
  }

  Future uploadFile(fileField) async {
    setState(() {});
    final user = await FirebaseAuth.instance.currentUser();
    final name = getUID(8);
    print('Uploading file for user ${user.uid}');
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('users/${user.uid}/files/${name}_${Path.basename(_path)}');
    StorageUploadTask uploadTask = storageReference.putFile(_file);
    await uploadTask.onComplete;
    print('File Uploaded');
    storageReference.getDownloadURL().then((fileURL) {
      Firestore.instance.collection('users').document(user.uid).updateData({
        'files.${fileField.url}': fileURL,
        'files.${fileField.name}': _fileName,
      }).then((res) {
        setState(() {
          widget.isValidate = true;
          _loading = false;
          file_name = _fileName;
          widget.file_name = file_name;
        });
      });
    });
  }
}
