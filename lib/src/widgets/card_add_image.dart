import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_native_image/flutter_native_image.dart';

import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/utils/utils.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;


class CardAddImage extends StatefulWidget{
  double width;
  double height;
  String idImage;
  String pathDefauldImage;
  int flexExpanded;
  DocumentSnapshot userDocument;
  bool isValidate = false;

  CardAddImage({@required this.width, @required this.height, @required this.idImage, @required this.pathDefauldImage,
    @required this.flexExpanded, @required this.userDocument});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CardAddImage();
  }


}

class _CardAddImage extends State<CardAddImage>{

  File _image;
  File _imageThum;
  String _loading = '';
  int percentaje = 40;


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    if (widget.userDocument[widget.idImage] != null){
      widget.isValidate = true;
    }else{
      widget.isValidate = false;
    }
    return GestureDetector(
        onTap: () { choosePhoto(widget.idImage); },
        child: Container(
          height: widget.height,
          decoration: BoxDecoration(
              color: HexColor('#C1C0C9'),
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage((widget.userDocument[widget.idImage] != null)?widget.userDocument[widget.idImage]:widget.pathDefauldImage)
              ),
              borderRadius: BorderRadius.circular(15.0)
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 8.0, bottom: 8.0),
                    child: (_loading != '')
                        ?SizedBox(
                      child: CircularProgressIndicator(strokeWidth: 2.0,),
                      height: 10.0,
                      width: 10.0,
                    )
                        :Container(
                      width: 20.0,
                      height: 20.0,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                      child: Center(child: Text('1')),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      );
  }

  void choosePhoto(photoId) {
    showDialog(
      child: AlertDialog(
        title: Text('¿Como quieres subir tu foto?'),
        actions: <Widget>[
          FlatButton(
            child: Text('Abrir galeria'),
            onPressed: () {
              Navigator.of(context).pop();
              chooseFile(photoId);
            },
          ),
          FlatButton(
            child: Text('Tomar foto'),
            onPressed: () {
              Navigator.of(context).pop();
              _tomarToma(photoId);
            },
          )
        ],
      ),
      context: context,
    );
  }

  Future _tomarToma(photoId) async {
    _procesarImagen(ImageSource.camera, photoId);
  }

  _procesarImagen(ImageSource origen, photoId) async {
    final foto = await ImagePicker.pickImage(
        source: origen
    );
    File Thumbled = await FlutterNativeImage.compressImage(foto.absolute.path,
        quality: 7, percentage: percentaje);
    print('Proccess');
    if ( foto != null ) {
      setState(() {
        _image = foto;
        _imageThum = Thumbled;
      });
    }
    uploadFile(photoId);
  }

  Future chooseFile(photoId) async {
    await ImagePicker.pickImage(source: ImageSource.gallery).then((image) async {
      if(image != null){
        File Thumbled = await FlutterNativeImage.compressImage(image.absolute.path,
            quality: 15, percentage: percentaje);
        var result = await FlutterImageCompress.compressAndGetFile(
          image.absolute.path, image.absolute.path,
          quality: 60,

        );

        setState(() {
          _image = result;


          _imageThum = Thumbled;
        });
        uploadFile(photoId);
      }
    });
  }

  Future<File> getCompressorImage(String path) async {
    File compressedFile = await FlutterNativeImage.compressImage(path,
        quality: 15, percentage: 40);

    return compressedFile;
  }
  Future uploadFile(photoId) async {
    setState(() {
      _loading = photoId;
    });
    final user = await FirebaseAuth.instance.currentUser();
    final name = getUID(8);


    print('Uploading file for user ${user.uid}');
    StorageReference storageReferenceThum = FirebaseStorage.instance
        .ref()
        .child('users/${user.uid}/photosThum/${name}_${Path.basename(_imageThum.path)}');
    StorageUploadTask uploadTaskThum = storageReferenceThum.putFile(_imageThum);
    await uploadTaskThum.onComplete;

    print('Uploading file for user ${user.uid}');
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('users/${user.uid}/photos/${name}_${Path.basename(_image.path)}');
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    await uploadTask.onComplete;


    print('File Uploaded');
    setState(() {
      _loading = '';
    });
    storageReferenceThum.getDownloadURL().then((fileURL) {
      /* setState(() {
        _photo_id = fileURL;
      });*/
      Firestore.instance.collection('users').document(user.uid).updateData({
        '${widget.idImage}Thum': fileURL
      });
    });

    storageReference.getDownloadURL().then((fileURL) {
      /* setState(() {
        _photo_id = fileURL;
      });*/
      Firestore.instance.collection('users').document(user.uid).updateData({
        '${widget.idImage}': fileURL
      });
    });
  }


}