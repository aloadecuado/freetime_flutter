import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/widgets/box_category.dart';

class BoxcategoryHome extends StatelessWidget {
  final user;

  BoxcategoryHome({Key key, this.user});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 20.0),
          child: Container(
            child: Text(
              'Categorías',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        _listCont(),
      ],
    );
  }

  Widget _listCont() {
    return Container(
      height: 130,
      child: FutureBuilder(
          future: Firestore.instance
              .collection('categories')
              .where('active', isEqualTo: true)
              .getDocuments(),
          builder: (context, snapshot) {
            return snapshot.connectionState == ConnectionState.done
                ? ListView(
                    //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    scrollDirection: Axis.horizontal,
                    children: snapshot.data.documents
                        .map<Widget>((DocumentSnapshot category) => BoxCategory(
                            category['name'],
                            category['picture'],
                            category.documentID,
                            user))
                        .toList())
                : Text('');
          }),
    );
  }
}
