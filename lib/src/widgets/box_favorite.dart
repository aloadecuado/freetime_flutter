import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/ui/pages/profile_user_page.dart';
import 'package:free_time/src/ui/pages/user_profile.dart';


class FavoritoBox extends StatelessWidget {

  final String _imageUrl;
  final String _texto;
  final DocumentSnapshot user;

  FavoritoBox(this._texto, this._imageUrl, this.user);

  @override
  Widget build(BuildContext context) {
    return Container(
      //padding: EdgeInsets.only(left: 10.0),
      child: GestureDetector(
        child: _crearContenedor(context),
        onTap: () {
          changeScreen(context, UserProfile(user: user,));
        },
      ),
    );
    
  }

  Widget _crearContenedor(BuildContext context) {
    return Center(
      child: SizedBox(
        width: MediaQuery.of(context).size.width*0.50,
        height:MediaQuery.of(context).size.height*0.40,
        child: Container(
          margin: EdgeInsets.only(left: 5.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Card(
              clipBehavior: Clip.antiAlias,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                    _crearImagenFondo(),
                    Column(
                      children: <Widget>[
                        Container(margin: EdgeInsets.only(top: 5)),
                        //_crearHeader(),
                        
                      ],
                    )
                ],
              ),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))
            ),
        ),
        
      ),  
    );
  }
  Widget _crearImagenFondo() {
    return Hero(
      child: Image(
        image: NetworkImage(_imageUrl),
        fit: BoxFit.cover,
      ),
      tag: _texto,
    );
    
  }
 

}
