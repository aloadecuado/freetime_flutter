import 'package:flutter/material.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:photo_view/photo_view.dart';

class ShowImageZoom extends StatefulWidget{
  ShowImageZoom({Key key, this.image}) : super(key: key);
  ImageProvider image;


  @override
  _ShowImageZoom createState() => _ShowImageZoom();


}

class _ShowImageZoom extends State<ShowImageZoom>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor('#8FE2F4'),
        elevation: 0.0,
        actions: <Widget>[

        ],
      ),
      body: Container(
          child: new PhotoView(
            imageProvider: widget.image,
            minScale: PhotoViewComputedScale.contained * 0.8,
            maxScale: 4.0,
          )
      ),
    );


  }

}