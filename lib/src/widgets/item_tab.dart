import 'package:flutter/material.dart';
import 'package:free_time/src/utils/color.dart';

Widget itemTab({ Function onTap, bool isActive, String text }) {
  return GestureDetector(
    onTap: onTap,
    child: Container(
      width: 35.0,
      padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      decoration: BoxDecoration(
        color: isActive ? HexColor('#FF689A'):HexColor('#ffffff'),
        borderRadius: BorderRadius.circular(30.0),
        border: Border.all(
          color: HexColor('#FF689A'),
          width: 2.0,
        )
      ),
      child: Center(
        child: Text(text, style: TextStyle(
          color: isActive ? HexColor('#ffffff'):HexColor('#FF689A'),
        )
      ))
    ),
  );
}