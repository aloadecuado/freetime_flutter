import 'package:flutter/material.dart';
import 'package:free_time/src/widgets/CustomDecorationBox.dart';

class CustomButton extends StatefulWidget{

  double height;
  double width;
  String textButton;
  bool isSelect = false;
  Function(int) onTap;
  int tag = -1;
  BoxDecoration boxDecoration;


  CustomButton({Key key, this.height, this.width, this.textButton, this.isSelect, this.onTap, this.tag, this.boxDecoration});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CustomButton();
  }

}

class _CustomButton extends State<CustomButton>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(

        child: CustomDecorationBox(
          width: widget.width,
          height: widget.height,
          alignment: Alignment.center,
          boxDecoration: widget.boxDecoration,
          child:

          Text(widget.textButton,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600
            ),
          )
        ),
        onTap: (){
          widget.onTap(widget.tag);
        }


      );
  }

}