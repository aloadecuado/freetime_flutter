import 'package:flutter/material.dart';

import 'CustomButton.dart';

class ExclusiveSelectorButton extends StatefulWidget {
  double height;
  double width;
  List<String> buttons;
  int indexDefaultSelect;
  Function(int) onTap;
  BoxDecoration boxDecorationOn;
  BoxDecoration boxDecorationOff;
  bool isExpanded = false;
  bool isListView = false;
  Axis orientation;

  ExclusiveSelectorButton(
      {Key key,
      this.height,
      this.width,
      this.buttons,
      this.onTap,
      this.indexDefaultSelect,
      this.boxDecorationOn,
      this.boxDecorationOff,
      this.isExpanded,
      this.isListView,
      this.orientation});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ExclusiveSelectorButton();
  }
}

class _ExclusiveSelectorButton extends State<ExclusiveSelectorButton> {
  int tag = -1;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return (!widget.isListView)
        ? Row(
            children: getListButtons(),
          )
        : Container(
            height: widget.height,
            child: ListView(
              scrollDirection: widget.orientation,
              children: getListButtons(),
            ),
          );
  }

  List<Widget> getListButtons() {
    List<Widget> customButtons = List<Widget>();
    int i = 0;
    widget.buttons.forEach((textButton) {
      var isSelect = false;
      if (i == widget.indexDefaultSelect) {
        isSelect = true;
        widget.indexDefaultSelect = -1;
      }
      var customButton = (widget.isExpanded != null)
          ? Expanded(
              flex: 1,
              child: CustomButton(
                height: widget.height,
                width: widget.width,
                isSelect: (isSelect) ? true : (this.tag == i),
                tag: i,
                textButton: textButton,
                boxDecoration: ((isSelect) ? true : (this.tag == i))
                    ? widget.boxDecorationOn
                    : widget.boxDecorationOff,
                onTap: (tag) {
                  setState(() {
                    this.tag = tag;
                  });
                  widget.onTap(tag);
                },
              ),
            )
          : CustomButton(
              height: widget.height,
              width: widget.width,
              isSelect: (isSelect) ? true : (this.tag == i),
              tag: i,
              textButton: textButton,
              boxDecoration: ((isSelect) ? true : (this.tag == i))
                  ? widget.boxDecorationOn
                  : widget.boxDecorationOff,
              onTap: (tag) {
                setState(() {
                  this.tag = tag;
                });
                widget.onTap(tag);
              },
            );

      customButtons.add(customButton);
      i++;
    });

    return customButtons;
  }
}
