import 'package:flutter/material.dart';

class TextInputLogin extends StatefulWidget {
  final String hintText;
  final String labelText;
  final bool obscureText;
  final Function validator;
  final TextInputType keyboardType;
  final TextEditingController controller;

  const TextInputLogin({
    Key key,
    this.hintText,
    this.validator,
    this.keyboardType,
    this.obscureText = false,
    @required this.labelText,
    @required this.controller,
  }) : super(key: key);

  @override
  _TextInputLoginState createState() => _TextInputLoginState();
}

class _TextInputLoginState extends State<TextInputLogin> {
  bool _obscureText = false;

  static const OutlineInputBorder borderStyle = OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(color: Colors.white, width: 1.0),
  );

  static const OutlineInputBorder errorBorderStyle = OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(color: Colors.red, width: 1.0),
  );

  static const TextStyle labelStyle = TextStyle(color: Colors.white);

  @override
  void initState() {
    _obscureText = widget.obscureText;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      obscureText: _obscureText,
      keyboardType: widget.keyboardType ?? TextInputType.text,
      style: labelStyle,
      decoration: InputDecoration(
        filled: true,
        fillColor: Color.fromRGBO(255, 255, 255, 0.3),
        hintText: widget.hintText,
        hintStyle: labelStyle,
        labelText: widget.labelText,
        labelStyle: labelStyle,
        focusedBorder: borderStyle,
        enabledBorder: borderStyle,
        errorBorder: errorBorderStyle,
        focusedErrorBorder: errorBorderStyle,
        suffixIcon: widget.obscureText
            ? IconButton(
                onPressed: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
                icon: Icon(
                  _obscureText ? Icons.visibility_off : Icons.visibility,
                  size: 28,
                  color: Colors.white70,
                ),
              )
            : null,
      ),
      validator: widget.validator,
    );
  }
}
