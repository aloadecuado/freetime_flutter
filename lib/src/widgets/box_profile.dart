import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/utils/utils.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;

class BoxProfile extends StatefulWidget {
  BoxProfile({this.user});

  final user;

  @override
  _BoxProfileState createState() => _BoxProfileState();
}

class _BoxProfileState extends State<BoxProfile> {
  File foto;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 5.0,
            offset: Offset(0.0, 1.0),
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          fotoEstado(),
          SizedBox(width: 20),
          _nombreDestino(),
        ],
      ),
    );
  }

  Widget fotoEstado() {
    return Stack(
      children: <Widget>[
        StreamBuilder(
          stream: Firestore.instance
              .collection('users')
              .document(widget.user.user.uid)
              .snapshots(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return GestureDetector(
              onTap: () {
                if (!widget.user.user.isAnonymous) {
                  chooseFile();
                }
              },
              child: CircleAvatar(
                radius: 45,
                backgroundColor: Colors.transparent,
                backgroundImage:
                    snapshot.hasData && snapshot.data['image'] != null
                        ? NetworkImage(snapshot.data['image'])
                        : AssetImage('assets/icon/no-usuario.png'),
              ),
            );
          },
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 70,
            left: 60,
          ),
          child: Container(
            width: 25,
            height: 25,
            decoration: BoxDecoration(
              color: HexColor('#3CD242'),
              shape: BoxShape.circle,
              border: Border.all(
                color: Colors.white,
                width: 2.0,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _nombreDestino() {
    return Flexible(
      flex: 1,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FutureBuilder(
            future: Firestore.instance
                .collection('users')
                .document(widget.user.user.uid)
                .get(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return Flexible(
                flex: 1,
                fit: FlexFit.loose,
                child: Text(
                  (snapshot.data != null && !widget.user.user.isAnonymous)
                      ? snapshot.data['name']
                      : 'Invitado',
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              );
            },
          ),
          SizedBox(
            height: 5.0,
          ),
          Flexible(
            flex: 1,
            fit: FlexFit.loose,
            child: Text(
              'Ciudad,País',
              style: TextStyle(
                fontSize: 14,
                color: Colors.black38,
                fontWeight: FontWeight.w200,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future chooseFile() async {
    await ImagePicker.pickImage(source: ImageSource.gallery)
        .then((image) async {
      if (image != null) {
        var result = await FlutterImageCompress.compressAndGetFile(
          image.absolute.path,
          image.absolute.path,
          quality: 60,
        );
        uploadFile(result);
      }
    });
  }

  Future uploadFile(result) async {
    final name = getUID(8);
    StorageReference storageReference = FirebaseStorage.instance.ref().child(
        'users/${widget.user.user.uid}/photos/${name}_${Path.basename(result.path)}');
    StorageUploadTask uploadTask = storageReference.putFile(result);
    await uploadTask.onComplete;
    print('File Uploaded');
    storageReference.getDownloadURL().then((fileURL) {
      Firestore.instance
          .collection('users')
          .document(widget.user.user.uid)
          .updateData({'image': fileURL});
    });
  }
}
