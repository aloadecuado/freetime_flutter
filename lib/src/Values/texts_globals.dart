import 'package:free_time/src/Internationality/localizations.dart';

class TextGlobal {
  final String locale = "es";
  final String Ambient = "Dev";

  String felicitaciones =
      (Localization("es" /*por ahora español*/).locale == "es")
          ? 'Felicitaciones!'
          : "";

  String timer =
      (Localization("es" /*por ahora español*/).locale == "es") ? 'Timer' : "";

  String descriptionTimer = (Localization("es" /*por ahora español*/).locale ==
          "es")
      ? 'Consiste en prestar un servicio para que las personas puedan conocerte como persona.'
      : "";

  String securePlaces =
      (Localization("es" /*por ahora español*/).locale == "es")
          ? 'Sitios seguros'
          : "";

  String descriptionPlaces = (Localization("es" /*por ahora español*/).locale ==
          "es")
      ? 'Seleccionamos sitios seguros para que puedas brindar tu servicio con totoal tranquilidad.'
      : "";

  String virtualChanel =
      (Localization("es" /*por ahora español*/).locale == "es")
          ? 'Canales virtuales'
          : "";

  String descriptionVirtual = (Localization("es" /*por ahora español*/)
              .locale ==
          "es")
      ? 'Puedes brindar tu servicio de manera virtual, adaptándonos a la época en la que estamos, brindando la misma experiencia y seguridad en el servicio.'
      : "";

  String profesionalTimer = (Localization("es" /*por ahora español*/).locale ==
          "es")
      ? 'Tiempo libre profesional'
      : "";

  String descriptionProfesional = (Localization("es" /*por ahora español*/).locale ==
          "es")
      ? 'Consiste en prestar un servicio, en este caso, dependiendo de la profesión que ejersas.'
      : "";

  String estaremosEvaluando = (Localization("es" /*por ahora español*/)
              .locale ==
          "es")
      ? ' Estaremos Evaluando tus documentos, está atento a la confirmación y formar así formar parte de Freetime.'
      : "";

  String aceptar = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Aceptar"
      : "";

  String subeTuArchivo =
      (Localization("es" /*por ahora español*/).locale == "es")
          ? "Sube tu archivo"
          : "";

  var agregueFecha = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Por favor agregue fecha privada y publica de nacimiento"
      : "";

  String acepteTerminos =
      (Localization("es" /*por ahora español*/).locale == "es")
          ? "Por favor acepte terminos y condiciones"
          : "";

  String nombreCategoria =
      (Localization("es" /*por ahora español*/).locale == "es")
          ? "Nombre, Categoria, Especialidad, profesion"
          : "";

  String suEstado = (Localization("es" /*por ahora español*/).locale == "es")
      ? "su estado esta en validacion pronto se comunicaran con usted"
      : "";
  String error =
      (Localization("es" /*por ahora español*/).locale == "es") ? "Error" : "";

  String informacion = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Informacion"
      : "";

  String porFavor = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Por favor revisa que todos los datos estén completos"
      : "";

  String cerrar =
      (Localization("es" /*por ahora español*/).locale == "es") ? "Cerrar" : "";

  String notificationApi =
      (Localization("es" /*por ahora español*/).Ambient == "es")
          ? "https://fcm.googleapis.com/fcm/send"
          : "http://api.freetime.com.co/v1/app/notification";

  String SERVICE_RESUEST = "SERVICE_RESUEST";

  String tienesUn = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Tienes un nuevo servicio"
      : "";

  String servicioDe = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Servicio de: "
      : "";

  var placeImageNo = (Localization("es" /*por ahora español*/).locale == "es")
      ? "https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Imagen_no_disponible.svg/1200px-Imagen_no_disponible.svg.png"
      : "";

  String olvidasteTu = (Localization("es" /*por ahora español*/).locale == "es")
      ? "¿Olvidaste tu contraseña?"
      : "";

  String restaurar = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Restaurar"
      : "";

  var botonDe = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Boton de Pánico"
      : "";

  String panicButtonApi =
      (Localization("es" /*por ahora español*/).Ambient == "es")
          ? "http://api.freetime.com.co/v1/app/panic/"
          : "http://api.freetime.com.co/v1/app/panic/";

  var tikTok =
      (Localization("es" /*por ahora español*/).locale == "es") ? "TikTok" : "";

  String diplomas = (Localization("es" /*por ahora español*/).locale == "es")
      ? "DIPLOMAS, CERFIFICADOS, CURSOS, POSTGRADOS, ETC"
      : "";

  List<String> dayOfWeek =
      (Localization("es" /*por ahora español*/).locale == "es")
          ? [" Lu ", " Ma ", " Mi ", " Ju ", " Vi ", " Sa ", " Do "]
          : [" Lu ", " Ma ", " Mi ", " Ju ", " Vi ", " Sa ", " Do "];

  String textSeleccionesDeLunesAViernes =
      (Localization("es" /*por ahora español*/).locale == "es")
          ? "Configurar de lunes a viernes"
          : "";

  String textSeleccionesDeLunesASabado =
      (Localization("es" /*por ahora español*/).locale == "es")
          ? "Configurar de lunes a sabado"
          : "";

  String textSeleccioneTodaLaSemana =
      (Localization("es" /*por ahora español*/).locale == "es")
          ? "Configurar para toda la semana"
          : "";

  var addDate = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Agrega una fecha"
      : "";

  String getString(String key) {
    Localization localization = Localization(locale);

    return localization.getString(key);
  }

  String creaSolicitud =
      (Localization("es" /*por ahora español*/).locale == "es")
          ? "Crear Solicitud"
          : "";
  String bienvenidoA = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Bienvenido a Freetime"
      : "";
  String beforeCall = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Quizas más tarde"
      : "";
  String seParte = (Localization("es" /*por ahora español*/).locale == "es")
      ? "Se parte de nuestro equipo y ofrece tus servicios."
      : "";
}
