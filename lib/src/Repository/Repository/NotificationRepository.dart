import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:free_time/src/Repository/Api/PostRestApi.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:http/http.dart' as http;

class NotificationRepository {

  TextGlobal _textGlobal = TextGlobal();

  PostRestApi postRestApi = PostRestApi();
  final String serverToken = '<Server-Token>';
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();

  Future<bool> sendNotification(String title, String body, String TYPE, String bodydata, String registrationToken) async {

    /*final response =
    await http.post(_textGlobal.notificationApi, headers: {"Content-Type": "application/json", "Authorization":"key=AAAAXAUd_Eg:APA91bF50iwvglYV77skXXZAltCJprrapkuT2yfQrTJBB2hDuoWlnK6aGGkl5ZN5mmqL8psK7MbnS66Ki8m6G3II0pVzz3OBpnFaMv02m1n9lCJG0fJiCRTGHLvkhZWFdzGAG8pFXd6v"},
    body: {
      "to":registrationToken,
      "collapse_key":"type_a",
      "notification":{
        "body":body,
        "title":title
      }
    }
    );*/

    final response = await postRestApi.post(_textGlobal.notificationApi,
      {
        "title": title,
        "body": body,
        "TYPE": TYPE,
        "bodydata": bodydata,
        "registrationToken": registrationToken
      }
    );


    if (response == "") {
      // Si el servidor devuelve una repuesta OK, parseamos el JSON
      return true;
    } else {
      // Si esta respuesta no fue OK, lanza un error.
      return false;
      throw Exception('Failed to load post');
    }

    /*if (response.statusCode == 200){
      return true;
    }else{
      return false;
    }*/


  }
}