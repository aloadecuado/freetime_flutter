import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:free_time/src/Repository/Api/GetRestApi.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/models/GeneralObjectResponse.dart';
import 'package:http/http.dart' as http;

class PanicButtonRepository {

  TextGlobal _textGlobal = TextGlobal();

  GetRestApi getRestApi = GetRestApi();

  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();

  Future<GeneralObjectResponse> sendPanicMesssage(String uid) async {

    final response = await getRestApi.get(_textGlobal.panicButtonApi, uid);

    GeneralObjectResponse generalObjectResponse = GeneralObjectResponse(jsonDecode(response));


    return generalObjectResponse;



  }
}