import 'package:cloud_firestore/cloud_firestore.dart';


class AddAndRemoveFavoriteApi{

  final String USERS = "users";
  final String PLACES = "places";

  final Firestore _db = Firestore.instance;

  Future<List<DocumentSnapshot>> getFavorites(String uidServer, String uidCurrent, String photoId) async{

    CollectionReference refUserServer = _db.collection(USERS).document(uidServer).collection(photoId.toString());


    QuerySnapshot querySnapshos = await refUserServer.getDocuments();
    return querySnapshos.documents;

  }

  Future<List<DocumentSnapshot>> addFavorite(String uidServer, String uidCurrent, String photoId) async{

    DocumentReference refUserCurrent = _db.collection(USERS).document(uidCurrent);
    CollectionReference refUserServer = _db.collection(USERS).document(uidServer).collection(photoId.toString());

    var isExist = await documentReferenceExist(uidServer, uidCurrent, photoId);

    if (!isExist){
      await refUserServer.add({"userCurrent":refUserCurrent, "isFavorite":true});

    }else{
      QuerySnapshot querySnapshot = await refUserServer.where("userCurrent", isEqualTo: refUserCurrent).getDocuments();
      querySnapshot.documents.forEach((document){
        document.reference.setData({"userCurrent":refUserCurrent, "isFavorite":true});
      });
    }

    QuerySnapshot querySnapshos = await refUserServer.where("isFavorite", isEqualTo: true).getDocuments();
    return querySnapshos.documents;

  }

  Future<List<DocumentSnapshot>> removeFavorite(String uidServer, String uidCurrent, String photoId) async{

    DocumentReference refUserCurrent = _db.collection(USERS).document(uidCurrent);
    CollectionReference refUserServer = _db.collection(USERS).document(uidServer).collection(photoId.toString());

    var isExist = await documentReferenceExist(uidServer, uidCurrent, photoId);

    if (isExist){
      QuerySnapshot querySnapshot = await refUserServer.where("userCurrent", isEqualTo: refUserCurrent).getDocuments();
      querySnapshot.documents.forEach((document){
        document.reference.setData({"userCurrent":refUserCurrent, "isFavorite":false});
      });
    }

    QuerySnapshot querySnapshos = await refUserServer.where("isFavorite", isEqualTo: true).getDocuments();
    return querySnapshos.documents;

  }

  Future<bool> documentReferenceExist(String uidServer, String uidCurrent, String photoId) async{

    DocumentReference refUserCurrent = _db.collection(USERS).document(uidCurrent);
    CollectionReference refUserServer = _db.collection(USERS).document(uidServer).collection(photoId.toString());


    QuerySnapshot querySnapshot = await refUserServer.where("userCurrent", isEqualTo: refUserCurrent).getDocuments();

    if (querySnapshot.documents.length >= 1){
      return true;
    }else{
      return false;
    }
  }

  Stream streamPhotoFavorites(String uidServer, String photoId) {
    return _db.collection(USERS).document(uidServer).collection(photoId.toString()).where("isFavorite", isEqualTo: true).snapshots();
  }

  bool isExistDocument(List<DocumentSnapshot> documents, DocumentReference documentReference){
    var exist = false;

    documents.forEach((document){
      if (document["userCurrent"] == documentReference){
      exist = true;

      }
    });

    return exist;
  }



}