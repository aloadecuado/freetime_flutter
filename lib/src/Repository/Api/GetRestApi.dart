import 'package:http/http.dart' as http;

class GetRestApi {




  Future<String> get(String url, String uid) async {
    final url1 = url + uid;
    final response = await http.get(url1);


    if (response.statusCode == 200) {
      // Si el servidor devuelve una repuesta OK, parseamos el JSON
      return response.body;
    } else {
      // Si esta respuesta no fue OK, lanza un error.
      return response.body;
      throw Exception('Failed to load post');

    }
  }
}