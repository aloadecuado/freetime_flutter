
import 'package:http/http.dart' as http;

class PostRestApi {




  Future<String> post(String url, Map<String, Object> body) async {
    final response =
    await http.post(url, body: body);

    if (response.statusCode == 200) {
      // Si el servidor devuelve una repuesta OK, parseamos el JSON
      return response.body;
    } else {
      // Si esta respuesta no fue OK, lanza un error.
      return response.body;
      throw Exception('Failed to load post');

    }
  }
}


