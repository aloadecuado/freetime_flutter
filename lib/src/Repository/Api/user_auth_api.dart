import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class UserAuthApi{
  final _facebookLogin = FacebookLogin();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<FirebaseUser> signFicabook() async{


    final result = await _facebookLogin.logIn(["email"]);


      FacebookAccessToken myToken = result.accessToken;

      AuthResult user = await _auth.signInWithCredential(FacebookAuthProvider.getCredential(
        accessToken: myToken.token
      ));


    return user.user;
  }
}