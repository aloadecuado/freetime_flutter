import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/utils/color.dart';

class ValidatePhone extends StatefulWidget {
  ValidatePhone({Key key, this.user, this.phone}) : super(key: key);
  
  final user;
  final phone;
  @override
  _ValidatePhoneState createState() => _ValidatePhoneState();
}

class _ValidatePhoneState extends State<ValidatePhone> {


  @override
  void initState() { 
    super.initState();
    FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: widget.phone,
      timeout: const Duration(seconds: 120),
      verificationCompleted: (AuthCredential authCredential) {
        print("authCredential ${authCredential}");
        Firestore.instance.collection('users').document(widget.user).updateData({
          'phone_validated': true
        }).whenComplete((){
          Navigator.of(context).pop();
        });
       
      },
      verificationFailed: (AuthException exception) {
        print("exception ${exception.message}");
      },
      codeSent: (String verificationId, [int forceResendingToken]) {
        print("codeSent ${verificationId}  forceResendingToken ${forceResendingToken}");
      },
      codeAutoRetrievalTimeout: (codeAutoRetrievalTimeout) {
        print("codeAutoRetrievalTimeout ${codeAutoRetrievalTimeout}");
      }); 
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close, color: HexColor('#FF689A')),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        elevation: 1.0,
        backgroundColor: Color(0xffF8F8F8),
        title: Row(
          children: <Widget>[
            Text('', textAlign: TextAlign.center, style: TextStyle(
              color: Color(0xff000000),
              fontSize: 17.0
            ),),
          ],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Validar número de télefono', style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20.0
            )),
            SizedBox(
              height: 40.0,
            ),
            Text('Ingresa el código que envíamos a tu celuar', style: TextStyle(
              fontSize: 16.0
            )),
            SizedBox(
              height: 40.0,
            ),
            Container(
              child: TextFormField(
                onEditingComplete: () {                  
                
                },
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.done,
                decoration: new InputDecoration.collapsed(
                  hintText: 'Código',
                ),
              ),
              decoration: BoxDecoration (
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                color: Colors.grey.withOpacity(0.2)
              ),
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
              ),
          ],
        ),
      ),
    );
  }
}