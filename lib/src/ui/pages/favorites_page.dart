import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/ui/pages/user_profile.dart';

import 'favorite_detail.dart';

class FavoritesPage extends StatefulWidget {
  FavoritesPage({Key key, this.user}) : super(key: key);

  final user;

  @override
  _FavoritesPageState createState() => _FavoritesPageState();
}

class _FavoritesPageState extends State<FavoritesPage> {
  List _favorites;
  bool _loading = true;
  TextEditingController _searchFav = new TextEditingController();

  @override
  void initState() {
    super.initState();
    getFavorites();
  }

  void getFavorites() async {
    var user = await Firestore.instance
        .collection('users')
        .document(widget.user.user.uid)
        .get();
    setState(() {
      _loading = false;
      _favorites = user.data['favorites'] ?? [];
    });
    print(_favorites);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).viewPadding.top + 20,
            bottom: 15,
            left: 15,
            right: 15,
          ),
          alignment: Alignment.centerLeft,
          child: Text(
            'Favoritos',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18.0,
            ),
          ),
        ),
        Container(
          height: 50.0,
          margin: EdgeInsets.only(
            bottom: 10,
            left: 15,
            right: 15,
          ),
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.grey[300]),
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.grey[100],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              SizedBox(
                width: 10.0,
              ),
              Expanded(
                child: TextFormField(
                  controller: _searchFav,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  decoration: new InputDecoration.collapsed(
                    hintText: 'Buscar',
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: Icon(
                    Icons.search,
                    color: Colors.grey[300],
                    size: 40.0,
                  ),
                ),
              )
            ],
          ),
        ),
        Expanded(
          child: ListView(
            padding: const EdgeInsets.only(top: 15),
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  changeScreen(
                    context,
                    FavoriteDetail(),
                  );
                },
                child: Container(
                  alignment: Alignment.centerRight,
                  width: MediaQuery.of(context).size.width,
                  child: Text('Elegir tus favoritos'),
                ),
              ),
              SizedBox(height: 20.0),
              (_loading)
                  ? Center(
                      child: SizedBox(
                        width: 50.0,
                        height: 50.0,
                        child: CircularProgressIndicator(),
                      ),
                    )
                  : GridView.count(
                      physics: NeverScrollableScrollPhysics(),
                      padding: const EdgeInsets.all(0),
                      shrinkWrap: true,
                      crossAxisCount: 2,
                      children: _favorites
                          .map<Widget>(
                            (favRef) => FutureBuilder<DocumentSnapshot>(
                              future: favRef.get(),
                              builder: (BuildContext context, snapshot) {
                                return Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: (snapshot.connectionState ==
                                          ConnectionState.done)
                                      ? GestureDetector(
                                          onTap: () {
                                            changeScreen(
                                              context,
                                              UserProfile(user: snapshot.data),
                                            );
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              image: DecorationImage(
                                                image: snapshot.data['image'] !=
                                                        null
                                                    ? NetworkImage(
                                                        snapshot.data['image'],
                                                      )
                                                    : AssetImage(
                                                        'assets/icon/no-usuario.png',
                                                      ),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                            child: Container(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  children: <Widget>[
                                                    Text(
                                                      snapshot.data['name'],
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 16),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      : Center(
                                          child: Text(
                                            'Cargando',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline,
                                          ),
                                        ),
                                );
                              },
                            ),
                          )
                          .toList()
                      // Generate 100 widgets that display their index in the List.
                      ),
              (!_loading && _favorites.length == 0)
                  ? Center(
                      child: Text('Aún no tienes favoritos'),
                    )
                  : Text('')
            ],
          ),
        ),
      ],
    );
  }
}
