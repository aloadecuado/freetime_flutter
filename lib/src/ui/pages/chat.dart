import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:provider/provider.dart';

class Chat extends StatefulWidget {
  Chat({Key key, this.service, this.user}) : super(key: key);
  final user;
  final service;
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  List<DocumentSnapshot> _messages = [];
  final TextEditingController _textEditingController =
      new TextEditingController();
  bool _isComposingMessage = false;
  var _scaffoldContext;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);
    final userRef =
        Firestore.instance.collection('users').document(user.user.uid);

    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false, // Don't show the leading button
          elevation: 0.0,
          backgroundColor: Color(0xffF8F8F8),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: HexColor('#00C3D2'),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: new Container(
          child: new Column(
            children: <Widget>[
              new Flexible(
                child: StreamBuilder(
                  stream: Firestore.instance
                      .collection('services')
                      .document(widget.service.documentID)
                      .collection('messages')
                      .orderBy('created_at', descending: false)
                      .snapshots(),
                  initialData: _messages,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    return Container(
                      height: MediaQuery.of(context).size.height * 1,
                      child: ListView(
                        children: snapshot.data.documents
                            .map<Widget>((DocumentSnapshot message) => Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          message.data['from'].path ==
                                                  userRef.path
                                              ? MainAxisAlignment.end
                                              : MainAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.all(8.0),
                                          padding: EdgeInsets.all(10.0),
                                          decoration: BoxDecoration(
                                              color:
                                                  message.data['from'].path ==
                                                          userRef.path
                                                      ? HexColor('#00C3D2')
                                                      : HexColor('#EFEFEF'),
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          child: Text(
                                            message.data['message'],
                                            style: TextStyle(
                                                color:
                                                    message.data['from'].path ==
                                                            userRef.path
                                                        ? Colors.white
                                                        : HexColor('#262628')),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ))
                            .toList(),
                      ),
                    );
                  },
                ),
              ),
              new Divider(height: 1.0),
              new Container(
                decoration:
                    new BoxDecoration(color: Theme.of(context).cardColor),
                child: _buildTextComposer(userRef),
              ),
              new Builder(builder: (BuildContext context) {
                _scaffoldContext = context;
                return new Container(width: 0.0, height: 0.0);
              })
            ],
          ),
          decoration: Theme.of(context).platform == TargetPlatform.iOS
              ? new BoxDecoration(
                  border: new Border(
                      top: new BorderSide(
                  color: Colors.grey[200],
                )))
              : null,
        ));
  }

  CupertinoButton getIOSSendButton(userRef) {
    return new CupertinoButton(
      child: new Text("Enviar"),
      onPressed: _isComposingMessage
          ? () => _textMessageSubmitted(_textEditingController.text, userRef)
          : null,
    );
  }

  IconButton getDefaultSendButton(userRef) {
    return new IconButton(
      icon: new Icon(Icons.send),
      onPressed: _isComposingMessage
          ? () => _textMessageSubmitted(_textEditingController.text, userRef)
          : null,
    );
  }

  Widget _buildTextComposer(userRef) {
    return new IconTheme(
        data: new IconThemeData(
          color: _isComposingMessage
              ? Theme.of(context).accentColor
              : Theme.of(context).disabledColor,
        ),
        child: new Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          child: new Row(
            children: <Widget>[
              new Flexible(
                child: new TextField(
                  controller: _textEditingController,
                  onChanged: (String messageText) {
                    setState(() {
                      _isComposingMessage = messageText.length > 0;
                    });
                  },
                  //onSubmitted: _textMessageSubmitted,
                  decoration: new InputDecoration.collapsed(
                      hintText: "Escribe un mensaje..."),
                ),
              ),
              new Container(
                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                child: Theme.of(context).platform == TargetPlatform.iOS
                    ? getIOSSendButton(userRef)
                    : getDefaultSendButton(userRef),
              ),
            ],
          ),
        ));
  }

  Future<Null> _textMessageSubmitted(String text, userRef) async {
    _textEditingController.clear();

    setState(() {
      _isComposingMessage = false;
    });

    _sendMessage(messageText: text, imageUrl: null, userRef: userRef);
  }

  void _sendMessage({String messageText, String imageUrl, userRef}) async {
    if (messageText.isNotEmpty) {
      await Firestore.instance
          .collection('services')
          .document(widget.service.documentID)
          .collection('messages')
          .document()
          .setData({
        'from': userRef,
        // 'from': Firestore.instance.collection('users').document('uXM9F6ADMraBkYZLG7lweFzUcxC3'),
        'message': messageText,
        'created_at': DateTime.now()
      });
    }
  }
}
