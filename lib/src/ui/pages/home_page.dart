import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/UserProfile/freetime_oferta_page.dart';
import 'package:free_time/src/ui/components/custom_dialog.dart';
import 'package:free_time/src/ui/pages/favorites_page.dart';
import 'package:free_time/src/ui/pages/login_page.dart';

import 'package:free_time/src/ui/pages/messenger_page.dart';
import 'package:free_time/src/ui/pages/profile_page.dart';
import 'package:free_time/src/ui/pages/welcomen_page.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/widgets/pop_overs_alert.dart';
import 'package:free_time/src/widgets/test.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';

import '../../config.dart';
import '../Loby/home_dos.dart';

const PAGES = [
  {
    "icon": MaterialCommunityIcons.home_outline,
    "value": 0,
    "validateAccess": false
  },
  {
    "icon": MaterialCommunityIcons.star_outline,
    "value": 1,
    "validateAccess": true
  },
  {
    "icon": MaterialCommunityIcons.message_text_outline,
    "value": 2,
    "validateAccess": false
  },
  {"icon": Icons.person_outline, "value": 3, "validateAccess": true},
];

class HomePage extends StatefulWidget {
  static final String routeName = 'home';
  //final String _imageUrl;
  HomePage({Key key, this.user}) : super(key: key);
  final user;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentIndex = 0;

  TextGlobal _textGlobal = TextGlobal();
  @override
  void initState() {
    // getUserLocation();
    checkMessageNewUser();
    super.initState();
  }

  getUserLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print(position);
    GeoPoint location = GeoPoint(position.latitude, position.longitude);
    if (widget.user != null) {
      if (widget.user.user != null) {
        Firestore.instance
            .collection('users')
            .document(widget.user.user.uid)
            .updateData({'location': location});
      }
    }
  }

  checkMessageNewUser() async {
    DocumentSnapshot user = await Firestore.instance
        .collection('users')
        .document(widget?.user?.user?.uid)
        .get();
    if (user.data != null && user?.data['newUser']) {
      PopOversAlert().showAlertTitleSubTitleYesOrNo(
          context,
          _textGlobal.bienvenidoA,
          _textGlobal.seParte,
          _textGlobal.creaSolicitud,
          _textGlobal.beforeCall, () async {
        await Firestore.instance
            .collection('users')
            .document(widget.user.user.uid)
            .updateData({'newUser': false});
        Navigator.of(context).pop();
        changeScreen(context, FreeOfertaPage());
      }, () {
        Navigator.of(context).pop();
      });
      /*showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) {
          final user = Provider.of<UserProvider>(_);
          return CustomAlertDialog(
            contentPadding: EdgeInsets.all(0.0),
            content: Container(
              height: 290.0,
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 200.0,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/background/modal.png'),
                        fit: BoxFit.cover
                      ),
                      borderRadius: BorderRadius.circular(20.0)
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: 10.0,),
                          Text('Bienvenido a Freetime', style:
                            TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold
                            ),
                          ),
                        ],
                      )
                    ),
                  ),
                  Container(
                    child: Positioned(
                      top: 100.0,
                      child: Container(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 30.0,),
                              Text(_textGlobal.seParte,
                                style: TextStyle(
                                  fontSize: 16.0,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 20.0,),
                              FlatButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0)
                                ),
                                color: HexColor('#FF62A5'),
                                onPressed: () async {
                                  await Firestore.instance.collection('users').document(widget.user.user.uid).updateData({
                                    'newUser': false
                                  });
                                  Navigator.of(context).pop();
                                  changeScreen(context, FreeOfertaPage());
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                                  child: Text('Crear Solicitud', style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17.0
                                  ),),
                                ),
                              ),
                              SizedBox(height: 10.0,),
                              GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text(_textGlobal.beforeCall,
                                  style: TextStyle(
                                    fontSize: 18.0,
                                    color: Colors.grey
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }
      );*/
    }
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);
    final isAnonymous = user?.user?.isAnonymous ?? false;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _callPage(currentIndex, user),
        ],
      ),
      bottomNavigationBar: _crearBottomNavigation(isAnonymous),
    );
  }

  Widget _callPage(int paginaActual, user) {
    switch (paginaActual) {
      case 0:
        return HomeDosPage(user: user);
      case 1:
        return FavoritesPage(user: user);
      case 2:
        return MessengetPage(user: user);
      case 3:
        return ProfileEditPage();
      default:
        return HomePage(user: user);
    }
  }

  Widget _crearBottomNavigation(isAnonymous) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(25),
          topLeft: Radius.circular(25),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            offset: Offset(0, 1),
            blurRadius: 8,
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(25),
          topLeft: Radius.circular(25),
        ),
        child: BottomAppBar(
          child: Padding(
            padding: const EdgeInsets.only(top: 15.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: generateOptions(),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> generateOptions() {
    return List.generate(PAGES.length, (int index) {
      return GestureDetector(
        child: Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            gradient: currentIndex == PAGES[index]['value']
                ? LinearGradient(
                    colors: [
                      Color.fromRGBO(0, 188, 208, 1),
                      Color.fromRGBO(190, 124, 246, 1)
                    ],
                    begin: FractionalOffset.bottomCenter,
                    end: FractionalOffset.topCenter,
                  )
                : null,
            borderRadius: BorderRadius.circular(5),
          ),
          child: Icon(
            PAGES[index]['icon'],
            size: currentIndex == PAGES[index]['value'] ? 25 : 30,
            color: currentIndex == PAGES[index]['value']
                ? Colors.white
                : Color.fromRGBO(196, 196, 196, 1),
          ),
        ),
        onTap: () {
          if (PAGES[index]['validateAccess']) {
            _validateAccess(PAGES[index]['value']);
          } else {
            _changePage(PAGES[index]['value']);
          }
        },
      );
    });
  }

  void _changePage(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  void _validateAccess(int page) {
    final user = Provider.of<UserProvider>(context, listen: false);
    final isAnonymous = user?.user?.isAnonymous ?? false;
    if (isAnonymous) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text("Alto ahí!"),
            content: new Text(
                "Regístrate para tener acceso a todas la funciones de Free Time"),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Registrarme",
                    style: TextStyle(color: MyColors.pink)),
                onPressed: () {
                  changeScreenReplacement(context, LoginPage());
                },
              ),
              new FlatButton(
                child:
                    new Text("Cerrar", style: TextStyle(color: MyColors.pink)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } else {
      _changePage(page);
    }
  }
}
