import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/components/custom_dialog.dart';
import 'package:free_time/src/ui/pages/home_page.dart';
import 'package:free_time/src/ui/pages/profile/my_diary_page.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ServiceDetail extends StatefulWidget {
  final String serviceId;

  ServiceDetail({Key key, this.serviceId}) : super(key: key);

  @override
  _ServiceDetailState createState() => _ServiceDetailState();
}

class _ServiceDetailState extends State<ServiceDetail> {
  bool _loading = true;
  DocumentSnapshot _service, _from;

  @override
  void initState() {
    super.initState();
    getService();
  }

  void getService() async {
    DocumentSnapshot service = await Firestore.instance
        .collection('services')
        .document(widget.serviceId)
        .get();
    setState(() {
      _service = service;
    });
    DocumentSnapshot from = await service.data['from'].get();
    setState(() {
      _from = from;
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Color(0xffF8F8F8),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: HexColor('#FF689A')),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Row(
          children: <Widget>[
            Text(
              'Confirmación del servicio.',
              textAlign: TextAlign.center,
              style: TextStyle(color: Color(0xff000000), fontSize: 17.0),
            ),
          ],
        ),
      ),
      body: Padding(
          padding: EdgeInsets.all(16.0),
          child: !_loading
              ? ListView(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.90,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/background/pattern_bg.png'),
                                  fit: BoxFit.cover)),
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.circular(10.0),
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    "Persona que solicita el servicio.",
                                    style: TextStyle(
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Container(
                                          width: 100.0,
                                          height: 100.0,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(100.0),
                                              image: DecorationImage(
                                                  image: NetworkImage((_from
                                                              .data['image'] !=
                                                          null)
                                                      ? _from.data['image']
                                                      : "https://p7.hiclipart.com/preview/691/765/226/computer-icons-person-anonymous.jpg"),
                                                  fit: BoxFit.cover))),
                                      SizedBox(
                                        height: 20.0,
                                      ),
                                      Text(
                                        _from.data['name'],
                                        style: TextStyle(
                                            fontSize: 22.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        (_from.data['profesion_type'] != null)
                                            ? _from.data['profesion_type']
                                            : "",
                                        style: TextStyle(
                                            fontSize: 18.0, color: Colors.grey),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.90,
                          child: Material(
                            elevation: 2.0,
                            borderRadius: BorderRadius.circular(10.0),
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    "Descripción del servicio.",
                                    style: TextStyle(
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Fecha",
                                        style: TextStyle(
                                            fontSize: 19.0,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        DateFormat("EEEE d, MMMM yyyy", 'es_MX')
                                            .format(DateTime
                                                .fromMillisecondsSinceEpoch(
                                                    _service.data["date"]
                                                            .seconds *
                                                        1000)),
                                        style: TextStyle(
                                          fontSize: 19.0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Hora",
                                        style: TextStyle(
                                            fontSize: 19.0,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "${_service.data['startAt']} - ${_service.data['endAt']}",
                                        style: TextStyle(
                                          fontSize: 19.0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Lugar",
                                        style: TextStyle(
                                            fontSize: 19.0,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "${_service.data['establishment']}",
                                        style: TextStyle(
                                          fontSize: 19.0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Servicio",
                                        style: TextStyle(
                                            fontSize: 19.0,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "${_service.data['type']}",
                                        style: TextStyle(
                                          fontSize: 19.0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Monto",
                                        style: TextStyle(
                                            fontSize: 19.0,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "\$ ${_service.data['ammount']}",
                                        style: TextStyle(
                                          fontSize: 19.0,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: RaisedButton(
                                  color: HexColor('#FF62A5'),
                                  onPressed: () async {
                                    showMessage('accepted');
                                  },
                                  child: Text(
                                    'Aceptar',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 19.0),
                                  )),
                            ),
                            SizedBox(
                              width: 20.0,
                            ),
                            Expanded(
                              child: RaisedButton(
                                  color: HexColor('#FF62A5'),
                                  onPressed: () async {
                                    showMessage('refused');
                                  },
                                  child: Text(
                                    'Rechazar',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 19.0),
                                  )),
                            )
                          ],
                        )
                      ],
                    )
                  ],
                )
              : CircularProgressIndicator()),
    );
  }

  void updateServiceStatus(status) async {
    print(status);
    await Firestore.instance
        .collection('services')
        .document(widget.serviceId)
        .updateData({'status': status});
    Navigator.of(context).pop();
    showUpdateSuccess(status);
  }

  void showUpdateSuccess(String status) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) {
          return CustomAlertDialog(
            contentPadding: EdgeInsets.all(0.0),
            content: Container(
              height: status == 'accepted' ? 350.0 : 250.0,
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 200.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/background/modal.png'),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: Padding(
                        padding: EdgeInsets.all(20.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Confirmación',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        )),
                  ),
                  Container(
                    child: Positioned(
                      top: 100.0,
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: 30.0,
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(left: 16.0, right: 16.0),
                                child: Text(
                                  (status == 'accepted')
                                      ? 'Se ha confirmado tu servicio correctamente. \n Recuerda ser puntual a la asistencia de tu cita. Una hora antes de tu servicio activaremos un chat  para que puedas comunicarte con Fernanda.'
                                      : 'Se ha cancelado el servicio correctamente',
                                  style: TextStyle(
                                    fontSize: 16.0,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              status == 'accepted'
                                  ? FlatButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0)),
                                      color: HexColor('#FF62A5'),
                                      onPressed: () {
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        final route = MaterialPageRoute(
                                            builder: (context) => DiaryPage());
                                        Navigator.push(context, route);
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16.0, vertical: 12.0),
                                        child: Text(
                                          'Ver mi agenda',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17.0),
                                        ),
                                      ),
                                    )
                                  : FlatButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0)),
                                      color: HexColor('#FF62A5'),
                                      onPressed: () {
                                        // changeScreenReplacement(context, HomePage(user: user));
                                        Navigator.of(context).pop();
                                        Navigator.of(context).pop();
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16.0, vertical: 12.0),
                                        child: Text(
                                          'Cerrar',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17.0),
                                        ),
                                      ),
                                    )
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  void showMessage(String status) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) {
          final user = Provider.of<UserProvider>(_);
          return CustomAlertDialog(
            contentPadding: EdgeInsets.all(0.0),
            content: Container(
              height: 300.0,
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 200.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/background/modal.png'),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: Padding(
                        padding: EdgeInsets.all(20.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Perfecto!',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        )),
                  ),
                  Container(
                    child: Positioned(
                      top: 100.0,
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: 30.0,
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(left: 16.0, right: 16.0),
                                child: Text(
                                  (status == 'accepted')
                                      ? 'Enviaremos la aceptacioón de tu servicio a ${_from.data['name']}, está atento a la confirmación del servico.'
                                      : 'Cancelaremos el servicio de  ${_from.data['name']} y ya no podrá solicitarte un servicio después.',
                                  style: TextStyle(
                                    fontSize: 16.0,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0)),
                                color: HexColor('#FF62A5'),
                                onPressed: () {
                                  updateServiceStatus(status);
                                  // changeScreenReplacement(context, ServiceDetail(serviceId: message['data']['SERVICE_ID']));
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0, vertical: 12.0),
                                  child: Text(
                                    'Ok, Entendido',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 17.0),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
