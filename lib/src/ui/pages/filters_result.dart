import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/pages/user_profile.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/widgets/text_field_search.dart';

class FilterResult extends StatefulWidget {
  FilterResult({Key key, this.category, this.gender}) : super(key: key);

  final String gender;
  final DocumentReference category;
  @override
  _FilterResultState createState() => _FilterResultState();
}

class _FilterResultState extends State<FilterResult> {
  TextEditingController _searchTextController = TextEditingController();

  StreamController<List<DocumentSnapshot>> usersStreamController =
      StreamController<List<DocumentSnapshot>>();
  Stream<List<DocumentSnapshot>> get usersResultStream =>
      usersStreamController.stream;
  StreamSink<List<DocumentSnapshot>> get usersResultSink =>
      usersStreamController.sink;

  TextGlobal textGlobal = TextGlobal();
  @override
  Widget build(BuildContext context) {
    Query query = Firestore.instance.collection('users');

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Container(
              child: TextFieldSearch(
                  _searchTextController, textGlobal.nombreCategoria, (s) async {
                QuerySnapshot querySnapshot = await query.getDocuments();

                List<DocumentSnapshot> documentsFilter =
                    new List<DocumentSnapshot>();

                querySnapshot.documents
                    .forEach((DocumentSnapshot documentSnapshot) {
                  if (documentSnapshot.data["name"].toString().contains(s) ||
                      documentSnapshot.data["genere"].toString().contains(s) ||
                      documentSnapshot.data["profesion_type"]
                          .toString()
                          .contains(s)) {
                    documentsFilter.add(documentSnapshot);
                  }
                });

                usersResultSink.add(documentsFilter);
              }, () {}),
            ),
            Container(
              width: 0.0,
              decoration: BoxDecoration(
                color: Color(0xffE7E7E7),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Padding(
                padding: EdgeInsets.all(4.0),
              ),
            ),
            StreamBuilder(
              stream: usersResultStream,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: Text('No se encontraron resultados.'),
                  );
                }
                return Container(
                  height: MediaQuery.of(context).size.height * 0.512,
                  child: snapshot.connectionState == ConnectionState.waiting
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : (snapshot.data.length != 0)
                          ? ListView(
                              children: showUsers(snapshot.data),
                            )
                          : Center(
                              child: Text('No se encontraron resultados.'),
                            ),
                );
              },
            ),

            /*FutureBuilder(
              future: query.getDocuments(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                return Container(
                  height: MediaQuery.of(context).size.height * 0.8,
                  child: snapshot.connectionState == ConnectionState.waiting
                    ?Center(
                      child: CircularProgressIndicator(),
                    )
                    :(snapshot.data.documents.length != 0)?ListView(
                      children: showUsers(snapshot.data.documents),
                    ):Center(
                      child: Text('No se encontraron resultados.'),
                    ),
                );
              },
            )*/
          ],
        ),
      ),
    );
  }

  List<Widget> showUsers(data) {
    return data
        .map<Widget>((DocumentSnapshot user) => GestureDetector(
              onTap: () {
                changeScreen(context, UserProfile(user: user));
              },
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 70.0,
                      height: 70,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100.0),
                        image: DecorationImage(
                          image: user.data['image'] != null
                              ? NetworkImage(user.data['image'])
                              : AssetImage('assets/icon/no-usuario.png'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20.0,
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          child: Text(
                            user.data['name'],
                            style: TextStyle(
                              fontSize: 17.0,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ))
        .toList();
  }
}
