import 'package:flutter/material.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/components/buttons/facebook_register.dart';
import 'package:free_time/src/ui/components/loading.dart';
import 'package:free_time/src/ui/components/my_app_bar.dart';
import 'package:free_time/src/ui/pages/home_page.dart';
import 'package:free_time/src/ui/pages/login_page.dart';
import 'package:free_time/src/ui/pages/validate_phone.dart';
import 'package:provider/provider.dart';

import '../../config.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key}) : super(key: key);

  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  final _key = GlobalKey<ScaffoldState>();

  TextEditingController _name = TextEditingController();
  TextEditingController _userName = TextEditingController();
  TextEditingController _identification = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _phone = TextEditingController();
  TextEditingController _pais = TextEditingController();
  TextEditingController _ciudad = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();
  bool _hidePass = false;
  bool _hidePass2 = false;
  bool _terminosCheck = false;
  bool _condicionesCheck = false;
  String docType = "CC";

  TextGlobal _textGlobal = TextGlobal();
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);
    return Scaffold(
      key: _key,
      appBar: MyAppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: FlatButton(
          textColor: Theme.of(context).primaryColor,
          onPressed: () {
            Navigator.of(context).maybePop();
          },
          child: Row(
            children: const <Widget>[
              Icon(Icons.keyboard_arrow_left, color: MyColors.teal),
              Text(
                "",
                style: TextStyle(
                    fontFamily: "OpenSans-SemiBold",
                    fontSize: 17,
                    color: MyColors.teal),
              )
            ],
          ),
        ),
      ),
      body: user.status == Status.Authenticating
          ? Loading()
          : Stack(
              children: <Widget>[
                SizedBox(height: 20),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Form(
                    key: _formKey,
                    child: ListView(
                      children: <Widget>[
                        SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: SizedBox(
                              width: double.infinity,
                              height: 50,
                              child: FacebookRegisterButton()),
                        ),
                        SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: SizedBox(
                              width: double.infinity,
                              height: 50,
                              child: RaisedButton(
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image.asset("assets/social/google.png"),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      "Registro con Google",
                                      style: TextStyle(
                                          letterSpacing: 1.5,
                                          fontFamily: "OpenSans-SemiBold"),
                                    ),
                                  ],
                                ),
                                onPressed: () {
                                  Provider.of<UserProvider>(context)
                                      .signInWithGoogle(context);
                                },
                              )),
                        ),
                        SizedBox(height: 20),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey.withOpacity(0.2),
                            elevation: 0.0,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: ListTile(
                                title: TextFormField(
                                  controller: _name,
                                  decoration: InputDecoration(
                                      hintText: "Nombre Completo",
                                      hintStyle: TextStyle(
                                          letterSpacing: 1.5,
                                          fontFamily: "OpenSans-SemiBold"),
                                      border: InputBorder.none),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "El campo es requerido";
                                    }
                                    return null;
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey.withOpacity(0.2),
                            elevation: 0.0,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: ListTile(
                                title: TextFormField(
                                  controller: _userName,
                                  decoration: InputDecoration(
                                      hintText: "Nombre de Usuario",
                                      hintStyle: TextStyle(
                                          letterSpacing: 1.5,
                                          fontFamily: "OpenSans-SemiBold"),
                                      border: InputBorder.none),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "El campo es requerido";
                                    }
                                    return null;
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey.withOpacity(0.2),
                            elevation: 0.0,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(left: 65),
                                    child: TextFormField(
                                      controller: _identification,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                          hintText: "Identificación",
                                          hintStyle: TextStyle(
                                              letterSpacing: 1.5,
                                              fontFamily: "OpenSans-SemiBold"),
                                          border: InputBorder.none),
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "El campo es requerido";
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                  Transform.translate(
                                    offset: Offset(-12, 0),
                                    child: Container(
                                      padding: EdgeInsets.only(
                                        left: 10,
                                      ),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color: Color(0xffD3D3D3))),
                                      child: DropdownButton<String>(
                                        value: docType,
                                        icon: Icon(
                                          Icons.arrow_drop_down,
                                          color: Color(0xffD3D3D3),
                                          size: 40,
                                        ),
                                        iconSize: 24,
                                        elevation: 16,
                                        underline: Container(
                                          height: 2,
                                          color: Colors.transparent,
                                        ),
                                        onChanged: (String newValue) {
                                          setState(() {
                                            docType = newValue;
                                          });
                                        },
                                        items: <String>['CC', 'CE']
                                            .map<DropdownMenuItem<String>>(
                                                (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(value),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey.withOpacity(0.2),
                            elevation: 0.0,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: ListTile(
                                title: TextFormField(
                                  controller: _email,
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                      hintText: "Correo electrónico",
                                      hintStyle: TextStyle(
                                          letterSpacing: 1.5,
                                          fontFamily: "OpenSans-SemiBold"),
                                      border: InputBorder.none),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      Pattern pattern =
                                          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                      RegExp regex = new RegExp(pattern);
                                      if (!regex.hasMatch(value))
                                        return 'Asegurese de colocar un email vàlido';
                                      else
                                        return null;
                                    }
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey.withOpacity(0.2),
                            elevation: 0.0,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: ListTile(
                                title: TextFormField(
                                  controller: _phone,
                                  keyboardType: TextInputType.phone,
                                  decoration: InputDecoration(
                                      hintText: "Teléfono",
                                      hintStyle: TextStyle(
                                          letterSpacing: 1.5,
                                          fontFamily: "OpenSans-SemiBold"),
                                      border: InputBorder.none),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "El campo es requerido";
                                    }
                                    return null;
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey.withOpacity(0.2),
                            elevation: 0.0,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: ListTile(
                                title: TextFormField(
                                  controller: _pais,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                      hintText: "País",
                                      hintStyle: TextStyle(
                                          letterSpacing: 1.5,
                                          fontFamily: "OpenSans-SemiBold"),
                                      border: InputBorder.none),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "El campo es requerido";
                                    }
                                    return null;
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey.withOpacity(0.2),
                            elevation: 0.0,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: ListTile(
                                title: TextFormField(
                                  controller: _ciudad,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                      hintText: "Ciudad",
                                      hintStyle: TextStyle(
                                          letterSpacing: 1.5,
                                          fontFamily: "OpenSans-SemiBold"),
                                      border: InputBorder.none),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "El campo es requerido";
                                    }
                                    return null;
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey.withOpacity(0.2),
                            elevation: 0.0,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: ListTile(
                                title: TextFormField(
                                  controller: _password,
                                  obscureText: !_hidePass,
                                  decoration: InputDecoration(
                                      hintText: "Contraseña",
                                      hintStyle: TextStyle(
                                          letterSpacing: 1.5,
                                          fontFamily: "OpenSans-SemiBold"),
                                      border: InputBorder.none),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Este campo es requerido";
                                    } else if (value.length < 6) {
                                      return "Debe ser mas de 6 dìgitos";
                                    }
                                    return null;
                                  },
                                ),
                                trailing: IconButton(
                                    icon: Icon(_hidePass
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                    onPressed: () {
                                      setState(() {
                                        _hidePass = !_hidePass;
                                      });
                                    }),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey.withOpacity(0.2),
                            elevation: 0.0,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: ListTile(
                                title: TextFormField(
                                  controller: _confirmPassword,
                                  obscureText: !_hidePass2,
                                  decoration: InputDecoration(
                                      hintText: "Repite  Contraseña",
                                      hintStyle: TextStyle(
                                          letterSpacing: 1.5,
                                          fontFamily: "OpenSans-SemiBold"),
                                      border: InputBorder.none),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Este campo es requerido";
                                    } else if (value.length < 6) {
                                      return "Debe ser mas de 6 dìgitos";
                                    }
                                    return null;
                                  },
                                ),
                                trailing: IconButton(
                                    icon: Icon(_hidePass2
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                    onPressed: () {
                                      setState(() {
                                        _hidePass2 = !_hidePass2;
                                      });
                                    }),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          child: ListTile(
                            leading: Checkbox(
                              checkColor: MyColors.pink,
                              activeColor: MyColors.grayLight,
                              value: _terminosCheck,
                              onChanged: (valor) {
                                setState(() {
                                  _terminosCheck = valor;
                                });
                              },
                            ),
                            title: Text(
                              "He leído y acepto los",
                              style: TextStyle(
                                  letterSpacing: 1.5,
                                  fontFamily: "OpenSans-SemiBold"),
                            ),
                            subtitle: Text(
                              "Términos y Condiciones",
                              style: TextStyle(
                                color: MyColors.tealLight,
                                fontSize: 17,
                                fontFamily: "OpenSans-SemiBold",
                              ),
                            ),
                          ),
                        ),
                        Container(
                          child: ListTile(
                            leading: Checkbox(
                              checkColor: MyColors.pink,
                              activeColor: MyColors.grayLight,
                              value: _condicionesCheck,
                              onChanged: (valor) {
                                setState(() {
                                  _condicionesCheck = valor;
                                });
                              },
                            ),
                            title: Text(
                                "He leído y acepto el procesamiento de datos personales sensibles y la",
                                style:
                                    TextStyle(fontFamily: "OpenSans-SemiBold")),
                            subtitle: Text("Política de Privacidad.",
                                style: TextStyle(
                                    color: MyColors.tealLight,
                                    fontSize: 17,
                                    fontFamily: "OpenSans-SemiBold")),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                          child: Container(
                            width: double.infinity,
                            height: 55,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              color: MyColors.pink,
                              disabledColor: MyColors.grayLight,
                              textColor: Colors.white,
                              child: Text(
                                "Regístrate",
                                style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 1.5,
                                ),
                              ),
                              onPressed: () async {
                                if (_formKey.currentState.validate() &&
                                    _terminosCheck &&
                                    _condicionesCheck) {
                                  if (_password.text != _confirmPassword.text) {
                                    _key.currentState.showSnackBar(SnackBar(
                                        content: Text(
                                            "Las contraseñas no coinciden")));
                                    return;
                                  }
                                  if (!await user.signUp(
                                      _name.text,
                                      _userName.text,
                                      _identification.text,
                                      _email.text,
                                      _phone.text,
                                      _pais.text,
                                      _ciudad.text,
                                      _password.text,
                                      docType)) {
                                    _key.currentState.showSnackBar(SnackBar(
                                        content: Text("Fallo el registro")));
                                    return;
                                  }
                                  changeScreenReplacement(
                                      context, ValidatePhone());
                                }
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Ya tienes Cuenta?",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    fontFamily: "OpenSans-SemiBold",
                                    letterSpacing: 1.5),
                              ),
                              FlatButton(
                                  child: Text(
                                    "Ingresa",
                                    style: TextStyle(
                                        fontFamily: "OpenSans-SemiBold",
                                        fontWeight: FontWeight.bold,
                                        color: MyColors.blue,
                                        letterSpacing: 1.5,
                                        fontSize: 18),
                                  ),
                                  onPressed: () =>
                                      changeScreen(context, LoginPage()))
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  @override
  void dispose() {
    _name.dispose();
    _userName.dispose();
    _identification.dispose();
    _email.dispose();
    _phone.dispose();
    _pais.dispose();
    _ciudad.dispose();
    _password.dispose();
    _confirmPassword.dispose();
    super.dispose();
  }
}
