import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/ui/pages/chat.dart';

class MessengetPage extends StatefulWidget {
  MessengetPage({Key key, this.user}) : super(key: key);

  final user;
  @override
  _MessengetPageState createState() => _MessengetPageState();
}

class _MessengetPageState extends State<MessengetPage> {
  bool _loading = true;
  List<DocumentSnapshot> _customerChats = [];
  List<DocumentSnapshot> _servicesChats = [];
  @override
  void initState() {
    super.initState();
    getChats();
  }

  getChats() async {
    DocumentReference userRef =
        Firestore.instance.collection('users').document(widget.user.user.uid);
    QuerySnapshot customerChats = await Firestore.instance
        .collection('services')
        .where('to', isEqualTo: userRef)
        .getDocuments();

    QuerySnapshot servicesChats = await Firestore.instance
        .collection('services')
        .where('from', isEqualTo: userRef)
        .getDocuments();

    setState(() {
      _servicesChats = servicesChats.documents;
      _customerChats = customerChats.documents;
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(
          left: 16.0,
          top: 16.0,
          right: 16.0,
        ),
        child: (_loading)
            ? Center(
                child: SizedBox(
                  width: 60.0,
                  height: 60.0,
                  child: CircularProgressIndicator(),
                ),
              )
            : ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Text(
                    'Mensajes',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Clientes',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                    ),
                  ),
                  renderChats(_customerChats),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Servicios solicitados',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                    ),
                  ),
                  renderChats(_servicesChats)
                ],
              ),
      ),
    );
  }

  Widget renderChats(chats) {
    return Column(
      children: chats
          .map<Widget>(
            (DocumentSnapshot chat) => GestureDetector(
              onTap: () {
                changeScreen(
                  context,
                  Chat(
                    service: chat,
                  ),
                );
              },
              child: FutureBuilder(
                  future: chat['to'].get(),
                  builder: (context, snapshot) {
                    return snapshot.connectionState == ConnectionState.done
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Container(
                                  width: 60.0,
                                  height: 60.0,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100.0),
                                    image: DecorationImage(
                                      image: snapshot.data['image'] != null
                                          ? NetworkImage(
                                              snapshot.data['image'],
                                            )
                                          : AssetImage(
                                              'assets/icon/no-usuario.png',
                                            ),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: Text(
                                  "${chat['type']} con ${snapshot.data['name']}",
                                ),
                              )
                            ],
                          )
                        : Text('');
                  }),
            ),
          )
          .toList(),
    );
  }
}
