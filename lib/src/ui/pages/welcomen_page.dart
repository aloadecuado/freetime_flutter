import 'package:flutter/material.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/components/imgBackground.dart';
import 'package:provider/provider.dart';

import '../../config.dart';
import 'login_page.dart';
import 'register_page.dart';

class WelcomenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);

    return Scaffold(
      extendBody: true,
      body: Stack(
        children: <Widget>[
          ImagenFondo(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: ListView(
              children: <Widget>[
                SizedBox(
                  height: 100,
                ),
                Center(
                  child: Text("FREE TIME",
                      style: TextStyle(
                          fontSize: 35,
                          fontWeight: FontWeight.bold,
                          color: Colors.white)),
                ),
                SizedBox(
                  height: 150,
                ),
                SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: Colors.white,
                    textColor: Color.fromRGBO(232, 41, 170, 1),
                    child: Center(
                      child: Text('Iniciar Sesión'),
                    ),
                    onPressed: () {
                      changeScreen(context, LoginPage());
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: Color.fromRGBO(232, 41, 170, 1),
                    textColor: Colors.white,
                    child: Center(
                      child: Text('Crear Cuenta'),
                    ),
                    onPressed: () {
                      changeScreen(context, RegisterPage());
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: Colors.white,
                    textColor: Color.fromRGBO(232, 41, 170, 1),
                    child: Center(
                      child: Text('Conoce Freetime'),
                    ),
                    onPressed: () {
                      user.signInAnonymously(context);
                    },
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
