import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:free_time/src/utils/color.dart' as prefix0;

//final FirebaseUser user;
class CentroAyudaPage extends StatefulWidget {
  @override
  _CentroAyudaPage createState() => _CentroAyudaPage();
}

class _CentroAyudaPage extends State<CentroAyudaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 1,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                'Preguntas frecuentes',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xff000000),
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ),
        body: ListView(
          padding: EdgeInsets.only(
            top: 30.0,
            left: 16.0,
            right: 16.0,
            bottom: 20.0,
          ),
          children: <Widget>[
            _cajaTop(),
            Container(
              margin: EdgeInsets.only(top: 30),
              padding: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 2.5,
                    offset: Offset(0.0, 1.0),
                  )
                ],
              ),
              child: FutureBuilder(
                future: Firestore.instance.collection('pqrs').getDocuments(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  return snapshot.connectionState == ConnectionState.waiting
                      ? Container(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        )
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: renderPqrs(snapshot.data));
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40, bottom: 10),
              height: 57.5,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey[400],
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
              child: FlatButton(
                color: prefix0.HexColor('#E8E8E8'),
                padding: EdgeInsets.only(
                  top: 10,
                  bottom: 10,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                onPressed: () async => await launch(
                  "https://wa.me/${573107943145}?text=Consulta",
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: Image.asset(
                        'assets/icon/wp.png',
                      ),
                    ),
                    Flexible(
                      flex: 4,
                      fit: FlexFit.tight,
                      child: Text(
                        'Escribenos por WhatsApp',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w800,
                          fontSize: 16,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 15, top: 20),
              height: 57.5,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    prefix0.HexColor('#00C3D2'),
                    prefix0.HexColor('#BB7DF6'),
                  ],
                  begin: FractionalOffset.bottomLeft,
                  end: FractionalOffset.bottomRight,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
              child: FlatButton(
                padding: EdgeInsets.only(
                  top: 10,
                  bottom: 10,
                  right: 40,
                  left: 40,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                onPressed: () async {
                  const url = 'mailto:Tiempolibreapp@gmail.com';
                  if (await canLaunch(url)) {
                    await launch(url);
                  } else {
                    throw 'Could not launch $url';
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Soporte via correo',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w800,
                          fontSize: 18),
                    )
                  ],
                ),
              ),
            ),
            FlatButton.icon(
              icon: Icon(
                Icons.phone,
                color: Color(0xff808080),
              ),
              color: Colors.transparent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              label: Padding(
                padding: const EdgeInsets.all(6.0),
                child: Text(
                  'Soporte de Freetime vía telefónica',
                  style: TextStyle(
                    color: Color(0xff808080),
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
              onPressed: () async {
                const url = 'tel:+573173665482';
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw 'Could not launch $url';
                }
              },
            ),
          ],
        ));
  }

  List<Widget> renderPqrs(data) {
    List<Widget> items = data.documents.map<Widget>((pqr) {
      return ExpansionTile(
        title: Text(
          pqr['title'],
          style: TextStyle(
            fontSize: 15.0,
            fontWeight: FontWeight.w800,
          ),
        ),
        children: [
          Container(
            child: Text(
              pqr['description'],
              textAlign: TextAlign.start,
            ),
          )
        ],
      );
    }).toList();
    items.insert(
        0,
        Text(
          'Preguntas frecuentes',
          textAlign: TextAlign.start,
          style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w800),
        ));
    return items;
  }

  _cajaTop() {
    return Material(
      elevation: 1,
      borderRadius: BorderRadius.all(Radius.circular(10)),
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 2.5,
              offset: Offset(0.0, 1.0),
            )
          ],
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 6,
              child: Container(
                child: Text('Hola! ¿Cómo te podemos ayudar?',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w800,
                    )),
              ),
            ),
            Icon(
              Icons.help,
              size: 50,
              color: prefix0.HexColor('#05C2D3'),
            )
          ],
        ),
      ),
    );
  }
}
