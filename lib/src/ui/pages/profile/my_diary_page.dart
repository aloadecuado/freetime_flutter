import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';
import '../../../utils/color.dart';

class DiaryPage extends StatefulWidget {
  DiaryPage({Key key}) : super(key: key);

  @override
  _DiaryPageState createState() => _DiaryPageState();
}

class _DiaryPageState extends State<DiaryPage> {
  DateTime _daySelected = DateTime.now();
  CalendarController _calendarController = CalendarController();
  Map<DateTime, List<dynamic>> _services = {};
  List _selectedEvents = [];
  @override
  void initState() {
    // TODO: implement initState
    getServices();
    super.initState();
  }

  getServices() async {
    final user = Provider.of<UserProvider>(context, listen: false);
    DocumentReference userRef =
        Firestore.instance.collection('users').document(user?.user?.uid);
    QuerySnapshot services = await Firestore.instance
        .collection('services')
        //.where('date', isEqualTo: _daySelected)
        .where('to', isEqualTo: userRef)
        .getDocuments();

    Iterable servicesDates = services.documents.map((service) {
      Timestamp d = service.data['date'];
      DateTime serviceDate =
          DateTime.fromMillisecondsSinceEpoch(d.seconds * 1000);
      print(serviceDate);
      return serviceDate;
    });

    servicesDates.map((date) {
      print(date);
    });

    setState(() {
      final _selectedDay = DateTime.now();
      for (var i = 0; i < services.documents.length; i++) {
        Timestamp d = services.documents[i].data['date'];
        DateTime serviceDate =
            DateTime.fromMillisecondsSinceEpoch(d.seconds * 1000);
        DateTime time = serviceDate.toLocal();
        var newHour = 0;
        var newMinute = 0;
        var newSecond = 0;
        serviceDate = new DateTime(time.year, time.month, time.day, newHour,
            newMinute, newSecond, time.millisecond, time.microsecond);
        if (_services[serviceDate] != null) {
          var c = [];
          c.addAll(_services[serviceDate]);
          c.add(services.documents[i].documentID);
          _services[serviceDate] = c;
        } else {
          _services[serviceDate] = [services.documents[i].documentID];
        }
      }
      _selectedEvents = _services[_selectedDay] ?? [];
    });
  }

  void _onDaySelected(DateTime day, List events) {
    print('CALLBACK: _onDaySelected');
    setState(() {
      _selectedEvents = events;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              'Mi Agenda',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xff000000),
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
      body: Container(
        padding: const EdgeInsets.only(top: 15),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            TableCalendar(
              availableCalendarFormats: {
                CalendarFormat.month: '',
                CalendarFormat.week: '',
              },
              events: _services,
              headerVisible: false,
              initialCalendarFormat: CalendarFormat.week,
              calendarController: _calendarController,
              locale: 'es_MX',
              onDaySelected: _onDaySelected,
              calendarStyle: CalendarStyle(
                selectedColor: MyColors.primaryColor,
                todayColor: MyColors.primaryColor,
                markersColor: MyColors.primaryColor,
                weekendStyle: TextStyle().copyWith(
                  color: MyColors.primaryColor,
                ),
                holidayStyle: TextStyle().copyWith(
                  color: MyColors.primaryColor,
                ),
                outsideDaysVisible: false,
              ),
              daysOfWeekStyle: DaysOfWeekStyle(
                weekendStyle: TextStyle().copyWith(
                  color: MyColors.primaryColor,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 5, 0, 20),
              width: 35,
              height: 6,
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(20),
              ),
            ),
            Expanded(
              child: _buildEventList(),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildEventList() {
    return ListView(
      children: _selectedEvents
          .map((event) => FutureBuilder<DocumentSnapshot>(
                future: Firestore.instance
                    .collection('services')
                    .document(event)
                    .get(),
                builder: (BuildContext context,
                    AsyncSnapshot<DocumentSnapshot> snapshot) {
                  return snapshot.connectionState == ConnectionState.done
                      ? Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Material(
                              elevation: 1,
                              borderRadius: BorderRadius.circular(10.0),
                              child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text(
                                            "${snapshot.data.data['startAt']}hrs",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                        Text(" - "),
                                        Text(
                                            "${snapshot.data.data['endAt']}hrs",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 20.0,
                                    ),
                                    FutureBuilder<DocumentSnapshot>(
                                      future: snapshot.data.data['from'].get(),
                                      builder: (BuildContext context,
                                          AsyncSnapshot<DocumentSnapshot>
                                              userSnapshot) {
                                        return userSnapshot.connectionState ==
                                                ConnectionState.done
                                            ? Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text("Nombre del cliente",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold)),
                                                  Text(userSnapshot
                                                      .data.data['name']),
                                                ],
                                              )
                                            : Text('');
                                      },
                                    ),
                                    SizedBox(
                                      height: 20.0,
                                    ),
                                    Text("Tipo de servicio:",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    Text(snapshot.data.data['type']),
                                    SizedBox(
                                      height: 20.0,
                                    ),
                                    Text("Establecimiento:",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    Text(snapshot.data.data['establishment']),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        FlatButton(
                                          color: HexColor('#05C2D3'),
                                          onPressed: () {},
                                          child: Text('LLegué'),
                                        ),
                                        FlatButton(
                                          color: HexColor('#05C2D3'),
                                          onPressed: () {},
                                          child: Text('Finalizar'),
                                        ),
                                        FlatButton(
                                          color: HexColor('#05C2D3'),
                                          onPressed: () {},
                                          child: Text('Cancelar'),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              )),
                        )
                      : Text('');
                },
              ))
          .toList(),
    );
  }
}
