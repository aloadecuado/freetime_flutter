import 'dart:io';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/pages/validate_phone.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/widgets/bottomSheetCamera.dart';
import 'package:path/path.dart' as Path;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/models/users_models_json.dart';
import 'package:free_time/src/ui/components/btn_base.dart';
import 'package:free_time/src/utils/utils.dart' as utils;
import 'package:free_time/src/utils/utils.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

const OPTIONS = [
  {
    "label": 'Camara',
    "icon": Icons.photo_camera,
    "value": ImageSource.camera,
    "isCancel": false,
  },
  {
    "label": 'Galería',
    "icon": Icons.photo_library,
    "value": ImageSource.gallery,
    "isCancel": false,
  },
  {
    "label": 'Cancelar',
    "icon": Icons.close,
    "value": null,
    "color": Colors.red,
    "isCancel": true,
  }
];

class ConfigurationPage extends StatefulWidget {
  @override
  _ConfigurationPageState createState() => _ConfigurationPageState();
}

class _ConfigurationPageState extends State<ConfigurationPage> {
  final _formKey = GlobalKey<FormState>();

  UsesJsonModel userss = new UsesJsonModel();
  final _key = GlobalKey<ScaffoldState>();
  TextEditingController _name = TextEditingController();
  TextEditingController _userName = TextEditingController();
  TextEditingController _identification = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _phone = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();

  final buttonSheet = ButtonSheetCamera(OPTIONS);

  bool _hidePass = false;
  bool _hidePass2 = false;
  String docType = "CC";

  File _foto;
  bool _guardando = false;
  String _fotoUrl;

  @override
  void initState() {
    // TODO: implement initState
    getData();
    super.initState();
  }

  void getData() {
    final user = Provider.of<UserProvider>(context, listen: false);
    Firestore.instance
        .collection('users')
        .document(user?.user?.uid)
        .get()
        .then((userDoc) {
      setState(() {
        _name.text = userDoc.data['name'];
        _userName.text = userDoc.data['userName'];
        _identification.text = userDoc.data['identification'];
        _email.text = userDoc.data['email'];
        _phone.text = userDoc.data['phone'];
        _fotoUrl = userDoc.data['image'];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Color(0xffF8F8F8),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              'Configuración',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xff000000),
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(10.0, 25, 10, 10),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    fotoEstado(user),
                    SizedBox(
                      width: 20.0,
                    ),
                    _nombreUser(),
                  ],
                ),
              ),
              SizedBox(
                height: 25.0,
              ),
              _crearDisponible(),
              _formulario(user),
            ],
          ),
        ),
      ),
    );
  }

  _formulario(user) {
    return Column(
      children: <Widget>[
        Column(
          children: <Widget>[
            SizedBox(
              height: 10.0,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
              child: Material(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.grey.withOpacity(0.2),
                elevation: 0.0,
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: ListTile(
                    title: TextFormField(
                      initialValue: userss.name,
                      controller: _name,
                      onChanged: (value) {
                        Firestore.instance
                            .collection('users')
                            .document(user?.user?.uid)
                            .updateData({'name': value});
                      },
                      decoration: InputDecoration(
                          hintText: "Nombre Completo",
                          hintStyle: TextStyle(
                              letterSpacing: 1.5,
                              fontFamily: "OpenSans-SemiBold"),
                          border: InputBorder.none),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "El campo es requerido";
                        }
                        return null;
                      },
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
              child: Material(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.grey.withOpacity(0.2),
                elevation: 0.0,
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: ListTile(
                    title: TextFormField(
                      controller: _userName,
                      onChanged: (value) {
                        Firestore.instance
                            .collection('users')
                            .document(user?.user?.uid)
                            .updateData({'userName': value});
                      },
                      decoration: InputDecoration(
                          hintText: "Nombre de Usuario",
                          hintStyle: TextStyle(
                              letterSpacing: 1.5,
                              fontFamily: "OpenSans-SemiBold"),
                          border: InputBorder.none),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "El campo es requerido";
                        }
                        return null;
                      },
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
              child: Material(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.grey.withOpacity(0.2),
                elevation: 0.0,
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 65),
                        child: TextFormField(
                          controller: _identification,
                          onChanged: (value) {
                            Firestore.instance
                                .collection('users')
                                .document(user?.user?.uid)
                                .updateData({'identification': value});
                          },
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              hintText: "Identificaciòn",
                              hintStyle: TextStyle(
                                  letterSpacing: 1.5,
                                  fontFamily: "OpenSans-SemiBold"),
                              border: InputBorder.none),

                          //onSaved: en espera ,
                          validator: (value) {
                            if (utils.esNumero(value)) {
                              //return "El campo es requerido";
                            } else {
                              return "Solo números";
                            }
                            return null;
                          },
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(-12, 0),
                        child: Container(
                          padding: EdgeInsets.only(
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Color(0xffD3D3D3))),
                          child: DropdownButton<String>(
                            value: docType,
                            icon: Icon(
                              Icons.arrow_drop_down,
                              color: Color(0xffD3D3D3),
                              size: 40,
                            ),
                            iconSize: 24,
                            elevation: 16,
                            underline: Container(
                              height: 2,
                              color: Colors.transparent,
                            ),
                            onChanged: (String newValue) {
                              setState(() {
                                docType = newValue;
                              });
                            },
                            items: <String>['CC', 'CE']
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
              child: Material(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.grey.withOpacity(0.2),
                elevation: 0.0,
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: ListTile(
                    title: TextFormField(
                      controller: _email,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          hintText: "Correo Electrònico",
                          hintStyle: TextStyle(
                              letterSpacing: 1.5,
                              fontFamily: "OpenSans-SemiBold"),
                          border: InputBorder.none),
                      validator: (value) {
                        if (value.isEmpty) {
                          Pattern pattern =
                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          RegExp regex = new RegExp(pattern);
                          if (!regex.hasMatch(value))
                            return 'Asegurese de colocar un email vàlido';
                          else
                            return null;
                        }
                      },
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
              child: Material(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.grey.withOpacity(0.2),
                elevation: 0.0,
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: ListTile(
                    title: TextFormField(
                      //initialValue: userss.telefono.toString(),
                      //onSaved: (value)=> userss.telefono = double.parse(value),
                      onChanged: (value) {
                        Firestore.instance
                            .collection('users')
                            .document(user?.user?.uid)
                            .updateData({'phone': value});
                      },
                      onEditingComplete: () {
                        changeScreen(
                            context,
                            ValidatePhone(
                                user: user?.user?.uid, phone: _phone.text));
                      },
                      controller: _phone,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                          hintText: "Teléfono",
                          hintStyle: TextStyle(
                              letterSpacing: 1.5,
                              fontFamily: "OpenSans-SemiBold"),
                          border: InputBorder.none),
                      validator: (value) {
                        if (utils.esNumero(value)) {
                          return null;
                        } else {
                          return 'El campo es requerido';
                        }
                      },
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
              child: Material(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.grey.withOpacity(0.2),
                elevation: 0.0,
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: ListTile(
                    title: TextFormField(
                      controller: _password,
                      obscureText: !_hidePass,
                      decoration: InputDecoration(
                          hintText: "Contraseña",
                          hintStyle: TextStyle(
                              letterSpacing: 1.5,
                              fontFamily: "OpenSans-SemiBold"),
                          border: InputBorder.none),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Este campo es requerido";
                        } else if (value.length < 6) {
                          return "Debe ser mas de 6 dìgitos";
                        }
                        return null;
                      },
                    ),
                    trailing: IconButton(
                        icon: Icon(_hidePass
                            ? Icons.visibility
                            : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            _hidePass = !_hidePass;
                          });
                        }),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
              child: Material(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.grey.withOpacity(0.2),
                elevation: 0.0,
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: ListTile(
                    title: TextFormField(
                      controller: _confirmPassword,
                      obscureText: !_hidePass2,
                      decoration: InputDecoration(
                          hintText: "Repite  Contraseña",
                          hintStyle: TextStyle(
                              letterSpacing: 1.5,
                              fontFamily: "OpenSans-SemiBold"),
                          border: InputBorder.none),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Este campo es requerido";
                        } else if (value.length < 6) {
                          return "Debe ser mas de 6 dìgitos";
                        }
                        return null;
                      },
                    ),
                    trailing: IconButton(
                        icon: Icon(_hidePass2
                            ? Icons.visibility
                            : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            _hidePass2 = !_hidePass2;
                          });
                        }),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              height: 50,
              width: MediaQuery.of(context).size.width * 0.87,
              margin: const EdgeInsets.only(bottom: 25),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                  colors: [
                    MyColors.primaryColor,
                    HexColor('#BE7CF6'),
                  ],
                  begin: FractionalOffset.bottomLeft,
                  end: FractionalOffset.bottomRight,
                ),
              ),
              child: BtnBase(
                texto: 'Actualizar',
                ontap: () {
                  showDialog(
                    context: context,
                    child: AlertDialog(
                      title: Text('Datos guardados'),
                      actions: <Widget>[
                        FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text('Cerrar'),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget fotoEstado(user) {
    return GestureDetector(
      onTap: _pickImage,
      child: Stack(
        children: <Widget>[
          CircleAvatar(
            radius: 45,
            backgroundColor: Colors.transparent,
            backgroundImage: _fotoUrl != null
                ? NetworkImage(_fotoUrl)
                : AssetImage('assets/icon/no-usuario.png'),
          ),
          Padding(
            padding: EdgeInsets.only(
              top: 60,
              left: 60,
            ),
            child: Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                color: Colors.black,
                shape: BoxShape.circle,
              ),
              child: Icon(
                Icons.camera_alt,
                color: Colors.white,
                size: 18,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _nombreUser() {
    return Flexible(
      flex: 1,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Flexible(
            flex: 1,
            fit: FlexFit.loose,
            child: Text(
              _name.text,
              maxLines: 1,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Flexible(
            flex: 1,
            fit: FlexFit.loose,
            child: Text(
              _email.text,
              style: TextStyle(
                fontSize: 14,
                color: Colors.black38,
                fontWeight: FontWeight.w200,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _submit() {
    if (!_formKey.currentState.validate()) return;
    //_formKey.currentContext.save(); //error en save
    setState(() {
      _guardando = false;
    });
    if (_foto != null) {}
  }

  void _pickImage() async {
    final user = Provider.of<UserProvider>(context, listen: false);
    final imageSource = await buttonSheet.show(context);
    if (imageSource != null) {
      final file = await ImagePicker.pickImage(source: imageSource);
      if (file != null) {
        if (imageSource == ImageSource.gallery) {
          final result = await FlutterImageCompress.compressAndGetFile(
            file.absolute.path,
            file.absolute.path,
            quality: 60,
          );
          uploadFile(result, user);
          return;
        }
        uploadFile(file, user);
      }
    }
  }

  Future uploadFile(result, user) async {
    if (result != null) {
      final name = getUID(8);
      StorageReference storageReference = FirebaseStorage.instance.ref().child(
          'users/${user.user.uid}/photos/${name}_${Path.basename(result.path)}');
      StorageUploadTask uploadTask = storageReference.putFile(result);
      await uploadTask.onComplete;
      print('File Uploaded');
      storageReference.getDownloadURL().then((fileURL) {
        Firestore.instance
            .collection('users')
            .document(user.user.uid)
            .updateData({'image': fileURL});
        setState(() {
          _fotoUrl = fileURL;
        });
      });
    }
  }

  Widget _crearDisponible() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        FlatButton(
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10),
          ),
          child: Text('Activar',
              style: TextStyle(
                color: userss.disponible ? Colors.white : Colors.grey[800],
                fontWeight: FontWeight.w600,
                fontSize: 18,
              )),
          color: userss.disponible ? HexColor('#05C2D3') : Colors.white,
          onPressed: () {
            setState(() {
              userss.disponible = true;
            });
          },
        ),
        FlatButton(
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10),
          ),
          child: Text(
            'Desactivar',
            style: TextStyle(
              color: !userss.disponible ? Colors.white : Colors.grey[800],
              fontWeight: FontWeight.w600,
              fontSize: 18,
            ),
          ),
          color: !userss.disponible ? HexColor('#05C2D3') : Colors.white,
          onPressed: () {
            setState(() {
              userss.disponible = false;
            });
          },
        )
      ],
    );
    /* return SwitchListTile(
      value: userss.disponible,
      title: Text('Disponible'),
      activeColor: MyColors.pink,
      onChanged: (value)=>setState((){
        userss.disponible = value;
      }) ,
    ); */
  }
}
