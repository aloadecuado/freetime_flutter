import 'package:flutter/material.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/utils/color.dart' as prefix0;
import 'package:free_time/src/widgets/detalles_serv_page.dart';

class HistorialServicio extends StatelessWidget {
  const HistorialServicio({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Color.fromRGBO(248, 248, 248, 1),
        leading: IconButton(
          icon: Icon(
            Icons.close,
            color: prefix0.HexColor('#07C1D3'),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              'Historial de servicios',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xff000000),
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 20),
        child: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: TabBar(
                labelColor: Color.fromRGBO(38, 38, 40, 1),
                unselectedLabelColor: Color.fromRGBO(151, 151, 151, 1),
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.grey[200],
                ),
                tabs: [
                  Tab(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("Realizados"),
                    ),
                  ),
                  Tab(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("Recibidos"),
                    ),
                  ),
                ]),
            body: TabBarView(children: [
              Center(
                child: Text('Aún no hay servicios'),
              ),
              Center(
                child: Text('Aún no hay servicios'),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}

class HexColor {}
