import 'package:flutter/material.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/components/buttons/facebook_register.dart';
import 'package:free_time/src/ui/components/buttons/google_register.dart';
import 'package:free_time/src/ui/components/imgBackground.dart';
import 'package:free_time/src/widgets/text_input_login.dart';
import 'package:provider/provider.dart';
import '../../config.dart';
import 'register_page.dart';

class LoginPage extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final _key = GlobalKey<ScaffoldState>();
  final TextGlobal textGlobal = TextGlobal();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);
    return Scaffold(
      key: _key,
      body: Stack(
        children: <Widget>[
          ImagenFondo(),
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Form(
              key: _formKey,
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 30),
                    child: Center(
                      child: Image.asset(
                        'assets/icon/logoLogin.png',
                        fit: BoxFit.contain,
                        height: 120,
                        width: 150,
                      ),
                    ),
                  ),
                  Text(
                    'Ingresa con tus redes:',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.justify,
                  ),
                  SizedBox(height: 15),
                  FacebookRegisterButton(),
                  SizedBox(height: 20),
                  GoogleRegisterButton(),
                  SizedBox(
                    height: 35,
                  ),
                  Text(
                    'Ingresa con tu usuario:',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.justify,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextInputLogin(
                    controller: _email,
                    labelText: 'Email',
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) => (value.isEmpty)
                        ? "Por favor Intodruzca su email"
                        : null,
                  ),
                  SizedBox(height: 20),
                  TextInputLogin(
                    controller: _password,
                    obscureText: true,
                    labelText: 'Contraseña',
                    validator: (value) => (value.isEmpty)
                        ? "Por favor introduzca su contraseña"
                        : null,
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  user.status == Status.Authenticating
                      ? Center(child: CircularProgressIndicator())
                      : SizedBox(
                          width: double.infinity,
                          height: 50,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            color: MyColors.pink,
                            textColor: Colors.white,
                            child: Text(
                              "INGRESAR",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                            onPressed: () async {
                              if (_formKey.currentState.validate()) {
                                if (!await user.signIn(
                                  _email.text,
                                  _password.text,
                                  context,
                                ))
                                  _key.currentState.showSnackBar(
                                    SnackBar(
                                      content: Text("Algo esta mal!!!"),
                                    ),
                                  );
                              }
                            },
                          ),
                        ),
                  Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.fromLTRB(0, 40, 0, 20),
                    child: GestureDetector(
                      child: RichText(
                        text: TextSpan(
                          text: "${textGlobal.olvidasteTu} ",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                          ),
                          children: <TextSpan>[
                            TextSpan(
                              text: textGlobal.restaurar,
                              style: TextStyle(
                                color: MyColors.blue,
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      onTap: () => changeScreen(context, RegisterPage()),
                      child: RichText(
                        text: TextSpan(
                          text: "¿Aún no tienes una cuenta? ",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                          ),
                          children: <TextSpan>[
                            TextSpan(
                              text: "Regístrate",
                              style: TextStyle(
                                color: MyColors.blue,
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
