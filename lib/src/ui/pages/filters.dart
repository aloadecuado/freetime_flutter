import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/ui/components/btn_base.dart';
import 'package:free_time/src/ui/pages/filters_result.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:geolocator/geolocator.dart';

const List<Map<String, dynamic>> GENDERS = [
  {"label": "Mujeres", "value": "Femenino"},
  {"label": "Hombres", "value": "Masculino"},
  {"label": "Ambos", "value": "All"},
];

class Filters extends StatefulWidget {
  Filters({Key key}) : super(key: key);

  @override
  _FiltersState createState() => _FiltersState();
}

class _FiltersState extends State<Filters> {
  DocumentReference _category;
  String _gender;

  TextEditingController _address = new TextEditingController();

  int _distance = 1;
  int _minAge = 18;
  int _maxAge = 30;

  List<DocumentSnapshot> _cats = [];

  @override
  void initState() {
    super.initState();
    _address.text = 'Chapinero Norte';
    // getUserLocation();
    getCategories();
  }

  getUserLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print(position);
  }

  getCategories() async {
    QuerySnapshot categories = await Firestore.instance
        .collection('categories')
        .where('active', isEqualTo: true)
        .getDocuments();
    setState(() {
      _cats = categories.documents;
    });
  }

  @override
  Widget build(BuildContext context) {
    final double bottom = MediaQuery.of(context).viewPadding.bottom;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Filtros',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.only(top: 20, bottom: 10),
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            child: Text(
              'Elige una categoría',
              style: TextStyle(
                color: Color(0xff363636),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Container(
            height: 55.0,
            alignment: Alignment.center,
            child: ListView(
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              scrollDirection: Axis.horizontal,
              children: _cats
                  .map<Widget>((DocumentSnapshot category) => Container(
                        child: FlatButton(
                          color: _category == category.reference
                              ? MyColors.primaryColor
                              : MyColors.gray,
                          onPressed: () {
                            updateCategory(category.reference);
                          },
                          child: Text(
                            category.data['name'],
                            style: TextStyle(
                              color: _category == category.reference
                                  ? Colors.white
                                  : Colors.grey,
                              fontWeight: _category == category.reference
                                  ? FontWeight.w800
                                  : FontWeight.w200,
                            ),
                          ),
                        ),
                      ))
                  .toList(),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(15, 30, 15, 0),
            child: Text(
              'Quiero conectarme con',
              style: TextStyle(
                color: Color(0xff363636),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          genderFilter(),
          Padding(
            padding: EdgeInsets.fromLTRB(15, 35, 15, 10),
            child: Text(
              'Edades de: $_minAge a $_maxAge',
              style: TextStyle(color: Color(0xff363636), fontSize: 17.0),
            ),
          ),
          RangeSlider(
            activeColor: MyColors.primaryColor,
            min: 18.0,
            max: 50.0,
            onChanged: (RangeValues value) {
              setState(() {
                _minAge = value.start.toInt();
                _maxAge = value.end.toInt();
              });
            },
            values: RangeValues(_minAge.toDouble(), _maxAge.toDouble()),
          ),
          SizedBox(
            height: 20.0,
          ),
        ],
      ),
      bottomNavigationBar: Container(
        color: Colors.white,
        padding: EdgeInsets.only(top: 15, bottom: bottom, left: 15, right: 15),
        child: Container(
          height: 50,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            gradient: LinearGradient(
              colors: [
                MyColors.primaryColor,
                HexColor('#BE7CF6'),
              ],
              begin: FractionalOffset.bottomLeft,
              end: FractionalOffset.bottomRight,
            ),
          ),
          child: BtnBase(
            texto: 'Buscar',
            ontap: () {
              changeScreen(
                context,
                FilterResult(category: _category, gender: _gender),
              );
            },
          ),
        ),
      ),
    );
  }

  void updateGender(gender) {
    setState(() {
      _gender = gender;
    });
  }

  void updateCategory(category) {
    setState(() {
      _category = category;
    });
  }

  Widget genderFilter() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: List.generate(
          GENDERS.length,
          (index) => Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: Container(
              height: 50,
              child: FlatButton(
                color: _gender == GENDERS[index]['value']
                    ? MyColors.primaryColor
                    : MyColors.gray,
                onPressed: () {
                  updateGender(GENDERS[index]['value']);
                },
                child: Text(
                  GENDERS[index]['label'],
                  style: TextStyle(
                    color: _gender == GENDERS[index]['value']
                        ? Colors.white
                        : Colors.grey,
                    fontWeight: _gender == GENDERS[index]['value']
                        ? FontWeight.w800
                        : FontWeight.w200,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
