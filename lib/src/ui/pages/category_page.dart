import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/pages/user_profile.dart';
import 'package:free_time/src/utils/color.dart';

class CategoryPage extends StatelessWidget {
  final String _imageUrl;
  final String _texto;
  final String _category;
  final user;

  CategoryPage(this._texto, this._imageUrl, this._category, this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false, // Don't show the leading button
        elevation: 0.0,
        backgroundColor: Color(0xffF8F8F8),
        title: Text(
          "",
          style: TextStyle(color: HexColor('#262628')),
        ),
        leading: Row(
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: HexColor('#00C3D2'),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: FutureBuilder(
          future: Firestore.instance
              .collection('users')
              .where('categories',
                  arrayContains: Firestore.instance
                      .collection('categories')
                      .document(_category))
              .getDocuments(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (!snapshot.hasData) {
              return Container();
            }
            return snapshot.connectionState == ConnectionState.done
                ? ListView(
                    children: snapshot.data.documents.map<Widget>((user) {
                      if (user["active"] == null) {
                        return Container();
                      }

                      if (user["active"] == false) {
                        return Container();
                      }
                      if (this.user.user.email == user["email"]) {
                        return Container();
                      }
                      return GestureDetector(
                        onTap: () {
                          changeScreen(context, UserProfile(user: user));
                        },
                        child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: 70.0,
                                height: 70.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100.0),
                                  image: DecorationImage(
                                    image: user.data['image'] != null
                                        ? NetworkImage(user.data['image'])
                                        : AssetImage(
                                            'assets/icon/no-usuario.png',
                                          ),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 20.0,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.6,
                                    child: Text(
                                      user.data['name'],
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20.0,
                                    height: 15,
                                  ),
                                  FutureBuilder(
                                    future: Firestore.instance
                                        .collection('categories')
                                        .document(_category)
                                        .get(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot snapshot) {
                                      return snapshot.connectionState ==
                                              ConnectionState.done
                                          ? Container(
                                              padding: EdgeInsets.all(6.0),
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: HexColor('#00C3D2'),
                                                  width: 1.0,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                  5,
                                                ),
                                              ),
                                              child: Text(
                                                snapshot.data['name'],
                                                style: TextStyle(
                                                  color: HexColor('#00C3D2'),
                                                ),
                                              ),
                                            )
                                          : Text('');
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      );
                    }).toList(),
                  )
                : Text('');
          },
        ),
      ),
    );
  }
}
