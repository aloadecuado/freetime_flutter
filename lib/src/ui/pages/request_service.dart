import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/Repository/Repository/NotificationRepository.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/components/btn_base.dart';
import 'package:free_time/src/ui/components/custom_dialog.dart';
import 'package:free_time/src/ui/pages/home_page.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/widgets/pop_overs_alert.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class RequestService extends StatefulWidget {
  RequestService({Key key, this.service, this.user}) : super(key: key);

  final DocumentSnapshot service;
  final DocumentSnapshot user;

  @override
  _RequestServiceState createState() => _RequestServiceState();
}

class _RequestServiceState extends State<RequestService> {
  bool _terms = false;
  String _selectedDate;
  DateTime _date;
  TimeOfDay _startAt = new TimeOfDay.now();
  TimeOfDay _endAt = new TimeOfDay(hour: 12, minute: 30);
  int _ammount = 0;
  ValueChanged<TimeOfDay> selectTime;
  List<dynamic> _sites = [];
  List<String> _typeDate = [
    'Presencial',
    'Virtual',
  ];
  String _selectedTypeDate;
  String _siteSelected;

  TextGlobal _textGlobal = TextGlobal();
  int diffHour = 0;

  @override
  void initState() {
    super.initState();
    getSites();
  }

  var _key;
  showError(String message) {
    _key.currentState.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  getSites() async {
    if (widget.service.documentID == 'meet_service') {
      print('meet_service');
      for (var i = 0; i < widget.service.data['establishment'].length; i++) {
        var establishment = await widget.service.data['establishment'][i].get();
        setState(() {
          _sites.add(establishment.data['name']);
        });
      }
    } else {
      setState(() {
        _sites = widget.user.data['services_sites'] ?? [];
      });
    }
  }

  void showRequestDialog(context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) {
          return CustomAlertDialog(
            key: _key,
            contentPadding: EdgeInsets.all(0.0),
            content: Container(
              height: 350.0,
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 200.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/background/modal.png'),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: Padding(
                        padding: EdgeInsets.all(20.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Envío de solicitud',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        )),
                  ),
                  Container(
                    child: Positioned(
                      top: 100.0,
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: 30.0,
                              ),
                              Text(
                                'Hemos enviado tu Solicitud de servicio a ${widget.user.data['name']}.',
                                style: TextStyle(
                                  fontSize: 16.0,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                'Mantente atento a recibir un mensaje con la respuesta.',
                                style: TextStyle(
                                  fontSize: 16.0,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0)),
                                color: HexColor('#FF62A5'),
                                onPressed: () {
                                  changeScreenReplacement(
                                      context, HomePage(user: widget.user));
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0, vertical: 12.0),
                                  child: Text(
                                    'Ok, Entendido',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 17.0),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);

    return Scaffold(
      resizeToAvoidBottomPadding: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: HexColor('#00C3D2'),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(
              'Solicitar servicio',
              style: TextStyle(
                  color: HexColor('#00C3D2'),
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            onPressed: () async {
              // Solicitar servicio
              print(validateServiceData());
              if (validateServiceData()) {
                await Firestore.instance
                    .collection('services')
                    .document()
                    .setData({
                  'date': _date,
                  'startAt': "${_startAt.hour}:${_startAt.minute}",
                  'endAt': "${_endAt.hour}:${_endAt.minute}",
                  'ammount': _ammount,
                  'establishment': _siteSelected,
                  'to': Firestore.instance
                      .collection('users')
                      .document(widget.user.documentID),
                  'from': Firestore.instance
                      .collection('users')
                      .document(user.user.uid),
                  'paymentMethod': 'Efectivo',
                  'status': 'requested',
                  'type': widget.service.data['service_name']
                });

                NotificationRepository().sendNotification(
                    _textGlobal.SERVICE_RESUEST,
                    _textGlobal.SERVICE_RESUEST,
                    _textGlobal.SERVICE_RESUEST,
                    _textGlobal.SERVICE_RESUEST,
                    widget.user["token"]);

                showRequestDialog(context);
              }
            },
          ),
        ],
        elevation: 0.0,
        backgroundColor: Color(0xffF8F8F8),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            Row(
              children: <Widget>[
                Checkbox(
                  onChanged: (bool value) {
                    setState(() {
                      _terms = value;
                    });
                  },
                  value: _terms,
                  activeColor: HexColor('#FFFFFF'),
                  checkColor: HexColor('#5EAE57'),
                ),
                Flexible(
                  child: Text(
                    'Acepto terminos y condiciones FreeTime',
                    style: TextStyle(fontSize: 16, color: HexColor('#0D86F5')),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              'Completa los datos para solicitar tu servicio',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Tipo de reunión',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Container(
                            padding: EdgeInsets.fromLTRB(
                              16,
                              0,
                              16,
                              0,
                            ),
                            child: DropdownButton(
                              underline: Text(''),
                              hint: Text(
                                'Escoge el tipo de reunión',
                              ),
                              value: _selectedTypeDate,
                              onChanged: (newValue) {
                                setState(() {
                                  _selectedTypeDate = newValue;
                                });
                              },
                              items: _typeDate.map((value) {
                                return DropdownMenuItem(
                                  child: new Text(value),
                                  value: value.toString(),
                                );
                              }).toList(),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Fecha',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: GestureDetector(
                            onTap: () {
                              _selectDate(context);
                            },
                            child: Container(
                              child: Text(_selectedDate ??
                                  'Escoge la fecha de la reunión'),
                              padding:
                                  EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Horario',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: GestureDetector(
                            onTap: () {
                              _selectTime('startAt');
                            },
                            child: Container(
                              child: Text((_startAt != null || _endAt != null)
                                  ? '${_startAt.format(context)}-${_endAt.format(context)}'
                                  : 'dd/MM/AAA' + "horas: ${diffHour}"),
                              padding:
                                  EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      children: _selectedTypeDate == 'Presencial'
                          ? <Widget>[
                              Expanded(
                                child: Text(
                                  'Lugar',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(
                                    16,
                                    0,
                                    16,
                                    0,
                                  ),
                                  child: DropdownButton<String>(
                                    underline: Text(''),
                                    hint: Text(
                                        _siteSelected ?? 'Selecciona un lugar'),
                                    items: _sites.map((value) {
                                      return new DropdownMenuItem<String>(
                                        value: value.toString(),
                                        child: new Text(
                                          value,
                                          textAlign: TextAlign.center,
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        _siteSelected = value;
                                      });
                                    },
                                  ),
                                ),
                              ),
                            ]
                          : [],
                    ),
                    SizedBox(
                      height: _selectedTypeDate == 'Presencial' ? 10 : 0,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Servicio',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: GestureDetector(
                            onTap: () {},
                            child: Container(
                              child: Text(
                                widget.service.data['service_name'],
                              ),
                              padding: EdgeInsets.fromLTRB(
                                16.0,
                                16.0,
                                16.0,
                                16.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Monto',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Container(
                            child: Text(
                                "\$${(int.parse(widget.service['price']) * diffHour)}"),
                            padding: EdgeInsets.fromLTRB(
                              16.0,
                              16.0,
                              16.0,
                              16.0,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            BtnBase(
              texto: 'Solicitar servicio',
              color: HexColor('#00C3D2'),
              ontap: () async {
                if (!_terms) {
                  showError(_textGlobal.acepteTerminos);
                  return;
                }
                // Solicitar servicio
                print(validateServiceData());
                if (validateServiceData()) {
                  DocumentReference documentReference = await Firestore.instance
                      .collection('services')
                      .document();
                  documentReference.setData({
                    'date': _date,
                    'startAt': "${_startAt.hour}:${_startAt.minute}",
                    'endAt': "${_endAt.hour}:${_endAt.minute}",
                    'ammount': _ammount,
                    'establishment': _siteSelected,
                    'to': Firestore.instance
                        .collection('users')
                        .document(widget.user.documentID),
                    'from': Firestore.instance
                        .collection('users')
                        .document(user.user.uid),
                    'paymentMethod': 'Efectivo',
                    'status': 'requested',
                    'type': widget.service.data['service_name']
                  });
                  NotificationRepository().sendNotification(
                      _textGlobal.tienesUn,
                      _textGlobal.servicioDe + user.user.displayName,
                      _textGlobal.SERVICE_RESUEST,
                      documentReference.documentID,
                      widget.user["token"]);
                  showRequestDialog(context);
                } else {
                  /*showDialog(
                      context: context,
                      child: AlertDialog(
                        title: Text('Por favor revisa que todos los datos estén completos'),
                        actions: <Widget>[
                          FlatButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text('Cerrar'),
                          )
                        ],
                      )
                    );*/

                  PopOversAlert().showAlertTitleSubTitleYes(
                      context,
                      _textGlobal.informacion,
                      _textGlobal.porFavor,
                      _textGlobal.cerrar, () {
                    Navigator.of(context).pop();
                  });
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  bool validateServiceData() {
    return _selectedDate != null &&
        _startAt != null &&
        _endAt != null &&
        _ammount != null &&
        (_selectedTypeDate == 'Presencial'
            ? _siteSelected != null
            : _siteSelected == null) &&
        _selectedTypeDate != null;
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      firstDate: DateTime.now(),
      lastDate: DateTime(2101),
      initialDate: DateTime.now(),
    );
    if (picked != null && picked.toString() != _selectedDate)
      setState(() {
        _selectedDate = DateFormat("dd-MM-yyyy").format(picked);
        _date = picked;
      });
    /*Firestore.instance.collection('users').document(widget.user.user.uid).updateData({
        'birthday': picked
      });*/
  }

  Future<void> _selectTime(type) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: _startAt);
    setState(() {
      if (type == 'startAt') {
        _startAt = picked ?? new TimeOfDay(hour: 12, minute: 30);
        _selectTime('_endAt');
      } else {
        _endAt = picked ?? new TimeOfDay(hour: 12, minute: 30);
        var hours = _endAt.hour - _startAt.hour;
        _ammount = hours * int.parse(widget.service.data['price']);

        diffHour = _endAt.hour - _startAt.hour;
      }
    });
  }
}
