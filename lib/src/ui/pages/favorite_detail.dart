import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:flutter_tindercard/flutter_tindercard.dart';
import 'package:free_time/src/widgets/scrollImagesTinder.dart';

class FavoriteDetail extends StatefulWidget {
  FavoriteDetail({Key key, this.user, this.currUser})
      : super(key: key);

  final user;
  final currUser;

  @override
  _FavoriteDetailState createState() => _FavoriteDetailState();
}

class _FavoriteDetailState extends State<FavoriteDetail> {
  List<DocumentSnapshot> listUsers = [];
  CardController controller; //Use this to trigger swap.
  List _favorites = [];

  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      getFavorites();
      getNewUser();
    });
  }

  getNewUser() async {
    var result = await Firestore.instance.collection('users').limit(10).getDocuments();
    setState(() {
      listUsers = result.documents;
    });
  }

  void getFavorites() async {
    var u = Provider.of<UserProvider>(context);
    DocumentSnapshot user = await Firestore.instance.collection('users').document(u.user.uid).get();
    print(user.data['favorites']);
    setState(() {
      _favorites = user.data['favorites'] != null ? user.data['favorites'] : [];
    });
  }

  saveFavoritesUser({userItem, user}){
    DocumentReference favRef = Firestore.instance.collection('users').document(userItem.documentID);
    Firestore.instance.collection('users').document(user.user.uid).updateData({'favorites': FieldValue.arrayUnion([favRef])});
    getFavorites();
  }

  deleteFavoritesUser({userItem, user}){
    DocumentReference favRef = Firestore.instance.collection('users').document(userItem.documentID);
    Firestore.instance.collection('users').document(user.user.uid).updateData({'favorites': FieldValue.arrayRemove([favRef])});
    getFavorites();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xffF8F8F8),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: HexColor('#FF689A')),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Container(
            child: TinderSwapCard(
                orientation: AmassOrientation.BOTTOM,
                totalNum:
                    listUsers != null ? listUsers.length : 0,
                swipeEdge: 1.0,
                maxWidth: MediaQuery.of(context).size.width * 0.9,
                maxHeight: 700,
                minWidth: MediaQuery.of(context).size.width * 0.8,
                minHeight: MediaQuery.of(context).size.width * 0.8,
                cardBuilder: (context, index) => Material(
                    elevation: 2.0,
                    borderRadius: BorderRadius.circular(20.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                              height: 400.0,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(20.0),
                                  topLeft: Radius.circular(20.0),
                                ),
                              ),
                              child: ScrollImagesTinder(
                                  item: listUsers[index])),
                          ButtonOption(
                              userItem: listUsers[index], user: user),
                          Text(listUsers[index].data['name'],
                              style: TextStyle(
                                  fontSize: 19.0, fontWeight: FontWeight.bold)),
                          Text(
                            listUsers[index].data['profesion_type'] ??
                                '',
                            style: TextStyle(color: Colors.grey),
                          )
                        ],
                      ),
                    )),
                cardController: controller = CardController(),
                swipeUpdateCallback:
                    (DragUpdateDetails details, Alignment align) {
                  /// Get swiping card's alignment
                  if (align.x < 0) {
                    //Card is LEFT swiping
                  } else if (align.x > 0) {
                    //Card is RIGHT swiping
                  }
                },
                swipeCompleteCallback:
                    (CardSwipeOrientation orientation, int index) {
                  /// Get orientation & index of swiped card!
                })),
      ),
    );
  }

  Color favoriteColor(userID) {
    DocumentReference favRef =
        Firestore.instance.collection('users').document(userID);
    if (_favorites.indexOf(favRef) != -1) {
      return MyColors.pink;
    } else {
      return Color(0xff363636);
    }
  }

  Widget ButtonOption({userItem, user}) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 50,
            child: Container(
              alignment: Alignment.centerRight,
              padding: EdgeInsets.only(right: 15),
              child: buttonWidget(
                userItem: userItem,
                user: user,
                icon: Icon(Icons.close, size: 35),
                handledButton: () {},
              ),
              transform: Matrix4.translationValues(0.0, -20.0, 0.0),
            ),
          ),
          Expanded(
            flex: 50,
            child: Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 15),
              child: buttonWidget(
                  userItem: userItem,
                  user: user,
                  icon: Icon(Icons.star, color: userItem.data['isFavorite'] == true ? Colors.blue : Colors.pinkAccent, size: 35),
                  handledButton: () {
                    if(userItem.data['isFavorite'] == true){
                      deleteFavoritesUser(user: user, userItem: userItem);
                      userItem.data['isFavorite'] = false;
                    }
                    else{
                      saveFavoritesUser(user: user, userItem: userItem);
                      userItem.data['isFavorite'] = true;
                    }
                    setState(() {});
                  }),
              transform: Matrix4.translationValues(0.0, -20.0, 0.0),
            ),
          )
        ],
      ),
    );
  }

  Widget buttonWidget({userItem, user, Function handledButton, Icon icon}) {
    return Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(40),
      child: GestureDetector(
        onTap: () => handledButton(),
        child: Container(
          padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
          decoration:
              BoxDecoration(color: Colors.white, shape: BoxShape.circle),
          child: icon,
        ),
      ),
    );
  }
}
