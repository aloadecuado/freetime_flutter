import 'package:flutter/material.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/Repository/Repository/PanicButtonRepository.dart';
import 'package:free_time/src/models/GeneralObjectResponse.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/components/PanicButton/PanicButton.dart';
import 'package:free_time/src/widgets/box_profile.dart';
import 'package:free_time/src/ui/UserProfile/Widget/box_profile_menu.dart';
import 'package:free_time/src/widgets/pop_overs_alert.dart';
import 'package:provider/provider.dart';

import '../../Values/texts_globals.dart';

class ProfileEditPage extends StatefulWidget {
  const ProfileEditPage({Key key, this.user}) : super(key: key);

  final user;
  @override
  _ProfileEditPage createState() => _ProfileEditPage();
}

class _ProfileEditPage extends State<ProfileEditPage> {
  TextGlobal _textGlobal = TextGlobal();
  bool isProgress = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          _crearFondo(context),
          _crearPantalla(context),
        ],
      ),
    );
  }

  Widget _crearPantalla(BuildContext context) {
    final user = Provider.of<UserProvider>(context);
    final double paddingTop = MediaQuery.of(context).viewPadding.top;
    return SingleChildScrollView(
      padding: EdgeInsets.only(top: paddingTop + 50),
      child: Column(
        children: <Widget>[
          BoxProfile(user: user),
          SizedBox(height: 20.0),
          BoxprofileMenu(
            user: user,
          ),
          SizedBox(height: 22.0),
          (!isProgress)
              ? PanicButton(
                  width: MediaQuery.of(context).size.width * 0.90,
                  buttonText: _textGlobal.botonDe,
                  onGetLongPressed: () async {
                    setState(() {
                      isProgress = true;
                    });
                    PanicButtonRepository panicButtonRepository =
                        PanicButtonRepository();
                    GeneralObjectResponse ge = await panicButtonRepository
                        .sendPanicMesssage(user.user.uid);

                    setState(() {
                      isProgress = false;
                    });
                    if (ge.status == "200") {
                      PopOversAlert().showAlertTitleSubTitleYes(
                          context,
                          _textGlobal.informacion,
                          ge.sendEmailPanicResponse.sendEmail,
                          _textGlobal.aceptar, () {
                        Navigator.of(context).pop();
                      });
                    } else {
                      PopOversAlert().showAlertTitleSubTitleYes(
                          context,
                          _textGlobal.error,
                          ge.status,
                          _textGlobal.aceptar, () {
                        Navigator.of(context).pop();
                      });
                    }
                  },
                )
              : CircularProgressIndicator()
        ],
      ),
    );
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.25,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            HexColor('#BE7CF6'),
            HexColor('#00C3D2'),
          ],
          begin: FractionalOffset.bottomLeft,
          end: FractionalOffset.bottomRight,
        ),
      ),
    );
  }
}
