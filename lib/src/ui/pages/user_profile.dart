import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/Repository/Api/AddAndRemoveFavoriteApi.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/models/UserModel.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/components/AddFavorite.dart';
import 'package:free_time/src/ui/pages/request_service.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/widgets/show_image_zoom.dart';
import 'package:intl/intl.dart';
import 'package:link/link.dart';
import 'package:provider/provider.dart';
import 'package:photo_view/photo_view.dart';

class UserProfile extends StatefulWidget {
  UserProfile({Key key, this.user}) : super(key: key);

  final DocumentSnapshot user;
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  bool _showImages = true;
  DocumentSnapshot _serviceSelected;
  List<DocumentSnapshot> _services = [];

  bool _isFavorite = false;
  List _favorites = [];
  DocumentSnapshot currentUser;
  DocumentSnapshot serviceUser;

  @override
  void initState() {
    super.initState();
    getServices();
    getCurrentUser();
    getFavorites();
  }

  AddAndRemoveFavoriteApi addAndRemoveFavoriteApi;

  void getCurrentUser() async {
    final user = Provider.of<UserProvider>(context, listen: false);
    DocumentSnapshot documentSnapshot = await Firestore.instance
        .collection('users')
        .document(user.user.uid)
        .get();
    DocumentSnapshot documentSnapshotServer = await Firestore.instance
        .collection('users')
        .document(widget.user.documentID)
        .get();

    setState(() {
      this.currentUser = documentSnapshot;
      this.serviceUser = documentSnapshotServer;
    });
  }

  void getFavorites() async {
    var u = Provider.of<UserProvider>(context, listen: false);
    DocumentSnapshot user = await Firestore.instance
        .collection('users')
        .document(u?.user?.uid)
        .get();
    print(user?.data['favorites']);
    setState(() {
      _favorites = user.data['favorites'] != null ? user.data['favorites'] : [];
    });
    DocumentReference favRef =
        Firestore.instance.collection('users').document(widget.user.documentID);
    if (_favorites.indexOf(favRef) != -1) {
      setState(() {
        _isFavorite = true;
      });
    }
  }

  getServices() async {
    var data = await Firestore.instance
        .collection('users')
        .document(widget.user.documentID)
        .collection('services')
        .getDocuments();
    setState(() {
      _services = data.documents;
      _serviceSelected = data.documents[0];
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    addAndRemoveFavoriteApi = AddAndRemoveFavoriteApi();
    final user = Provider.of<UserProvider>(context);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: HexColor('#FFFFFF'),
        title: Text(
          'Perfil',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(bottom: 30),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                Container(
                  width: size.width,
                  height: size.height * 0.3,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color.fromRGBO(0, 188, 208, 1),
                        Color.fromRGBO(190, 124, 246, 1)
                      ],
                      begin: FractionalOffset.topCenter,
                      end: FractionalOffset.bottomCenter,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: size.height * 0.14),
                  child: Stack(
                    alignment: Alignment.topCenter,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                              blurRadius: 5.0,
                              offset: Offset(0.0, 1.0),
                            )
                          ],
                        ),
                        child: Container(
                          width: size.width * 0.9,
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          child: Padding(
                            padding: const EdgeInsets.only(top: 85),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  widget.user.data['name'],
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                SizedBox(height: 5.0),
                                Text(
                                  (widget.user.data['profesion_type'] != null)
                                      ? widget.user.data['profesion_type']
                                      : "",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 16,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      FractionalTranslation(
                        translation: Offset(0.0, -0.52),
                        child: Align(
                          child: CircleAvatar(
                            radius: 75,
                            backgroundColor: Colors.transparent,
                            backgroundImage: widget.user.data['image'] != null
                                ? NetworkImage(widget.user.data['image'])
                                : AssetImage('assets/icon/no-usuario.png'),
                          ),
                          alignment: Alignment.center,
                        ),
                      ),
                      Container(
                        width: 100,
                        height: 50,
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        margin: const EdgeInsets.only(top: 45),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromRGBO(0, 188, 208, 1),
                              Color.fromRGBO(190, 124, 246, 1)
                            ],
                            begin: FractionalOffset.bottomRight,
                            end: FractionalOffset.bottomLeft,
                          ),
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            GestureDetector(
                              onTap: () async {
                                var userData = await Firestore.instance
                                    .collection('users')
                                    .document(user.user.uid)
                                    .get();
                                var favorites =
                                    userData.data['favorites'] ?? [];
                                var favRef = Firestore.instance
                                    .collection('users')
                                    .document(widget.user.documentID);
                                if (favorites.indexOf(favRef) == -1) {
                                  Firestore.instance
                                      .collection('users')
                                      .document(user.user.uid)
                                      .updateData({
                                    'favorites': FieldValue.arrayUnion([favRef])
                                  });
                                  setState(() {
                                    _isFavorite = true;
                                  });
                                } else {
                                  Firestore.instance
                                      .collection('users')
                                      .document(user.user.uid)
                                      .updateData({
                                    'favorites':
                                        FieldValue.arrayRemove([favRef])
                                  });
                                  setState(() {
                                    _isFavorite = false;
                                  });
                                }
                                getFavorites();
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: Icon(
                                  _isFavorite ? Icons.star : Icons.star_border,
                                  color: Colors.white,
                                  size: 26,
                                ),
                              ),
                            ),
                            Text(
                              '${NumberFormat.compact().format(_favorites.length)}',
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.white,
                                fontWeight: FontWeight.w800,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  tabButton(true, 'Fotos'),
                  tabButton(false, 'Mis servicios'),
                ],
              ),
            ),
            _showImages ? renderImages() : renderData(),
          ],
        ),
      ),
    );
  }

  Widget renderImages() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            getImageWithGesture(
                (widget.user.data['photo_1'] != null)
                    ? NetworkImage(widget.user.data['photo_1'])
                    : AssetImage('assets/icon/no-usuario.png'),
                "photo_1",
                (widget.user.data['photo_1'] != null)),
            getImageWithGesture(
                (widget.user.data['photo_2'] != null)
                    ? NetworkImage(widget.user.data['photo_2'])
                    : AssetImage('assets/icon/no-usuario.png'),
                "photo_2",
                (widget.user.data['photo_2'] != null))
          ],
        ),
        Row(
          children: <Widget>[
            getImageWithGesture(
                (widget.user.data['photo_3'] != null)
                    ? NetworkImage(widget.user.data['photo_3'])
                    : AssetImage('assets/icon/no-usuario.png'),
                "photo_3",
                (widget.user.data['photo_3'] != null)),
            getImageWithGesture(
                (widget.user.data['photo_4'] != null)
                    ? NetworkImage(widget.user.data['photo_4'])
                    : AssetImage('assets/icon/no-usuario.png'),
                'photo_4',
                (widget.user.data['photo_4'] != null))
          ],
        ),
        Row(
          children: <Widget>[
            getImageWithGesture(
                (widget.user.data['photo_5'] != null)
                    ? NetworkImage(widget.user.data['photo_5'])
                    : AssetImage('assets/icon/no-usuario.png'),
                'photo_5',
                (widget.user.data['photo_5'] != null)),
            getImageWithGesture(
                (widget.user.data['photo_6'] != null)
                    ? NetworkImage(widget.user.data['photo_6'])
                    : AssetImage('assets/icon/no-usuario.png'),
                'photo_6',
                (widget.user.data['photo_6'] != null))
          ],
        )
      ],
    );
  }

  Widget renderData() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Sobre mi',
                style: TextStyle(fontWeight: FontWeight.w800, fontSize: 22),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                (widget.user.data['description'] != null)
                    ? widget.user.data['description']
                    : "NA",
                style: TextStyle(fontSize: 16),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Column(
          children: _services
              .map<Widget>((DocumentSnapshot service) => GestureDetector(
                    onTap: () {
                      setState(() {
                        _serviceSelected = service;
                      });
                    },
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.0, vertical: 7),
                      child: Material(
                        color: Colors.white,
                        elevation: 2.0,
                        borderRadius: BorderRadius.circular(4.0),
                        child: getText(service),
                      ),
                    ),
                  ))
              .toList(),
        ),
      ],
    );
  }

  Widget getImageWithGesture(
      ImageProvider urlImage, String photoId, bool photoExist) {
    return Expanded(
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              if (photoExist) {
                changeScreen(
                    context,
                    ShowImageZoom(
                      image: urlImage,
                    ));
              }
            },
            child: Image(
              image: urlImage,
              fit: BoxFit.fill,
            ),
          ),
          (this.serviceUser != null)
              ? StreamBuilder<QuerySnapshot>(
                  stream: addAndRemoveFavoriteApi.streamPhotoFavorites(
                      this.serviceUser.documentID.toString(), photoId),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Container(
                        padding: EdgeInsets.only(
                          right: 10,
                        ),
                        height: 190,
                        alignment: Alignment.bottomRight,
                        child: AddFvorite(
                          textFav: "0",
                          isFav: false,
                          onAddFav: (isFav) {},
                        ),
                      );
                    }
                    return (photoExist)
                        ? Container(
                            padding: EdgeInsets.all(10),
                            height: 190,
                            alignment: Alignment.bottomRight,
                            child: AddFvorite(
                              textFav: "${snapshot.data.documents.length}",
                              isFav: addAndRemoveFavoriteApi.isExistDocument(
                                  snapshot.data.documents,
                                  this.currentUser.reference),
                              onAddFav: (isFav) {
                                if (isFav) {
                                  addAndRemoveFavoriteApi.removeFavorite(
                                      this.serviceUser.documentID.toString(),
                                      this.currentUser.documentID.toString(),
                                      photoId);
                                } else {
                                  addAndRemoveFavoriteApi.addFavorite(
                                      this.serviceUser.documentID.toString(),
                                      this.currentUser.documentID.toString(),
                                      photoId);
                                }
                              },
                            ),
                          )
                        : Container();
                  })
              : Container()
        ],
      ),
    );
  }

  Widget getText(DocumentSnapshot service) {
    if (service.data['price'] == null) {
      return Container();
    }
    return Container(
      height: 60,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4.0),
        border: Border.all(
          width: 2.0,
          color: Colors.transparent,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              service.data['service_name'],
              style: TextStyle(
                  fontSize: 18.0,
                  color: _serviceSelected.documentID == service.documentID
                      ? HexColor('##00C3D2')
                      : Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              "\$${service.data['price']}/h",
              style: TextStyle(
                  fontSize: 18.0,
                  color: _serviceSelected.documentID == service.documentID
                      ? HexColor('##00C3D2')
                      : Colors.black,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }

  Widget tabButton(bool value, String label) => Container(
        width: 150,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: _showImages == value
              ? LinearGradient(
                  colors: [
                    HexColor('#00C3D2'),
                    HexColor('#BE7CF6'),
                  ],
                  begin: FractionalOffset.bottomLeft,
                  end: FractionalOffset.bottomRight,
                )
              : LinearGradient(
                  colors: [
                    Colors.grey[300],
                    Colors.grey[300],
                  ],
                  begin: FractionalOffset.bottomLeft,
                  end: FractionalOffset.bottomRight,
                ),
          borderRadius: BorderRadius.circular(10),
        ),
        height: 40,
        child: FlatButton(
          onPressed: () {
            setState(() {
              _showImages = value;
            });
          },
          child: Text(
            label,
            style: TextStyle(
              color: _showImages == value ? Colors.white : Colors.black,
              fontSize: 16,
              fontWeight:
                  _showImages == false ? FontWeight.w600 : FontWeight.w200,
            ),
          ),
        ),
      );
}
