import 'dart:async';

import 'package:generic_bloc_provider/generic_bloc_provider.dart';

class UserProfileBloc implements Bloc{
  StreamController<int> streamController = StreamController<int>();

  Stream<int> get selectInsertHourStream => streamController.stream;
  StreamSink<int> get selectInsertHourSink => streamController.sink;


  StreamController<int> selectDayOfWeekStreamController = StreamController<int>();

  Stream<int> get selectDayOfWeekStream => selectDayOfWeekStreamController.stream;
  StreamSink<int> get selectDayOfWeekSink => selectDayOfWeekStreamController.sink;


  StreamController<bool> check1StreamController = StreamController<bool>();

  Stream<bool> get check1Stream => check1StreamController.stream;
  StreamSink<bool> get check1Sink => check1StreamController.sink;


  StreamController<bool> check2StreamController = StreamController<bool>();

  Stream<bool> get check2Stream => check2StreamController.stream;
  StreamSink<bool> get check2Sink => check2StreamController.sink;


  StreamController<bool> check3StreamController = StreamController<bool>();

  Stream<bool> get check3Stream => check3StreamController.stream;
  StreamSink<bool> get check3WeekSink => check3StreamController.sink;

  @override
  void dispose() {
    // TODO: implement dispose
  }

}