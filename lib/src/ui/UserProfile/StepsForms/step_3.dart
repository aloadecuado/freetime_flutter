import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/ui/components/btn_base.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/utils/utils.dart';
import 'package:free_time/src/widgets/add_docuemnt_file.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' as Path;

class Step3 extends StatefulWidget {
  Step3({Key key, this.next, this.user, this.showError}) : super(key: key);

  final Function(DocumentSnapshot) next;
  final Function showError;
  final user;

  @override
  _Step3State createState() => _Step3State();
}

class _Step3State extends State<Step3> {
  /*
   * Text controllers 
   */
  TextEditingController _name = TextEditingController();
  TextEditingController _id = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _phone = TextEditingController();
  TextEditingController _facebook = TextEditingController();
  TextEditingController _instagram = TextEditingController();
  TextEditingController _linkedin = TextEditingController();
  TextEditingController _twitter = TextEditingController();
  TextEditingController _tikTok = TextEditingController();
  TextEditingController _profesionType = TextEditingController();

  String hv_name_file = "";
  String cer_name_file = "";
  /* 
   * State helpers
   */
  String _idType = 'CC';
  String _idTypeRecibed = "";

  int _genere = 0;

  /* 
   * Focus nodes
   */
  final focusId = FocusNode();
  final focusEmail = FocusNode();
  final focusPhone = FocusNode();

  String _fileName;
  String _path;
  String _extension;
  bool _hasValidMime = false;
  FileType _pickingType;

  File _file;
  String _cv, _cert;

  AddDocumentFile addDocumentFile1;
  AddDocumentFile addDocumentFile2;

  bool _primeravez = false;

  bool _button_loading = false;

  TextGlobal _textGlobal = TextGlobal();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firestore.instance
          .collection('users')
          .document(widget?.user?.user?.uid)
          .get(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return CircularProgressIndicator(
            strokeWidth: 2.0,
          );
        }

        if (!_primeravez) {
          _name.text = snapshot.data['full_name'];
          if (snapshot.data["id_type"] != null) {
            _idTypeRecibed = snapshot.data["id_type"];
          }
          _id.text = snapshot.data['identification'];
          _email.text = snapshot.data['email'];
          _phone.text = snapshot.data['phone'];
          if (snapshot.data["genere"] != null) {
            switch (snapshot.data["genere"]) {
              case "Masculino":
                {
                  _genere = 1;
                }
                break;
              case "Femenino":
                {
                  _genere = 2;
                }
                break;
              case "Otro":
                {
                  _genere = 3;
                }
                break;
            }
          }
          _facebook.text = snapshot.data['facebook_url'];
          _instagram.text = snapshot.data['instagram_url'];
          _linkedin.text = snapshot.data['likedin_url'];
          _twitter.text = snapshot.data['twitter_url'];
          _tikTok.text = snapshot.data['tiktok_url'];
          _profesionType.text = snapshot.data['profesion_type'];

          if (snapshot.data['files'] != null) {
            if (snapshot.data['files']["cv"] != null) {
              if (snapshot.data['files']["cv"]["name"] != null) {
                hv_name_file = snapshot.data['files']["cv"]["name"];
              }
            }

            if (snapshot.data['files']["cert"] != null) {
              if (snapshot.data['files']["cert"]["name"] != null) {
                cer_name_file = snapshot.data['files']["cert"]["name"];
              }
            }
          }

          _primeravez = true;
        }

        addDocumentFile1 =
            AddDocumentFile('cv', 'Sube tu hoja de vida', hv_name_file);
        addDocumentFile2 = AddDocumentFile(
            'cert', 'Sube tu certificado de estudio', cer_name_file);
        return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Información Personal.',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              child: TextFormField(
                controller: _name,
                onEditingComplete: () {
                  print(_name.text);

                  FocusScope.of(context).requestFocus(focusId);
                },
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                decoration: new InputDecoration.collapsed(
                  hintText: 'NOMBRE COMPLETO*',
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color(0xfff7f7f7)),
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            ),
            SizedBox(height: 10.0),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color(0xfff7f7f7),
                  border: Border.all(color: HexColor('#e2e2e2'), width: 1.0)),
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                    flex: 2,
                    child: DropdownButton<String>(
                      underline: Text(''),
                      hint: Text((_idTypeRecibed.isEmpty)
                          ? _idType = 'CC'
                          : _idTypeRecibed),
                      items: <String>['CC', 'CE'].map((String value) {
                        return new DropdownMenuItem<String>(
                          value: value,
                          child: new Text(
                            value,
                            textAlign: TextAlign.center,
                          ),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          _idType = value;
                        });
                        print(_idType);
                      },
                    ),
                  ),
                  Expanded(
                    flex: 8,
                    child: Container(
                      child: TextFormField(
                        controller: _id,
                        onEditingComplete: () {
                          print(_id.text);

                          FocusScope.of(context).requestFocus(focusEmail);
                        },
                        focusNode: focusId,
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                        decoration: new InputDecoration.collapsed(
                          hintText: 'Identificación',
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Color(0xfff7f7f7)),
                      padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 10.0),
            Container(
              child: TextFormField(
                onEditingComplete: () {
                  print(_email.text);

                  FocusScope.of(context).requestFocus(focusPhone);
                },
                focusNode: focusEmail,
                controller: _email,
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                decoration: new InputDecoration.collapsed(
                  hintText: 'Correo electrónico',
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color(0xfff7f7f7)),
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              child: TextFormField(
                onEditingComplete: () {
                  print(_phone.text);
                },
                focusNode: focusPhone,
                controller: _phone,
                keyboardType: TextInputType.phone,
                textInputAction: TextInputAction.next,
                decoration: new InputDecoration.collapsed(
                  hintText: 'Teléfono',
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color(0xfff7f7f7)),
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            ),
            SizedBox(
              height: 20.0,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    child: Center(
                      child: Text('SEXO'),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        border: Border.all(
                          color: HexColor('#979797'),
                        )),
                    padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                  ),
                ),
                SizedBox(width: 10.0),
                Expanded(
                  flex: 2,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        _genere = 1;
                      });
                    },
                    child: Container(
                      child: Text(
                        'M',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: (_genere == 1)
                              ? Colors.white
                              : HexColor('#363636'),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: (_genere == 1)
                              ? HexColor('#0CBFD5')
                              : Color(0xfff7f7f7)),
                      padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                    ),
                  ),
                ),
                SizedBox(width: 10.0),
                Expanded(
                  flex: 2,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        _genere = 2;
                      });
                    },
                    child: Container(
                      child: Text(
                        'F',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: (_genere == 2)
                              ? Colors.white
                              : HexColor('#363636'),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: (_genere == 2)
                              ? HexColor('#0CBFD5')
                              : Color(0xfff7f7f7)),
                      padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                    ),
                  ),
                ),
                SizedBox(width: 10.0),
                Expanded(
                  flex: 2,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        _genere = 3;
                      });
                    },
                    child: Container(
                      child: Text(
                        'OTRO',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: (_genere == 3)
                              ? Colors.white
                              : HexColor('#363636'),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: (_genere == 3)
                              ? HexColor('#0CBFD5')
                              : Color(0xfff7f7f7)),
                      padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 20.0),
            Container(
              child: Center(
                child: Text('ELIGE TUS REDES SOCIALES'),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(
                    color: HexColor('#979797'),
                  )),
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            ),
            SizedBox(height: 10.0),
            Container(
              child: TextFormField(
                onChanged: (value) {},
                controller: _facebook,
                keyboardType: TextInputType.url,
                textInputAction: TextInputAction.next,
                decoration: new InputDecoration.collapsed(
                  hintText: 'Facebook',
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color(0xfff7f7f7)),
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            ),
            SizedBox(height: 10.0),
            Container(
              child: TextFormField(
                onChanged: (value) {},
                controller: _instagram,
                keyboardType: TextInputType.url,
                textInputAction: TextInputAction.next,
                decoration: new InputDecoration.collapsed(
                  hintText: 'Instagram',
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color(0xfff7f7f7)),
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            ),
            SizedBox(height: 10.0),
            Container(
              child: TextFormField(
                onChanged: (value) {},
                controller: _linkedin,
                keyboardType: TextInputType.url,
                textInputAction: TextInputAction.next,
                decoration: new InputDecoration.collapsed(
                  hintText: 'Linkedin',
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color(0xfff7f7f7)),
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            ),
            SizedBox(height: 10.0),
            Container(
              child: TextFormField(
                onEditingComplete: () {
                  print(_twitter.text);
                },
                controller: _twitter,
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                decoration: new InputDecoration.collapsed(
                  hintText: 'Twitter',
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color(0xfff7f7f7)),
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            ),
            SizedBox(height: 10.0),
            Container(
              child: TextFormField(
                onEditingComplete: () {
                  print(_tikTok.text);
                },
                controller: _tikTok,
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                decoration: new InputDecoration.collapsed(
                  hintText: _textGlobal.tikTok,
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color(0xfff7f7f7)),
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            ),
            SizedBox(height: 20.0),
            Text(
              'Información Profesional.',
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),
            ),
            SizedBox(height: 20.0),
            Container(
              child: TextFormField(
                onChanged: (value) {},
                controller: _profesionType,
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                decoration: new InputDecoration.collapsed(
                  hintText: 'TIPO DE PROFESIÓN',
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Color(0xfff7f7f7),
              ),
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            ),
            SizedBox(height: 10.0),
            addDocumentFile1
            /*GestureDetector(
                onTap: () {
                  _openFileExplorer('cv');
                },
                child: Container(
                  child: Center(child: Text(_cv ?? 'Sube tu hoja de vida'),),
                  decoration: BoxDecoration (
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(
                      color: HexColor('#979797'),
                    )
                  ),
                  padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                ),
              )*/
            ,
            SizedBox(height: 10.0),
            addDocumentFile2,
            SizedBox(height: 10.0),
            Text(
              _textGlobal.diplomas,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w200,
              ),
            ),
            /*GestureDetector(
                onTap: () {
                  _openFileExplorer('cert');
                },
                child: Container(
                  child: Center(child: Text(_cert ?? 'Sube tu certificado de estudio'),),
                  decoration: BoxDecoration (
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(
                      color: HexColor('#979797'),
                    )
                  ),
                  padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                ),
              )*/
            Container(
              margin: const EdgeInsets.fromLTRB(0, 50, 0, 10),
              width: MediaQuery.of(context).size.width * 0.85,
              height: 50,
              child: (!_button_loading)
                  ? BtnBase(
                      color: HexColor('#0CBFD5'),
                      texto: 'Continuar',
                      ontap: () {
                        if (_name.text.isEmpty ||
                            _id.text.isEmpty ||
                            _email.text.isEmpty ||
                            _phone.text.isEmpty ||
                            _profesionType.text.isEmpty) {
                          widget.showError(
                              "Debe llenar todos los campos requeridos");
                          return;
                        }

                        int countSocilaWeb = 0;

                        if (!_facebook.text.isEmpty) {
                          countSocilaWeb++;
                        }
                        if (!_instagram.text.isEmpty) {
                          countSocilaWeb++;
                        }
                        if (!_linkedin.text.isEmpty) {
                          countSocilaWeb++;
                        }
                        if (!_twitter.text.isEmpty) {
                          countSocilaWeb++;
                        }
                        if (!_tikTok.text.isEmpty) {
                          countSocilaWeb++;
                        }

                        if (countSocilaWeb < 2) {
                          widget.showError(
                              "Debe agregar aunque sea 2 redes sociales");
                          return;
                        }

                        print(addDocumentFile1.isValidate);
                        if (addDocumentFile1.file_name.isEmpty ||
                            addDocumentFile2.file_name.isEmpty) {
                          widget.showError(
                              "Debe agregar la hoja de vida(curriculum) y su certificado de estudio");
                          return;
                        }

                        addBasicInfo();

                        setState(() {
                          _button_loading = true;
                        });
                      })
                  : CircularProgressIndicator(
                      strokeWidth: 2.0,
                    ),
            ),
          ],
        );
      },
    );
  }

  Future addBasicInfo() async {
    /*_name.text = snapshot.data['full_name'];
    _id.text = snapshot.data['identification'];
    _email.text = snapshot.data['email'];
    _phone.text = snapshot.data['phone'];
    _facebook.text = snapshot.data['facebook_url'];
    _instagram.text = snapshot.data['instagram_url'];
    _linkedin.text = snapshot.data['likedin_url'];
    _twitter.text = snapshot.data['twitter_url'];
    _profesionType.text = snapshot.data['profesion_type'];*/

    String genero = "";
    switch (_genere) {
      case 1:
        {
          genero = "Masculino";
        }
        break;
      case 2:
        {
          genero = "Femenino";
        }
        break;
      case 3:
        {
          genero = "Otro";
        }
        break;
    }

    Firestore.instance
        .collection('users')
        .document(widget?.user?.user?.uid)
        .updateData({
      'full_name': _name.text,
      "id_type": _idType,
      "identification": _id.text,
      "email": _email.text,
      "phone": _phone.text,
      'genere': genero,
      "facebook_url": _facebook.text,
      "instagram_url": _instagram.text,
      "likedin_url": _linkedin.text,
      "twitter_url": _twitter.text,
      "tiktok_url": _tikTok.text,
      "profesion_type": _profesionType.text,
    }).then((dynamic) {
      setState(() {
        _button_loading = true;
      });
      Firestore.instance
          .collection("users")
          .document(widget?.user?.user?.uid)
          .get()
          .then((dataSnapShot) {
        widget.next(dataSnapShot);
      });
    });
  }

  /*void _openFileExplorer(fileField) async {
    if (_pickingType != FileType.CUSTOM || _hasValidMime) {
      try {
        _path = null;        
        _file = await FilePicker.getFile(
          type: _pickingType, fileExtension: _extension
        );
        if (_file != null) {
          _path = _file.path;
          setState(() {
            _fileName = _path.split('/').last ?? _path.split('/').last;
          });
          uploadFile(fileField);
        }
      } catch (e) {
        print("Unsupported operation" + e.toString());
      }     
    }
  }


  Future uploadFile(fileField) async { 
    setState(() {
    }); 
    final user = await FirebaseAuth.instance.currentUser();
    final name = getUID(8);
    print('Uploading file for user ${user.uid}');
    StorageReference storageReference = FirebaseStorage.instance
      .ref()
      .child('users/${user.uid}/files/${name}_${Path.basename(_path)}');
    StorageUploadTask uploadTask = storageReference.putFile(_file);
    await uploadTask.onComplete;
    print('File Uploaded');
    storageReference.getDownloadURL().then((fileURL) {
      Firestore.instance.collection('users').document(user.uid).updateData({
        'files.${fileField}.url': fileURL,
        'files.${fileField}.name': _fileName,
      });


      if (fileField == 'cv'){
        setState(() {
          _cv = _fileName;
        });
      } else {
        setState(() {
          _cert = _fileName;
        });
      }
    });
  } */
}
