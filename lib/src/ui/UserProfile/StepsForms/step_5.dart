import 'package:flutter/material.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/ui/components/btn_base.dart';
import 'package:free_time/src/widgets/pop_overs_alert.dart';

class Step5 extends StatefulWidget {
  Step5({Key key, this.next, this.user, this.finish}) : super(key: key);

  final user;
  final Function next;
  final Function finish;

  @override
  _Step5State createState() => _Step5State();
}

class _Step5State extends State<Step5> {
  TextEditingController _description = TextEditingController();

  String _activeDay = 'Lun';

  TextGlobal _textGlobal = TextGlobal();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.85,
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Container(
            height: 50,
            width: MediaQuery.of(context).size.width * 0.85,
            child: BtnBase(
              color: HexColor('#0CBFD5'),
              texto: 'Indica tu tiempo libre',
              textColor: Colors.white,
              ontap: () {
                widget.next();
              },
            ),
          ),
          SizedBox(
            height: 70,
          ),
          Container(
            width: 200,
            child: Text(
              'Añade una descripción corta acerca de ti',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 17,
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            height: 150,
            child: TextField(
              controller: _description,
              onChanged: (value) {
                Firestore.instance
                    .collection('users')
                    .document(widget.user.user.uid)
                    .updateData({'description': _description.text});
              },
              keyboardType: TextInputType.multiline,
              maxLines: 3,
              textInputAction: TextInputAction.done,
              decoration: new InputDecoration.collapsed(
                hintText:
                    'Mi nombre es Mary Burgess, estudié publicidad en al universidad de los Andes, trabajé en agencias reconocidas en Colombia como: Juan Valdes, Revista Semana ',
              ),
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                color: Color(0xfff7f7f7)),
            padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
          ),
          Container(
            margin: const EdgeInsets.only(top: 40, bottom: 20),
            height: 50,
            width: MediaQuery.of(context).size.width * 0.85,
            child: BtnBase(
              color: HexColor('#0CBFD5'),
              texto: 'Previsualizar mi perfil',
              textColor: Colors.white,
              ontap: () {
                // widget.next();
              },
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20, bottom: 20),
            height: 50,
            width: MediaQuery.of(context).size.width * 0.85,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color.fromRGBO(0, 188, 208, 1),
                  Color.fromRGBO(190, 124, 246, 1)
                ],
                begin: FractionalOffset.bottomLeft,
                end: FractionalOffset.bottomRight,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            child: BtnBase(
              texto: 'Enviar solicitud',
              textColor: Colors.white,
              ontap: () {
                PopOversAlert().showAlertTitleSubTitleYes(
                    context,
                    _textGlobal.felicitaciones,
                    _textGlobal.estaremosEvaluando,
                    _textGlobal.aceptar, () {
                  Navigator.of(context).pop();
                  widget.finish();
                });

                Firestore.instance
                    .collection('users')
                    .document(widget.user.user.uid)
                    .updateData({'register_complete': true});
              },
            ),
          ),
        ],
      ),
    );
  }
}
