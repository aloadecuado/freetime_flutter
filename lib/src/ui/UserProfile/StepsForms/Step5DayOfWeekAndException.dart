import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/models/DateExceptionModel.dart';
import 'package:free_time/src/models/EnabledHours.dart';
import 'package:free_time/src/ui/UserProfile/Bloc/UserProfileBloc.dart';
import 'package:free_time/src/ui/UserProfile/Widget/AddDatesException.dart';
import 'package:free_time/src/ui/UserProfile/Widget/BoxSelectDaysOfWeek.dart';
import 'package:free_time/src/ui/UserProfile/Widget/DaySelectForWeek.dart';
import 'package:free_time/src/ui/components/btn_base.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/widgets/CustomDecorationBox.dart';
import 'package:free_time/src/widgets/ExclusiveSelectorButton.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';

class Step5DayOfWeekAndException extends StatefulWidget {
  final Function next;
  final Function showError;
  final user;
  final DocumentSnapshot userDocument;

  Step5DayOfWeekAndException(
      {Key key, this.next, this.showError, this.user, this.userDocument});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Step5DayOfWeekAndException();
  }
}

class _Step5DayOfWeekAndException extends State<Step5DayOfWeekAndException> {
  TextGlobal _textGlobal = TextGlobal();
  List<String> textButtons = ["Semana", "Excepciones"];

  UserProfileBloc userProfileBloc;

  List<EnabledHours> listEnabledHours;

  BoxSelectDaysOfWeek boxSelectDaysOfWeek;

  bool isEnabled = false;

  List<DateExceptionModel> ddatesException;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    userProfileBloc = UserProfileBloc();
    userProfileBloc.selectInsertHourSink.add(0);

    boxSelectDaysOfWeek = BoxSelectDaysOfWeek(
      isEnabled: isEnabled,
      listEnabledHours: listEnabledHours,
      onSelectEnabledHours: (listEnabled) {
        listEnabledHours = listEnabled;
      },
    );

    ddatesException = List<DateExceptionModel>();

    return Container(
      width: MediaQuery.of(context).size.width * 0.85,
      color: Colors.white,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 10.0,
          ),
          //selectExperience(),
          //streamSelectDayOfWeek(true)
          StreamBuilder(
              stream: userProfileBloc.selectInsertHourStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data == 0) {
                    return boxSelectDaysOfWeek;
                  } else {
                    return AddDatesException(
                      datesException: this.ddatesException,
                      onDatesException: (datesException) {
                        this.ddatesException = datesException;
                      },
                    );
                  }
                }
                return Container();
              }),
          SizedBox(
            height: 10.0,
          ),
          BtnBase(
              color: HexColor('#0CBFD5'),
              texto: 'OK',
              ontap: () {
                widget.next();
              }),
          SizedBox(
            height: 10.0,
          ),
        ],
      ),
    );
  }

  List<Widget> getList() {
    List<Widget> widgets = List<Widget>();

    _textGlobal.dayOfWeek.forEach((day) {
      widgets.add(getTitle(day));
    });
    /*for (var i = 0; i < 10; i++) {
      children.add(new ListTile());
    }*/

    return widgets;
  }

  Widget getTitle(String title) {
    return CustomDecorationBox(
      alignment: Alignment.center,
      width: 80,
      height: 80,
      boxDecoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [HexColor('#FFFFFF')]),),
      child: Text(
        title,
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
          color: Colors.white
        ),
      ),
    );
  }
}
