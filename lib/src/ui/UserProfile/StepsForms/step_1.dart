import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/components/btn_base.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/utils/utils.dart';
import 'package:free_time/src/widgets/card_add_image.dart';
import 'package:free_time/src/widgets/pop_overs_alert.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;
import 'package:provider/provider.dart';

class Step1 extends StatefulWidget {
  Step1({Key key, this.next, this.showError, this.close}) : super(key: key);

  final Function next;
  final Function close;
  final Function showError;
  @override
  Step1State createState() => Step1State();
}

class Step1State extends State<Step1> {
  File _image;
  File _imageThum;
  String _loading = '';
  int percentaje = 40;

  CardAddImage cardAddImage1;
  CardAddImage cardAddImage2;
  CardAddImage cardAddImage3;
  CardAddImage cardAddImage4;
  CardAddImage cardAddImage5;
  CardAddImage cardAddImage6;

  TextGlobal _textGlobal = TextGlobal();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<UserProvider>(context);
    print(user.user.uid);

    return StreamBuilder(
        stream: Firestore.instance
            .collection('users')
            .document(user.user.uid)
            .snapshots(),
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            var userDocument = snapshot.data;

            /*if(userDocument["register_complete"] != null){

          return Container(
            alignment: Alignment.center,
            child: GestureDetector(
              child: Text(_textGlobal.suEstado, style: TextStyle(
                fontSize: 20,
                color: Colors.black
              ),
              ),
              onTap: (){

                widget.close();
              },
            )

          );

        }*/
            cardAddImage1 = CardAddImage(
              userDocument: userDocument,
              pathDefauldImage:
                  'https://via.placeholder.com/150/C1C0C9/C1C0C9?text=j',
              idImage: 'photo_1',
              height: 200,
            );
            cardAddImage2 = CardAddImage(
              userDocument: userDocument,
              pathDefauldImage:
                  'https://via.placeholder.com/150/C1C0C9/C1C0C9?text=j',
              idImage: 'photo_2',
              height: 95,
            );
            cardAddImage3 = CardAddImage(
              userDocument: userDocument,
              pathDefauldImage:
                  'https://via.placeholder.com/150/C1C0C9/C1C0C9?text=j',
              idImage: 'photo_3',
              height: 95,
            );
            cardAddImage4 = CardAddImage(
              userDocument: userDocument,
              pathDefauldImage:
                  'https://via.placeholder.com/150/C1C0C9/C1C0C9?text=j',
              idImage: 'photo_4',
              height: 95,
            );
            cardAddImage5 = CardAddImage(
              userDocument: userDocument,
              pathDefauldImage:
                  'https://via.placeholder.com/150/C1C0C9/C1C0C9?text=j',
              idImage: 'photo_5',
              height: 95,
            );
            cardAddImage6 = CardAddImage(
              userDocument: userDocument,
              pathDefauldImage:
                  'https://via.placeholder.com/150/C1C0C9/C1C0C9?text=j',
              idImage: 'photo_6',
              height: 95,
            );

            return Container(
              child: Column(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text(
                        'Sube la foto que mejor te identifique! Puedes subir 6 fotos, te sugerimos subir alguna de tu trabajo para que los demas lo puedan conocer!!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 15.0,
                        ),
                      ),
                      SizedBox(
                        height: 40.0,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 6,
                            child: cardAddImage1,
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Expanded(
                              flex: 3,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  cardAddImage2,
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  cardAddImage3,
                                ],
                              ))
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(flex: 3, child: cardAddImage4),
                          SizedBox(
                            width: 5.0,
                          ),
                          Expanded(
                            flex: 3,
                            child: cardAddImage5,
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Expanded(
                            flex: 3,
                            child: cardAddImage6,
                          ),
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(0, 50, 0, 10),
                        width: MediaQuery.of(context).size.width * 0.85,
                        height: 50,
                        child: BtnBase(
                          color: HexColor('#05C2D3'),
                          texto: 'Continuar',
                          ontap: () {
                            if (!cardAddImage1.isValidate &&
                                !cardAddImage2.isValidate &&
                                !cardAddImage3.isValidate &&
                                !cardAddImage4.isValidate &&
                                !cardAddImage5.isValidate &&
                                !cardAddImage6.isValidate) {
                              widget.showError(
                                  "debe agregar aunque sea una imagen");
                            } else {
                              widget.next();
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          } else {
            return Container();
          }
        });
  }
}
