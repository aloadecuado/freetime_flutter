import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/ui/components/btn_base.dart';
import 'package:free_time/src/ui/components/custom_dialog.dart';
import 'package:free_time/src/widgets/card_add_image.dart';
import 'package:free_time/src/widgets/pop_overs_alert.dart';
import 'package:path/path.dart' as Path;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/utils/utils.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class Step2 extends StatefulWidget {
  Step2({Key key, this.next, this.showError}) : super(key: key);

  final Function next;
  final Function showError;
  @override
  _Step2State createState() => _Step2State();
}

class _Step2State extends State<Step2> {
  File _image;
  File _imageThum;
  int percentaje = 40;

  String _bird_day = "";
  String _bird_day_public = "";

  DateTime _bird_day_date;
  DateTime _bird_day_public_date;

  String _selectedDate;
  String _selectedDatePublic;

  var _loading;
  bool _button_loading = false;

  CardAddImage cardAddImage1;
  CardAddImage cardAddImage2;
  CardAddImage cardAddImage3;

  TextGlobal _textGlobal = TextGlobal();
  @override
  Widget build(BuildContext context) {
    var user = Provider.of<UserProvider>(context);
    print(user.user.uid);
    return StreamBuilder(
      stream: Firestore.instance
          .collection('users')
          .document(user.user.uid)
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return CircularProgressIndicator(
            strokeWidth: 2.0,
          );
        }
        if (snapshot.hasData && snapshot.data["birthday"] != null) {
          Timestamp timestamp = snapshot.data['birthday'];
          //_bird_day = snapshot.data['birthday'].toString();
          _bird_day_date = DateTime.fromMicrosecondsSinceEpoch(
              timestamp.microsecondsSinceEpoch); //snapshot.data['birthday'];
          _bird_day = DateFormat("dd-MM-yyyy").format(_bird_day_date);
        }

        if (snapshot.hasData && snapshot.data["bird_day_public"] != null) {
          Timestamp timestamp = snapshot.data['bird_day_public'];
          //_bird_day_public = snapshot.data["bird_day_public"].toString();
          _bird_day_public_date = DateTime.fromMicrosecondsSinceEpoch(
              timestamp.microsecondsSinceEpoch);
          _bird_day_public =
              DateFormat("dd-MM-yyyy").format(_bird_day_public_date);
          //snapshot.data["bird_day_public"];
        }
        if (!snapshot.hasData) {
          return Container();
        }
        var userDocument = snapshot.hasData ? snapshot.data : {};

        cardAddImage1 = CardAddImage(
          userDocument: userDocument,
          pathDefauldImage:
              'https://via.placeholder.com/150/C1C0C9/C1C0C9?text=j',
          idImage: 'photo_id_1',
          height: 125,
        );
        cardAddImage2 = CardAddImage(
          userDocument: userDocument,
          pathDefauldImage:
              'https://via.placeholder.com/150/C1C0C9/C1C0C9?text=j',
          idImage: 'photo_id_2',
          height: 125,
        );
        cardAddImage3 = CardAddImage(
          userDocument: userDocument,
          pathDefauldImage:
              'https://firebasestorage.googleapis.com/v0/b/free-time-98d04.appspot.com/o/dni.png?alt=media&token=7a040d8a-cf2f-4fb4-8dad-36b4c3642cc0',
          idImage: 'photo_id',
          height: 200,
        );
        return Container(
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text(
                      'Fotografía del documento de identidad.',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 17),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 5,
                          child: cardAddImage1,
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        Expanded(
                          flex: 5,
                          child: cardAddImage2,
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: 37,
                        right: 42,
                        top: 10,
                        bottom: 10,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Delantera',
                            style: TextStyle(fontSize: 16),
                          ),
                          Text(
                            'Posterior',
                            style: TextStyle(fontSize: 16),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                      'Queremos verificar que eres tu.',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    cardAddImage3,
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                        'Sube una foto con el docuento en tu mano como esta referenciado en la foto de ejemplo',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 16,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(height: 20.0),
                    Text(
                      'FECHA DE NACIMIENTO REAL',
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        _selectDate(context);
                      },
                      child: Container(
                        child: Text(
                          (_bird_day.isNotEmpty) ? _bird_day : 'DD/MM/AAA',
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            color: Color(0xfff7f7f7)),
                        padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'FECHA DE NACIMIENTO QUE QUIERES MOSTRAR EN LA APLICACIÓN',
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        _selectDatePublic(context);
                      },
                      child: Container(
                        child: Text((_bird_day_public.isNotEmpty)
                            ? _bird_day_public
                            : 'DD/MM/AAA'),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            color: Color(0xfff7f7f7)),
                        padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(0, 50, 0, 10),
                      width: MediaQuery.of(context).size.width * 0.85,
                      height: 50,
                      child: BtnBase(
                        color: HexColor('#05C2D3'),
                        texto: 'Continuar',
                        ontap: () {
                          if (!cardAddImage1.isValidate ||
                              !cardAddImage2.isValidate ||
                              !cardAddImage3.isValidate) {
                            widget.showError(
                                "debe agregar aunque sea una imagen");
                            return;
                          }
                          if (_bird_day.isEmpty || _bird_day_public.isEmpty) {
                            widget.showError(_textGlobal.agregueFecha);
                            return;
                          }

                          addBasicInfo();

                          widget.next();
                        },
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      firstDate: DateTime(1980, 1),
      lastDate: DateTime(2101),
      initialDate: DateTime(2019, 1),
    );
    if (picked != null && picked.toString() != _bird_day)
      setState(() {
        _bird_day = DateFormat("dd-MM-yyyy").format(picked);
        _bird_day_date = picked;
      });
  }

  Future<Null> _selectDatePublic(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      firstDate: DateTime(1980, 1),
      lastDate: DateTime(2101),
      initialDate: DateTime(2019, 1),
    );
    if (picked != null && picked.toString() != _bird_day_public)
      setState(() {
        _bird_day_public = DateFormat("dd-MM-yyyy").format(picked);
        _bird_day_public_date = picked;
      });
  }

  Future addBasicInfo() async {
    final user = Provider.of<UserProvider>(context, listen: false);
    Firestore.instance.collection('users').document(user.user.uid).updateData({
      "birthday": _bird_day_date,
      "bird_day_public": _bird_day_public_date,
    }).then((dynamic) {
      setState(() {
        _button_loading = true;
      });
      Firestore.instance
          .collection("users")
          .document(user.user.uid)
          .get()
          .then((dataSnapShot) {
        widget.next(dataSnapShot);
      });
    });
  }
}
