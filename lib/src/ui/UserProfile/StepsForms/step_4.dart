import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/models/category.dart';
import 'package:free_time/src/models/establishment.dart';
import 'package:free_time/src/ui/components/btn_base.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:free_time/src/widgets/pop_overs_alert.dart';
import 'package:free_time/src/Values/texts_globals.dart';

class Step4 extends StatefulWidget {
  Step4({Key key, this.next, this.user, this.showError, this.userDocument})
      : super(key: key);

  final Function next;
  final Function showError;
  final user;
  final DocumentSnapshot userDocument;

  @override
  _Step4State createState() => _Step4State();
}

class _Step4State extends State<Step4> {
  AutoCompleteTextField searchTextField;

  GlobalKey<AutoCompleteTextFieldState<Category>> key = new GlobalKey();
  List<Category> _categories = [];
  List<Establishment> _secureEstablishments = [];
  List<Establishment> _secureEstablishmentsAded = [];
  List<String> _customEstablishments = [];
  List<Category> _categoriesAdded = [];

  CollectionReference usersReference =
      Firestore.instance.collection('users').reference();

  TextEditingController _newSite = new TextEditingController();

  bool isShowServices = true;
  bool virtualFreeTime = false;
  bool virtualFreeJob = false;
  bool domiciliary = false;

  void _changeVirtualfreeTime(bool newValue) => setState(() {
        virtualFreeTime = newValue;

        if (virtualFreeTime) {
          // TODO: Here goes your functionality that remembers the user.
        } else {
          // TODO: Forget the user
        }
      });

  void _changeVirtualfreeJob(bool newValue) => setState(() {
        virtualFreeJob = newValue;

        if (virtualFreeJob) {
          // TODO: Here goes your functionality that remembers the user.
        } else {
          // TODO: Forget the user
        }
      });

  void _changeDomiciliary(bool newValue) => setState(() {
        domiciliary = newValue;

        if (domiciliary) {
          // TODO: Here goes your functionality that remembers the user.
        } else {
          // TODO: Forget the user
        }
      });

  TextGlobal _textGlobal = TextGlobal();

  @override
  void initState() {
    Firestore.instance
        .collection('categories')
        .where('active', isEqualTo: true)
        .getDocuments()
        .then((categories) {
      categories.documents.forEach((category) {
        setState(() {
          _categories.add(new Category.fromDocumentSnapShot(category));
        });
      });
    });

    Firestore.instance
        .collection('establishments')
        .where('active', isEqualTo: true)
        .getDocuments()
        .then((establishments) {
      establishments.documents.forEach((establishment) {
        setState(() {
          _secureEstablishments
              .add(new Establishment.fromDocumentSnapShot(establishment));
        });
      });
    });

    Firestore.instance
        .collection('users')
        .document(widget.user.user.uid)
        .get()
        .then((user) {
      user.data['services_sites'].forEach((site) {
        setState(() {
          _customEstablishments.add(site);
        });
      });
      for (var i = 0; i < user.data['categories'].length; i++) {
        DocumentReference ref = user.data['categories'][i];
        ref.get().then((category) {
          setState(() {
            _categoriesAdded.add(Category.fromDocumentSnapShot(category));
          });
        });
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    searchTextField = AutoCompleteTextField<Category>(
      key: key,
      style: TextStyle(color: Color(0xff5A5A5A), fontSize: 16.0),
      decoration: InputDecoration(
        border: InputBorder.none,
        contentPadding: EdgeInsets.all(0.0),
        hintText: 'Busca una categoría',
        hintStyle: TextStyle(color: Color(0xff5A5A5A)),
      ),
      itemFilter: (item, query) {
        return item.name.toLowerCase().startsWith(query.toLowerCase());
      },
      itemSorter: (a, b) {
        return a.name.compareTo(b.name);
      },
      itemSubmitted: (category) async {
        if (_categoriesAdded
                .map((cat) => cat.name)
                .toList()
                .indexOf(category.name) ==
            -1) {
          DocumentReference categoryRef =
              Firestore.instance.collection('categories').document(category.id);
          await Firestore.instance
              .collection('users')
              .document(widget.user.user.uid)
              .updateData({
            'categories': FieldValue.arrayUnion([categoryRef])
          });
          setState(() {
            _categoriesAdded.add(category);
          });
        }
      },
      suggestions: _categories,
      itemBuilder: (context, item) {
        return Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            item.name,
            style: TextStyle(fontSize: 16.0),
          ),
        );
      },
    );
    print(isShowServices);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          'Añade las categorías afines a tus servicios.',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.search, color: HexColor('#5A5A5A')),
              SizedBox(
                width: 10.0,
              ),
              Flexible(
                child: searchTextField,
              )
            ],
          ),
          height: 50,
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Color(0xfff7f7f7),
          ),
          padding: EdgeInsets.symmetric(horizontal: 10),
        ),
        SizedBox(height: 15.0),
        Wrap(children: renderCategories(_categoriesAdded)),
        SizedBox(
          height: 20.0,
        ),
        Text(
          'Lista los servicios que prestarás en la plataforma.',
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        SizedBox(
          height: 20.0,
        ),
        (isShowServices) ? getServices() : getButtonShowServices(),
        SizedBox(
          height: 10.0,
        ),
        renderServicesList(),
        SizedBox(
          height: 25,
        ),
        Container(
          height: 50,
          width: MediaQuery.of(context).size.width * 0.85,
          child: BtnBase(
            color: HexColor('#0CBFD5'),
            texto: 'Añadir servicio',
            textColor: HexColor('#FFFFFF'),
            ontap: () {
              createUserService();
            },
          ),
        ),
        SizedBox(height: 20.0),
        Container(
          height: 50,
          width: MediaQuery.of(context).size.width * 0.85,
          child: BtnBase(
            color: HexColor('#0CBFD5'),
            texto: 'Continuar',
            ontap: () {
              if (_categoriesAdded.length < 1) {
                widget.showError("debes registrar aunque sea un categoria");
                return;
              }

              if (_secureEstablishmentsAded.length < 1) {
                widget.showError("debes registrar aunque sea un sitio");
                return;
              }

              widget.next();
            },
          ),
        ),
        SizedBox(height: 20.0),
      ],
    );
  }

  Widget getServices() {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Color.fromRGBO(12, 191, 213, 0.2),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(5),
                    width: 90,
                    decoration: BoxDecoration(
                        color: HexColor('#0CBFD5'),
                        borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(
                          Icons.alarm_on,
                          color: HexColor(
                            '#FFFFFF',
                          ),
                        ),
                        Text(
                          'Timer',
                          style: TextStyle(
                              fontSize: 18,
                              color: HexColor(
                                '#FFFFFF',
                              )),
                        ),
                      ],
                    ),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.info_outline,
                      color: HexColor('#0CD52C'),
                    ),
                    onPressed: () {
                      PopOversAlert().showAlertTitleSubTitleYes(
                          context,
                          _textGlobal.timer,
                          _textGlobal.descriptionTimer,
                          _textGlobal.aceptar, () {
                        Navigator.of(context).pop();
                      });
                    },
                  ),
                ],
              ),
              InkWell(
                child: Icon(
                  Icons.close,
                  size: 24,
                  color: Colors.white,
                ),
                onTap: () {
                  setState(() {
                    isShowServices = false;
                  });
                },
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Stack(
            children: <Widget>[
              Container(
                height: 40,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'CONOCER A:',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    Container(
                      width: 170,
                      height: 40,
                      padding: EdgeInsets.only(top: 10),
                      color: Colors.white,
                      child: Text(
                        '${(widget.userDocument["full_name"] != null) ? widget.userDocument["full_name"] : "Al servidor"}',
                        style: TextStyle(
                          fontSize: 16,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 15),
          Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: Container(
                    child: Text(
                      'PRECIO POR HORA:',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10.0),
                Expanded(
                  flex: 6,
                  child: Container(
                    child: TextFormField(
                      onChanged: (value) {
                        usersReference
                            .document(widget.user.user.uid)
                            .collection('services')
                            .document('meet_service')
                            .setData(
                                {'service_name': 'Conocerme', 'price': value});
                      },
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.done,
                      decoration: new InputDecoration.collapsed(
                        hintText: '\$000.000',
                      ),
                    ),
                    decoration: BoxDecoration(
                      color: Color(0xffffffff),
                    ),
                    padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 218,
                child: Text(
                  'Elegimos sitios seguros para tus reuniones de tiempo libre',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.info_outline,
                  color: HexColor('#0CD52C'),
                ),
                onPressed: () {
                  PopOversAlert().showAlertTitleSubTitleYes(
                      context,
                      _textGlobal.securePlaces,
                      _textGlobal.descriptionPlaces,
                      _textGlobal.aceptar, () {
                    Navigator.of(context).pop();
                  });
                },
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.only(
              left: 10,
              right: 10,
            ),
            child: DropdownButton<Establishment>(
              underline: Text(''),
              isExpanded: true,
              hint: Text('Elegir establecimientos'),
              items: _secureEstablishments.map((Establishment establishment) {
                return DropdownMenuItem<Establishment>(
                  value: establishment,
                  child: Text(
                    establishment.name,
                    textAlign: TextAlign.center,
                  ),
                );
              }).toList(),
              onChanged: (Establishment value) {
                addEstablishment(value);
              },
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Wrap(
            children: renderEstablishments(_secureEstablishmentsAded),
          ),
          Row(
            children: [
              Checkbox(
                value: virtualFreeTime,
                onChanged: (value) => _changeVirtualfreeTime(value),
                checkColor: HexColor('#5EAE57'),
                activeColor: HexColor('#FFFFFF'),
              ),
              Container(
                width: 170,
                child: Text(
                  'Presto el servicio por canal virtual',
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.info_outline,
                  color: HexColor('#0CD52C'),
                ),
                onPressed: () {
                  PopOversAlert().showAlertTitleSubTitleYes(
                      context,
                      _textGlobal.virtualChanel,
                      _textGlobal.descriptionVirtual,
                      _textGlobal.aceptar, () {
                    Navigator.of(context).pop();
                  });
                },
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget getButtonShowServices() {
    return BtnBase(
      color: HexColor('#0CBFD5'),
      texto: 'Ofrecer un servicio TIMER',
      textColor: HexColor('#FFFFFF'),
      ontap: () {
        setState(() {
          isShowServices = true;
        });
      },
    );
  }

  /*
   * Add  Establishments to service.
   */
  void addEstablishment(value) {
    setState(() {
      DocumentReference establishmentRef =
          Firestore.instance.collection('establishments').document(value.id);
      if (_secureEstablishmentsAded
              .map((item) => item.name)
              .toList()
              .indexOf(value.name) ==
          -1) {
        setState(() {
          _secureEstablishmentsAded.add(value);
        });
      }
      usersReference
          .document(widget.user.user.uid)
          .collection('services')
          .document('meet_service')
          .updateData({
        'establishment': FieldValue.arrayUnion([establishmentRef])
      });
    });
  }

  List<Widget> renderEstablishments(categories) {
    return categories.map<Widget>((Establishment establishment) {
      return establishment.renderEstablishment(() {
        DocumentReference establishmentRef = Firestore.instance
            .collection('establishments')
            .document(establishment.id);
        usersReference
            .document(widget.user.user.uid)
            .collection('services')
            .document('meet_service')
            .updateData({
          'establishment': FieldValue.arrayRemove([establishmentRef])
        });
        int indexEstablishment = _secureEstablishmentsAded
            .map((cat) => cat.name)
            .toList()
            .indexOf(establishment.name);
        setState(() {
          _secureEstablishmentsAded.removeAt(indexEstablishment);
        });
      });
    }).toList();
  }

  /*
   * Add service sites
   */
  void addServiceSite() async {
    await usersReference.document(widget.user.user.uid).updateData({
      'services_sites': FieldValue.arrayUnion([_newSite.text])
    });
    setState(() {
      _customEstablishments.add(_newSite.text);
      _newSite.text = '';
    });
  }

  List<Widget> renderCustomeSites() {
    return _customEstablishments.map<Widget>((customSite) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 2),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              border: Border.all(
                color: Colors.black,
                width: 2.0,
              )),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(customSite),
              SizedBox(
                width: 10.0,
              ),
              GestureDetector(
                onTap: () {
                  int indexSite = _customEstablishments
                      .map((site) => site)
                      .toList()
                      .indexOf(customSite);
                  setState(() {
                    _customEstablishments.removeAt(indexSite);
                  });
                  Firestore.instance
                      .collection('users')
                      .document(widget.user.user.uid)
                      .updateData({
                    'services_sites': FieldValue.arrayRemove([customSite])
                  });
                },
                child: Icon(Icons.close),
              )
            ],
          ),
        ),
      );
    }).toList();
  }

  /*
   *  Add custom stie 
   */
  void createUserService() async {
    await usersReference
        .document(widget.user.user.uid)
        .collection('services')
        .document()
        .setData({'service_type': 'professional', 'service_name': ''});
  }

  List<Widget> renderCategories(categories) {
    return categories.map<Widget>((Category category) {
      return category.renderCategory(() {
        int indexCat = _categoriesAdded
            .map((cat) => cat.name)
            .toList()
            .indexOf(category.name);
        setState(() {
          _categoriesAdded.removeAt(indexCat);
        });
      });
    }).toList();
  }

  Widget renderServicesList() {
    return StreamBuilder(
      stream: usersReference
          .document(widget.user.user.uid)
          .collection('services')
          .where('service_type', isEqualTo: 'professional')
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return (snapshot.data != null)
            ? Column(
                children: renderServiceForm(snapshot.data.documents),
              )
            : Container(
                child: Text(''),
              );
      },
    );
  }

  List<Widget> renderServiceForm(services) {
    return services
        .map<Widget>(
          (DocumentSnapshot service) => Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color.fromRGBO(12, 191, 213, 0.2)),
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.only(top: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Tiempo libre profesional',
                          textAlign: TextAlign.left,
                          style: TextStyle(fontSize: 16),
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.info_outline,
                            color: HexColor('#0CD52C'),
                          ),
                          onPressed: () {
                            PopOversAlert().showAlertTitleSubTitleYes(
                                context,
                                _textGlobal.profesionalTimer,
                                _textGlobal.descriptionProfesional,
                                _textGlobal.aceptar, () {
                              Navigator.of(context).pop();
                            });
                          },
                        ),
                      ],
                    ),
                    InkWell(
                      child: Icon(
                        Icons.close,
                        size: 24,
                        color: Colors.white,
                      ),
                      onTap: () {
                        usersReference
                            .document(widget.user.user.uid)
                            .collection('services')
                            .document(service.documentID)
                            .delete();
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 8,
                        child: TextFormField(
                          initialValue: service.data['service_name'],
                          onChanged: (value) {
                            usersReference
                                .document(widget.user.user.uid)
                                .collection('services')
                                .document(service.documentID)
                                .updateData({'service_name': value});
                          },
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.done,
                          decoration: new InputDecoration.collapsed(
                            hintText: 'Ej. Clases de guitarra',
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () {
                            usersReference
                                .document(widget.user.user.uid)
                                .collection('services')
                                .document(service.documentID)
                                .delete();
                          },
                          child: Icon(
                            Icons.close,
                            color: HexColor('#808080'),
                          ),
                        ),
                      )
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Color(0xffffffff)),
                  padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                ),
                SizedBox(height: 10.0),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          '\$ PRECIO',
                          style: TextStyle(fontSize: 16),
                        ),
                        padding: EdgeInsets.only(
                          left: 40,
                        ),
                      ),
                      SizedBox(width: 10.0),
                      Container(
                        width: 120,
                        child: TextFormField(
                          initialValue: service.data['price'],
                          onChanged: (value) {
                            usersReference
                                .document(widget.user.user.uid)
                                .collection('services')
                                .document(service.documentID)
                                .updateData({'price': value});
                          },
                          keyboardType: TextInputType.number,
                          textInputAction: TextInputAction.done,
                          decoration: new InputDecoration.collapsed(
                            hintText: 'Precio por hora',
                          ),
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Color(0xffffffff)),
                        padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        margin: EdgeInsets.only(right: 20),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 218,
                      child: Text(
                        'Elegimos sitios seguros para tus reuniones de tiempo libre',
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.info_outline,
                        color: HexColor('#0CD52C'),
                      ),
                      onPressed: () {
                        PopOversAlert().showAlertTitleSubTitleYes(
                            context,
                            _textGlobal.securePlaces,
                            _textGlobal.descriptionPlaces,
                            _textGlobal.aceptar, () {
                          Navigator.of(context).pop();
                        });
                      },
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.only(
                    left: 10,
                    right: 10,
                  ),
                  child: DropdownButton<Establishment>(
                    underline: Text(''),
                    isExpanded: true,
                    hint: Text('Elegir establecimientos'),
                    items: _secureEstablishments
                        .map((Establishment establishment) {
                      return DropdownMenuItem<Establishment>(
                        value: establishment,
                        child: Text(
                          establishment.name,
                          textAlign: TextAlign.center,
                        ),
                      );
                    }).toList(),
                    onChanged: (Establishment value) {
                      addEstablishment(value);
                    },
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Wrap(
                  children: renderEstablishments(_secureEstablishmentsAded),
                ),
                Row(
                  children: [
                    Checkbox(
                      value: domiciliary,
                      onChanged: (value) => _changeDomiciliary(value),
                      checkColor: HexColor('#5EAE57'),
                      activeColor: HexColor('#FFFFFF'),
                    ),
                    Container(
                      width: 170,
                      child: Text(
                        'Presto el servicio a domicilio',
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Checkbox(
                      value: virtualFreeJob,
                      onChanged: (value) => _changeVirtualfreeJob(value),
                      checkColor: HexColor('#5EAE57'),
                      activeColor: HexColor('#FFFFFF'),
                    ),
                    Container(
                      width: 170,
                      child: Text(
                        'Presto el servicio por canal virtual',
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.info_outline,
                        color: HexColor('#0CD52C'),
                      ),
                      onPressed: () {
                        PopOversAlert().showAlertTitleSubTitleYes(
                            context,
                            _textGlobal.virtualChanel,
                            _textGlobal.descriptionVirtual,
                            _textGlobal.aceptar, () {
                          Navigator.of(context).pop();
                        });
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        )
        .toList();
  }
}
