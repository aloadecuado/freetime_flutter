import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:free_time/src/ui/components/dot.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/providers/user_provider.dart';

import 'StepsForms/step_1.dart';
import 'StepsForms/step_2.dart';
import 'StepsForms/step_3.dart';
import 'StepsForms/step_4.dart';
import 'StepsForms/step_5.dart';
import 'StepsForms/Step5DayOfWeekAndException.dart';

class FreeOfertaPage extends StatefulWidget {
  FreeOfertaPage({Key key}) : super(key: key);

  @override
  _FreeOfertaPageState createState() => _FreeOfertaPageState();
}

class _FreeOfertaPageState extends State<FreeOfertaPage> {
  int _step = 0;

  final _key = GlobalKey<ScaffoldState>();
  var closeProces = false;

  DocumentSnapshot documentSnapshot;

  TextGlobal _textGlobal = TextGlobal();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false, // Don't show the leading button
        elevation: 1.0,
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            (_step == 0)
                ? GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.close,
                        color: HexColor('#0CBFD5'),
                      ),
                    ),
                  )
                : GestureDetector(
                    onTap: () {
                      setState(() {
                        _step--;
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: HexColor('#0CBFD5'),
                      ),
                    ),
                  ),
            Text(
              'Servicios FreeTime',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xff000000),
                fontSize: 16,
              ),
            ),
          ],
        ),
        //title:
      ),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.85,
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        dot(_step, 0),
                        dot(_step, 1),
                        dot(_step, 2),
                        dot(_step, 3),
                        dot(_step, 4),
                        dot(_step, 5)
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              renderSteps(),
            ],
          ),
        ),
      ),
    );
  }

  renderSteps() {
    final user = Provider.of<UserProvider>(context, listen: false);
    switch (_step) {
      case 0:
        return Step1(
          next: () {
            setState(() {
              _step++;
            });
          },
          showError: (message) => showError(message),
          close: () => Navigator.of(context).pop(),
        );
      case 1:
        return Step2(
          next: () {
            setState(() {
              _step++;
            });
          },
          showError: (message) => showError(message),
        );
      case 2:
        return Step3(
            next: (documentSnapshot1) {
              setState(() {
                documentSnapshot = documentSnapshot1;
                _step++;
              });
            },
            user: user,
            showError: (message) => showError(message));
      case 3:
        return Step4(
          next: () {
            setState(() {
              _step++;
            });
          },
          user: user,
          showError: (message) => showError(message),
          userDocument: documentSnapshot,
        );
      case 4:
        return Step5(
            next: () {
// Here you can write your code
              setState(() {
                _step++;
              });
            },
            finish: () {
              Navigator.of(context).pop();
            },
            user: user);
      case 5:
        return Step5DayOfWeekAndException(
            next: () {
// Here you can write your code

              setState(() {
                _step--;
              });
            },
            user: user);
    }
  }

  showError(String message) {
    _key.currentState.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }
}
