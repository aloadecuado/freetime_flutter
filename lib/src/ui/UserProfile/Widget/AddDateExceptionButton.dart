import 'package:flutter/material.dart';

class AddDateExceptionButton extends StatefulWidget {
  double width;
  double height;
  Function(DateTime) onAdd;
  String textAddDate;

  AddDateExceptionButton(
      {Key key,
      @required this.width,
      @required this.height,
      @required this.onAdd,
      @required this.textAddDate});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AddDateExceptionButton();
  }
}

class _AddDateExceptionButton extends State<AddDateExceptionButton> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.only(top: 5.0),
        width: widget.width,
        height: widget.height,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Color.fromARGB(255, 100, 100, 100),
                Color.fromARGB(255, 100, 100, 100)
              ]),
        ),
        child: Column(
          children: <Widget>[
            Icon(Icons.add),
            Text(
              widget.textAddDate,
              style: TextStyle(fontSize: 14, color: Colors.white),
            )
          ],
        ),
      ),
      onTap: () {
        selectDate(context);
      },
    );
  }

  Future selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      firstDate: DateTime.now(),
      lastDate: DateTime(2101),
      initialDate: DateTime.now(),
    );
    widget.onAdd(picked);
  }
}
