import 'package:flutter/material.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/models/EnabledHours.dart';
import 'package:free_time/src/ui/UserProfile/Bloc/UserProfileBloc.dart';
import 'package:free_time/src/widgets/CustomCheckBoxRow.dart';
import 'package:free_time/src/widgets/CustomExclusiveCheckBox.dart';
import 'package:free_time/src/widgets/ExclusiveSelectorButton.dart';

import 'DaySelectForWeek.dart';

class BoxSelectDaysOfWeek extends StatefulWidget{
  List<EnabledHours> listEnabledHours;
  Function(List<EnabledHours>) onSelectEnabledHours;
  bool isEnabled = false;

  BoxSelectDaysOfWeek({Key key, this.listEnabledHours, this.onSelectEnabledHours, this.isEnabled});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return _BoxSelectDaysOfWeek();
  }

}
class _BoxSelectDaysOfWeek extends State<BoxSelectDaysOfWeek>{

  UserProfileBloc userProfileBloc;
  List<String> textButtonsDayForWeek = ["Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"];
  int tagDayOfWeek = 0;
  int tagHourOfDay = -1;

  EnabledHours enabledHoursMo;
  EnabledHours enabledHoursTu;
  EnabledHours enabledHoursWe;
  EnabledHours enabledHoursTh;
  EnabledHours enabledHoursFr;
  EnabledHours enabledHoursSa;
  EnabledHours enabledHoursSu;

  bool isMondayToFriday = false;
  bool isMondayToSaturday = false;
  bool isAllWeek = false;

  int indexDaysDisabled = 6;

  TextGlobal _textGlobal = TextGlobal();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    userProfileBloc = UserProfileBloc();
    if (widget.listEnabledHours == null){
      widget.listEnabledHours = List<EnabledHours>();
      enabledHoursMo = EnabledHours({
        "dayOfWeek":"Monday",
        "hoursForDay":List<int>()

      });

      enabledHoursTu = EnabledHours({
        "dayOfWeek":"Tuesday",
        "hoursForDay":List<int>()

      });

      enabledHoursWe = EnabledHours({
        "dayOfWeek":"Wednesday",
        "hoursForDay":List<int>()

      });

      enabledHoursTh = EnabledHours({
        "dayOfWeek":"Thursday",
        "hoursForDay":List<int>()

      });

      enabledHoursFr = EnabledHours({
        "dayOfWeek":"Friday",
        "hoursForDay":List<int>()

      });

      enabledHoursSa = EnabledHours({
        "dayOfWeek":"Saturday",
        "hoursForDay":List<int>()

      });

      enabledHoursSu = EnabledHours({
        "dayOfWeek":"Sunday",
        "hoursForDay":List<int>()

      });
      widget.listEnabledHours.add(enabledHoursMo);
      widget.listEnabledHours.add(enabledHoursTu);
      widget.listEnabledHours.add(enabledHoursWe);
      widget.listEnabledHours.add(enabledHoursTh);
      widget.listEnabledHours.add(enabledHoursFr);
      widget.listEnabledHours.add(enabledHoursSa);
      widget.listEnabledHours.add(enabledHoursSu);
    }


    return Column(

      children: <Widget>[
        selectExperience(),
        streamSelectDayOfWeek(),

        CustomExclusiveCheckBox(
          defaultTag: -1,
          listCheckBox: [_textGlobal.textSeleccionesDeLunesAViernes, _textGlobal.textSeleccionesDeLunesASabado, _textGlobal.textSeleccioneTodaLaSemana],
          onTapSelect: (state, tag){
            if(!state){
              clearEnabledHour();
            }else{
              switch(tag){
                case 0:
                  selectMondayToFriday();
                  break;

                case 1:
                  selectMondayToSaturday();
                  break;

                case 2:
                  selectMallWeek();
                  break;
              }
            }

            userProfileBloc.selectDayOfWeekSink.add(tagDayOfWeek);
          },
        )
      ],
    );
  }

  Widget selectExperience(){

    return Column(
            children: <Widget>[
              ExclusiveSelectorButton(

                orientation: Axis.horizontal,
                isListView: true,
                width: 60,
                height: 60,
                buttons: textButtonsDayForWeek,
                indexDefaultSelect: 0,
                boxDecorationOn: BoxDecoration(

                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [Color(0xFF00C3D2), Color(0xFF00C3D2)]
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 1.0, // has the effect of softening the shadow
                        spreadRadius: 1.0, // has the effect of extending the shadow
                        offset: Offset(
                          1.0, // horizontal, move right 10
                          1.0, // vertical, move down 10
                        ),
                      )
                    ]
                ),

                boxDecorationOff: BoxDecoration(

                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [Colors.white, Colors.white]
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 1.0, // has the effect of softening the shadow
                        spreadRadius: 1.0, // has the effect of extending the shadow
                        offset: Offset(
                          1.0, // horizontal, move right 10
                          1.0, // vertical, move down 10
                        ),
                      )
                    ]
                ),
                onTap: (tag){

                  tagDayOfWeek = tag;

                  userProfileBloc.selectDayOfWeekSink.add(tagDayOfWeek);
                },

              ),

              /*StreamBuilder(
                    stream: userProfileBloc.selectDayOfWeekStream,
                    builder: (context, snapshot) {
                      return Container(
                        height: 300,
                        child: ListView(
                          children: rowsHour(),
                        ),
                      );
                    }
                  )*/
            ],
          );
  }

  Widget streamSelectDayOfWeek(){
    return StreamBuilder(
        stream: userProfileBloc.selectDayOfWeekStream,
        builder: (context, snapshot) {

          //return Container();
          return Container(
            height: 300,
            child: ListView(
              children: rowsHour(),
            ),
          );
        }
    );
  }

  List<Widget> rowsHour(){

    List<Widget> listsDaysForWeek = List<Widget>();
    for(int i = 0; i < 24; i++){
      var widgett = DaySelectForWeek(

        isEnabled: widget.listEnabledHours[tagDayOfWeek].isPrograming,
        tag: i,
        textHour: "${i}:00 - ${i + 1}:00",
        textDayForWeek: getDayForWeek(i),
        isSelect: (existHourOfDay(i)),
        onSelect: (tag, isSelect){

          tagHourOfDay = tag;
          (isSelect)?addHourOfDay():removeHourOfDay();

          widget.onSelectEnabledHours(widget.listEnabledHours);
        },
      );

      listsDaysForWeek.add(widgett);
    }
    return listsDaysForWeek;
  }

  void addHourOfDay(){
    bool exist = false;
    widget.listEnabledHours[tagDayOfWeek].hoursForDay.forEach((hour){
      if (hour == tagHourOfDay){
        exist = true;

      }
    });

    if(!exist){
      widget.listEnabledHours[tagDayOfWeek].hoursForDay.add(tagHourOfDay);
    }
  }

  void removeHourOfDay(){
    int tag = -1;
    widget.listEnabledHours[tagDayOfWeek].hoursForDay.forEach((hour){
      if (hour == tagHourOfDay){
        tag = tagHourOfDay;

      }
    });

    if(tag >=0){
      widget.listEnabledHours[tagDayOfWeek].hoursForDay.removeAt(tag);
    }
  }

  bool existHourOfDay(int indexHour ){
    bool exist = false;
    widget.listEnabledHours[tagDayOfWeek].hoursForDay.forEach((hour){
      if (hour == indexHour){
        exist = true;

      }
    });

    return exist;
  }

  String getDayForWeek(int numDay){
    switch(numDay){
      case 0:
        return "Lu";
        break;
      case 1:
        return "Ma";
        break;
      case 2:
        return "Mi";
        break;
      case 3:
        return "Ju";
        break;
      case 4:
        return "Vi";
        break;
      case 5:
        return "Sa";
        break;
      case 6:
        return "Do";
        break;
    }
  }



  void selectMondayToFriday(){
    indexDaysDisabled = 4;
    int i = 0;
    widget.listEnabledHours.forEach((enabled){
      if(i >= 1 && i<= indexDaysDisabled){
        enabled.hoursForDay = widget.listEnabledHours[0].hoursForDay;
        enabled.isPrograming = true;
      }
      i++;
    });
  }

  void selectMondayToSaturday(){
    indexDaysDisabled = 5;
    int i = 0;
    widget.listEnabledHours.forEach((enabled){
      if(i >= 1 && i<= indexDaysDisabled){

        enabled.hoursForDay = widget.listEnabledHours[0].hoursForDay;
        enabled.isPrograming = true;
      }
      i++;
    });
  }

  void selectMallWeek(){
    indexDaysDisabled = 6;
    int i = 0;
    widget.listEnabledHours.forEach((enabled){
      if(i >= 1 && i<= indexDaysDisabled){


        enabled.hoursForDay = widget.listEnabledHours[0].hoursForDay;
        enabled.isPrograming = true;
      }
      i++;
    });
  }

  void clearEnabledHour(){
    int i = 0;
    widget.listEnabledHours.forEach((enabled){
      if(i >= 1 && i<= indexDaysDisabled){
        enabled.hoursForDay = List<int>();
        widget.listEnabledHours[0].hoursForDay.forEach((hour){
          enabled.hoursForDay.add(hour);
        });
        enabled.isPrograming = false;
      }

      i++;
    });
  }

}