import 'package:flutter/material.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/ui/UserProfile/Widget/DateSelectWidget.dart';

class DateException extends StatefulWidget {
  double width;
  double height;
  Function(DateTime, int) onAddStartDate;
  Function(DateTime, int) onAddEndDate;
  DateTime startDate;
  DateTime endDate;
  int tag;

  DateException(
      {Key key,
      @required this.width,
      @required this.height,
      @required this.onAddStartDate,
      @required this.onAddEndDate,
      this.startDate,
      this.endDate,
      @required this.tag});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DateException();
  }
}

class _DateException extends State<DateException> {
  TextGlobal _textGlobal = TextGlobal();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: widget.height,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: DateSelectWidget(
              date: widget.startDate,
              textAddDate: _textGlobal.addDate,
              color: Colors.green,
              onSelectDate: (date) {
                setState(() {
                  widget.startDate = date;
                });
                widget.onAddStartDate(widget.startDate, widget.tag);
              },
            ),
          ),
          Expanded(
            flex: 1,
            child: DateSelectWidget(
              date: widget.endDate,
              textAddDate: _textGlobal.addDate,
              color: Colors.red,
              onSelectDate: (date) {
                setState(() {
                  widget.endDate = date;
                });
                widget.onAddEndDate(widget.endDate, widget.tag);
              },
            ),
          )
        ],
      ),
    );

    Container();
  }
}
