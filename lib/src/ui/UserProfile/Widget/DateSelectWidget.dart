import 'package:flutter/material.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:intl/intl.dart';

import 'AddDateExceptionButton.dart';

class DateSelectWidget extends StatefulWidget {
  Color color;
  DateTime date;
  String textAddDate;
  Function(DateTime) onSelectDate;

  DateSelectWidget(
      {Key key,
      @required this.color,
      this.date,
      @required this.onSelectDate,
      @required this.textAddDate});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DateSelectWidget();
  }
}

class _DateSelectWidget extends State<DateSelectWidget> {
  @override
  Widget build(BuildContext context) {
    String dateFormatDayOfWeek = "";
    String dateFormat = "";

    if (widget.date != null) {
      dateFormatDayOfWeek = DateFormat('EEEE').format(widget.date);
      dateFormat = DateFormat("dd-Month-yyyy").format(widget.date);
    }

    // TODO: implement build
    return GestureDetector(
      child: Container(
        height: 60,
        color: Colors.red,
        /*child: Row(
          children: <Widget>[
            Container(
              width: 60,
              height: 60,
              alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  Text(dateFormat.split("-")[0],
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black
                    ),
                  ),
                  Text(dateFormat.split("-")[1],
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black
                    ),
                  ),
                  Text(dateFormat.split("-")[2],
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black
                    ),
                  )
                ],
              ),
            ),

            Container(
              child: Text(dateFormatDayOfWeek,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.white
                  )
              ),
            )
          ],
        ),*/
      ),
      onTap: () {
        selectDate(context, widget.date);
      },
    );
  }

  Future selectDate(BuildContext context, DateTime date) async {
    final DateTime picked = await showDatePicker(
      context: context,
      firstDate: (date != null) ? date : DateTime.now(),
      lastDate: DateTime(2101),
      initialDate: DateTime.now(),
    );
    widget.onSelectDate(picked);
  }
}
