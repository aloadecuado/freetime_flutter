import 'package:flutter/material.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:free_time/src/ui/UserProfile/freetime_oferta_page.dart';
import 'package:free_time/src/ui/pages/profile/centro_ayuda_page.dart';
import 'package:free_time/src/ui/pages/profile/cofiguration_page.dart';

import 'package:free_time/src/ui/pages/profile/historia_servicios_page.dart';
import 'package:free_time/src/ui/pages/profile/my_diary_page.dart';

final List<Map<String, dynamic>> options = [
  {
    "label": "Historial de servicios",
    "icon": Icons.history,
    "widget": HistorialServicio()
  },
  {
    "label": "Configuración",
    "icon": Icons.settings,
    "widget": ConfigurationPage()
  },
  {
    "label": "Ofrecer un servicio Freetime",
    "icon": Icons.star,
    "widget": FreeOfertaPage()
  },
  {
    "label": "Mi agenda",
    "icon": Icons.event_note,
    "widget": DiaryPage(),
  },
  {
    "label": "Centro de ayuda",
    "icon": Icons.help_outline,
    "widget": CentroAyudaPage()
  },
  {
    "label": "Cerrar sesión",
    "icon": Icons.exit_to_app,
  },
];

class BoxprofileMenu extends StatelessWidget {
  BoxprofileMenu({Key key, this.user}) : super(key: key);

  final user;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 5.0,
            offset: Offset(0.0, 1.0),
          )
        ],
      ),
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: List.generate(
          options.length,
          (index) => Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: (index + 1) == options.length
                      ? Colors.white
                      : Theme.of(context).dividerColor,
                ),
              ),
            ),
            child: ListTile(
              onTap: () => onPressOption(options[index]['widget'], context),
              contentPadding: EdgeInsets.fromLTRB(15, 5, 15, 5),
              leading: Icon(
                options[index]['icon'],
                color: Color.fromRGBO(196, 196, 196, 1),
              ),
              trailing: Icon(Icons.keyboard_arrow_right),
              title: Text(options[index]['label']),
            ),
          ),
        ),
      ),
    );
  }

  void onPressOption(Widget widget, BuildContext context) {
    if (widget != null) {
      final route = MaterialPageRoute(
        builder: (context) => widget,
      );
      Navigator.push(context, route);
    } else {
      UserProvider.instance().signOut(context);
    }
  }
}
