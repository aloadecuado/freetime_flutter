import 'dart:async';

import 'package:flutter/material.dart';
import 'package:free_time/src/utils/color.dart';

class DaySelectForWeek extends StatefulWidget {
  String textHour;
  bool isSelect = false;
  Function(int, bool) onSelect;
  String textDayForWeek;
  int tag = 0;
  bool isEnabled = false;

  DaySelectForWeek(
      {Key key,
      this.textDayForWeek,
      this.textHour,
      this.onSelect,
      this.tag,
      this.isSelect,
      this.isEnabled});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DaySelectForWeek();
  }
}

class _DaySelectForWeek extends State<DaySelectForWeek> {
  StreamController<bool> selectdayStreamController = StreamController<bool>();

  Stream<bool> get selectdayStream => selectdayStreamController.stream;
  StreamSink<bool> get selectdaySink => selectdayStreamController.sink;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return StreamBuilder(
        stream: selectdayStream,
        builder: (context, snapshot) {
          /*if(!snapshot.hasData){
          return Container();
        }*/
          if (!snapshot.hasData) {
            //widget.isSelect = false;
          }
          return GestureDetector(
            child: Container(
              height: 60,
              width: 30,
              margin: EdgeInsets.all(5.0),
              decoration: myBoxDecoration(widget.isSelect, widget.isEnabled),
              alignment: Alignment(-0.95, 0),
              //color: Colors.black,//(snapshot.data)?Color(0xFF00C3D2):Colors.white,
              child: Text(
                widget.textHour,
                style: TextStyle(
                  fontSize: 15,
                  color: (widget.isSelect) ? Colors.white : Colors.black,
                ),
              ),
            ),
            onTap: (!widget.isEnabled)
                ? () {
                    widget.isSelect = !widget.isSelect;
                    selectdaySink.add(widget.isSelect);
                    widget.onSelect(widget.tag, widget.isSelect);
                  }
                : () {},
          );
        });
  }

  BoxDecoration myBoxDecoration(bool isSelect, bool isEnabled) {
    return BoxDecoration(
      //border: Border.all(),
      borderRadius: BorderRadius.all(Radius.circular(5.0)),
      color: (!isEnabled)
          ? (isSelect) ? HexColor('#0CBFD5') : Colors.white
          : (isSelect) ? Colors.blueGrey : Colors.grey,
    );
  }
}
