import 'package:flutter/material.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/models/DateExceptionModel.dart';
import 'package:free_time/src/ui/UserProfile/Widget/AddDateExceptionButton.dart';

import 'DateException.dart';

class AddDatesException extends StatefulWidget {
  Function(List<DateExceptionModel>) onDatesException;
  List<DateExceptionModel> datesException;

  AddDatesException(
      {Key key,
      @required this.onDatesException,
      @required this.datesException});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AddDatesException();
  }
}

class _AddDatesException extends State<AddDatesException> {
  TextGlobal _textGlobal = TextGlobal();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Row(
          children: getListDates(),
          /*children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                height: 60,
                color: Colors.red,
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                height: 60,
                color: Colors.green,
              ),
            )
          ],*/
        ),
        AddDateExceptionButton(
          height: 60,
          textAddDate: _textGlobal.addDate,
          onAdd: (date) {
            setState(() {
              addFirstDates(date);
            });
          },
        )
      ],
    );
  }

  List<Widget> getListDates() {
    List<Widget> widgets = List<Widget>();

    widget.datesException.forEach((ddateException) {
      var dateException = DateException(
        height: 60,
        onAddEndDate: (endDate, tag) {
          setState(() {
            addEndDates(endDate, tag);
          });

          widget.onDatesException(widget.datesException);
        },
        onAddStartDate: (startDate, tag) {
          setState(() {
            addStartDates(startDate, tag);
          });
          widget.onDatesException(widget.datesException);
        },
        endDate: ddateException.endDate,
        startDate: ddateException.startDate,
      );
      widgets.add(dateException);
    });

    return widgets;
  }

  void addFirstDates(DateTime date) {
    var dateException = DateExceptionModel();
    dateException.startDate = date;
    dateException.endDate = date;
    dateException.active = true;

    widget.datesException.add(dateException);
  }

  void addStartDates(DateTime date, int tag) {
    widget.datesException[tag].startDate = date;
  }

  void addEndDates(DateTime date, int tag) {
    widget.datesException[tag].endDate = date;
  }
}
