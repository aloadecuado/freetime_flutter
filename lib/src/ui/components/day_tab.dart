import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class DayTab extends StatefulWidget {
  DayTab({ Key key, this.user, this.day,}) : super(key: key);

  final user;
  final String  day;

  @override
  _DayTabState createState() => _DayTabState();
}

class _DayTabState extends State<DayTab> {

  TimeOfDay _startAt, _endAt;
  ValueChanged<TimeOfDay> selectTime;

  @override
  void initState() {
    _startAt = new TimeOfDay(hour: 12, minute: 30);
    _endAt = new TimeOfDay(hour: 12, minute: 30);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Text('Selecciona el horario de tiempo libre.'),
          SizedBox(height: 20.0),
          Row(
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                  child: Text('Desde'),
                  decoration: BoxDecoration (
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Color(0xff9AE6F5)
                  ),
                  padding: EdgeInsets.all(10.0),
                ),
              ),
              SizedBox(width: 10.0),
              Expanded(
                flex: 6,
                child: Container(
                  child: GestureDetector(
                    child: Text('${_startAt.format(context)}hrs', style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),),
                    onTap: () {
                      _selectTime(context, 'startAt');
                    },
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Color(0xfff7f7f7)
                  ),
                  padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                ),
              )
            ],              
          ),
          SizedBox(height: 20.0,),
          Row(
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                  child: Text('Hasta'),
                  decoration: BoxDecoration (
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Color(0xff9AE6F5)
                  ),
                  padding: EdgeInsets.all(10.0),
                ),
              ),
              SizedBox(width: 10.0),
              Expanded(
                flex: 6,
                child: Container(
                  child: GestureDetector(
                    child: Text('${_endAt.format(context)}hrs', style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),),
                    onTap: () {
                      _selectTime(context, 'endAt');
                    },
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Color(0xfff7f7f7)
                  ),
                  padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                ),
              )
            ],              
          )
        ],
      ),
    );
  }

  Future<void> _selectTime(BuildContext context, type) async {
    final TimeOfDay picked =  await showTimePicker(
      context: context, 
      initialTime: _startAt
    );
    setState(() {
      if ( type == 'startAt' ){
        _startAt = picked  ?? new TimeOfDay(hour: 12, minute: 30);
      } else {
        _endAt = picked  ?? new TimeOfDay(hour: 12, minute: 30);
      }
      
      Firestore.instance.collection('users').document(widget.user.user.uid).updateData({
        'daysAvailable.${widget.day}.${type}': picked.format(context),
      });

    });
  }
}