import "package:flutter/material.dart";
import 'package:free_time/src/models/BannerModel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:free_time/src/ui/components/activityIndicator.dart';

class CustomBanner extends StatelessWidget {
  final double widthBanner;
  final BannerModel bannerModel;
  final Function(String) onSendSite;

  const CustomBanner({
    Key key,
    this.onSendSite,
    this.widthBanner,
    this.bannerModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      width: widthBanner,
      child: GestureDetector(
        onTap: () => onSendSite(bannerModel.urlSite),
        child: CachedNetworkImage(
          imageUrl: bannerModel.urlImage,
          placeholder: (context, url) => ActivityIndicator(),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
      ),
    );
  }
}
