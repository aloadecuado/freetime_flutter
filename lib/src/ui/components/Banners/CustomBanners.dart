import 'package:url_launcher/url_launcher.dart';

import 'CustomBanner.dart';
import "package:flutter/material.dart";
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:free_time/src/models/BannerModel.dart';

class CustomBanners extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CustomBanners();
}

class _CustomBanners extends State<CustomBanners> {
  List<Widget> bannerWidegts = List<Widget>();
  List<BannerModel> listBanners = List<BannerModel>();

  @override
  void initState() {
    getBanners();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height * 0.15;

    return Container(
      height: height,
      margin: EdgeInsets.only(top: 5, bottom: 20),
      child: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: generateBanners(),
      ),
    );
  }

  List<Widget> generateBanners() {
    double width = MediaQuery.of(context).size.width * 0.7;
    bannerWidegts = List<CustomBanner>();
    listBanners.forEach((item) {
      CustomBanner customBanner = CustomBanner(
        bannerModel: item,
        onSendSite: _launchURL,
        widthBanner: width,
      );
      bannerWidegts.add(customBanner);
    });

    return bannerWidegts;
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void getBanners() async {
    Firestore.instance.collection('banners').getDocuments().then((banners) {
      List<BannerModel> bannerModelResponse = List<BannerModel>();
      banners.documents.forEach((banner) {
        BannerModel bannerModel = BannerModel(banner.data);
        bannerModelResponse.add(bannerModel);
      });
      setState(() {
        listBanners = bannerModelResponse;
      });
    });
  }
}
