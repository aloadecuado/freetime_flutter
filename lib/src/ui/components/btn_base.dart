import 'package:flutter/material.dart';

class BtnBase extends StatelessWidget {
  final String texto;
  final Function ontap;
  final Color color;
  final Color textColor;

  const BtnBase({Key key, this.texto, this.ontap, this.color, this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      color: color,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Text(
          texto,
          style: TextStyle(
            color: textColor != null ? textColor : Colors.white,
            fontWeight: FontWeight.w800,
            fontSize: 18,
          ),
        ),
      ),
      onPressed: ontap,
    );
  }
}
