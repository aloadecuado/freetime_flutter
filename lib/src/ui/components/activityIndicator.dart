import 'package:flutter/material.dart';

class ActivityIndicator extends StatelessWidget {
  final double width;
  final double height;

  const ActivityIndicator({
    Key key,
    this.width = 10,
    this.height = 10,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      alignment: Alignment.center,
      child: CircularProgressIndicator(
        strokeWidth: 3.0,
        valueColor: AlwaysStoppedAnimation(
          Colors.grey.shade300,
        ),
      ),
    );
  }
}
