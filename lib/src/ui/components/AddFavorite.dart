import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:meta/meta.dart';

class AddFvorite extends StatefulWidget {
  String textFav;
  bool isFav;
  Function(bool) onAddFav;
  DocumentSnapshot currenUser;
  DocumentSnapshot serviceUser;

  AddFvorite({this.textFav, this.isFav, this.onAddFav});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AddFvorite();
  }
}

class _AddFvorite extends State<AddFvorite> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: 70,
      height: 35,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            HexColor('#00C3D2'),
            HexColor('#BE7CF6'),
          ],
          begin: FractionalOffset.bottomLeft,
          end: FractionalOffset.bottomRight,
        ),
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10.0),
            bottomRight: Radius.circular(10.0),
            topLeft: Radius.circular(10.0),
            topRight: Radius.circular(10.0)),
      ),
      child: Row(
        children: <Widget>[
          Container(
              padding: EdgeInsets.only(
                right: 10,
              ),
              width: 35,
              height: 35,
              alignment: Alignment.center,
              child: IconButton(
                onPressed: () {
                  widget.onAddFav(widget.isFav);
                },
                icon: Icon(
                  (!widget.isFav) ? Icons.favorite_border : Icons.favorite,
                  color: Colors.white,
                ),
              ) /*GestureDetector(
              onTapUp: widget.onAddFav(widget.isFav),
              child: Icon((!widget.isFav)?Icons.favorite_border:Icons.favorite),

            ),*/
              ),
          Container(
            width: 35,
            height: 25,
            alignment: Alignment.center,
            child: Text(
              widget.textFav,
              style: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.bold
              ),
            ),
          )
        ],
      ),
    );
  }
}
