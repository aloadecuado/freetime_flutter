import 'package:flutter/material.dart';
import 'package:free_time/src/utils/color.dart';

Widget dot(step, index){

  var item = index;

  return Container(
    width: 10.0,
    height: 10.0,
    decoration: BoxDecoration(
      color: (step == item)?HexColor('#C4C4C4'):Colors.white,
      borderRadius: BorderRadius.circular(50.0),
      border: Border.all(
        color: HexColor('#C4C4C4'),
        width: 2.0
      )
    ),
  );
}