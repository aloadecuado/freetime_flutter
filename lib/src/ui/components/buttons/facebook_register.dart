import 'package:flutter/material.dart';
import 'package:free_time/src/Repository/Repository/user_auth_respository.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:provider/provider.dart';

class FacebookRegisterButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: RaisedButton(
        color: Color(0xff2672CB),
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/social/facebook.png",
              color: Colors.white,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              "Ingresa con Facebook",
              style: TextStyle(letterSpacing: 1.5, fontWeight: FontWeight.w500),
            ),
          ],
        ),
        onPressed: () {
          Provider.of<UserProvider>(context).signInWithFacebook(context);
        },
      ),
    );
  }
}
