import 'package:flutter/material.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:provider/provider.dart';

class GoogleRegisterButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: RaisedButton(
        color: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/social/google.png"),
            SizedBox(
              width: 10,
            ),
            Text(
              "Ingresa con Google",
              style: TextStyle(letterSpacing: 1.5, fontWeight: FontWeight.w500),
            ),
          ],
        ),
        onPressed: () {
          Provider.of<UserProvider>(context).signInWithGoogle(context);
        },
      ),
    );
  }
}
