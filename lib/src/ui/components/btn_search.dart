import 'package:flutter/material.dart';
import 'package:free_time/src/config.dart';

class BtnSearch extends StatelessWidget {
  final Function onTap;
  final String label;
  final Color color;

  const BtnSearch({Key key, this.onTap, this.label, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 50.0,
        margin: const EdgeInsets.symmetric(horizontal: 15),
        padding: const EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.grey[300]),
          borderRadius: BorderRadius.circular(10.0),
          color: MyColors.backgroundSearch,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.search,
              color: Colors.grey[300],
              size: 26.0,
            ),
            SizedBox(width: 10),
            Text(
              'Buscar',
              style: TextStyle(
                color: Colors.grey[400],
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            )
          ],
        ),
      ),
    );
  }
}
