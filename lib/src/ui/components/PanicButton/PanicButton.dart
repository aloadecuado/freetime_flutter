import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class PanicButton extends StatefulWidget {
  Function() onGetLongPressed;
  String buttonText;
  double width;
  PanicButton(
      {@required this.onGetLongPressed,
      @required this.buttonText,
      @required this.width});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PanicButton();
  }
}

class _PanicButton extends State<PanicButton> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 50,
      width: widget.width,
      margin: EdgeInsets.only(bottom: 20),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        color: Color.fromRGBO(228, 0, 0, 1),
      ),
      child: GestureDetector(
        onTap: () {
          widget.onGetLongPressed();
        },
        child: Text(
          widget.buttonText,
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
