import 'package:flutter/material.dart';
import 'package:free_time/src/config.dart';
import 'package:free_time/src/ui/components/Banners/CustomBanners.dart';

import 'package:free_time/src/ui/components/btn_search.dart';
import 'package:free_time/src/widgets/app_bar_home.dart';

import 'package:free_time/src/widgets/box_category_home.dart';

import 'package:free_time/src/ui/Loby/Widgets/box_top_home.dart';
import 'package:free_time/src/widgets/favorite_home.dart';

import '../pages/filters_result.dart';

class HomeDosPage extends StatelessWidget {
  final user;

  const HomeDosPage({
    Key key,
    this.user,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarHome(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            BtnSearch(
              onTap: () {
                changeScreen(context, FilterResult());
              },
            ),
            SizedBox(height: 30),
            CustomBanners(),
            SizedBox(height: 10.0),
            BoxtopHome(),
            SizedBox(height: 20.0),
            BoxcategoryHome(
              user: user,
            ),
            SizedBox(height: 10.0),
            FavorieHome(user: user),
          ],
        ),
      ),
    );
  }
}
