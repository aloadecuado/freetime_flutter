import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/ui/pages/favorite_detail.dart';
import 'package:free_time/src/ui/pages/profile_user_page.dart';
import 'package:free_time/src/ui/pages/user_profile.dart';

import '../../../config.dart';

class BoxTopCard extends StatelessWidget {
  BoxTopCard(this._texto, this._imageUrl,
      {this.listUsers, this.user, this.currUser});
  final String _imageUrl;
  final String _texto;
  final List<DocumentSnapshot> listUsers;
  final user;
  final currUser;

  @override
  Widget build(BuildContext context) {
    return Container(
      //padding: EdgeInsets.only(left: 10.0),
      child: GestureDetector(
        child: _crearContenedor(),
        onTap: () {
          /*Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => FavoriteDetail(listUsers: listUsers, user: user, currUser: currUser) )
          );*/

          changeScreen(context, UserProfile(user: user));
        },
      ),
    );
  }

  Widget _crearContenedor() {
    return Center(
      child: SizedBox(
        width: 120,
        height: 160,
        child: Container(
          margin: EdgeInsets.only(left: 15),
          /* decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),*/
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
            clipBehavior: Clip.antiAlias,
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                image: NetworkImage(_imageUrl != null
                    ? _imageUrl
                    : "https://via.placeholder.com/100x100/F7099D/F7099D"), //NetworkImage(_imageUrl),
                fit: BoxFit.cover,
              )),
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: [
                      Color.fromARGB(100, 0, 0, 0),
                      Color.fromARGB(0, 0, 0, 0),
                    ],
                    tileMode: TileMode
                        .repeated, // repeats the gradient over the canvas
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[_mensaje()],
                ),
              ),
            ),
            //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))
          ),
        ),
      ),
    );
  }

  Widget _mensaje() {
    return Center(
      child: Container(
        width: 85,
        alignment: Alignment.center,
        margin: EdgeInsets.only(
          bottom: 10,
        ),
        child: Text(
          _texto,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
