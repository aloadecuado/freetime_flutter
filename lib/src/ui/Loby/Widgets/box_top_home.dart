import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free_time/src/providers/user_provider.dart';
import 'package:provider/provider.dart';
import 'box_top_.dart';

class BoxtopHome extends StatefulWidget {
  BoxtopHome({Key key}) : super(key: key);

  @override
  _BoxtopHomeState createState() => _BoxtopHomeState();
}

class _BoxtopHomeState extends State<BoxtopHome> {
  List<DocumentSnapshot> _top10 = [];

  @override
  initState() {
    super.initState();
    getTop();
  }

  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 20.0),
          child: Container(
            child: Text(
              'Top 10',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 5.0,
        ),
        _listCont(),
      ],
    );
  }

  Widget _listCont() {
    var currUser = Provider.of<UserProvider>(context);
    return Container(
      height: 160,
      margin: EdgeInsets.only(right: 15),
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: _top10.map<Widget>((top) {
          if (top.data['categories'] != null) {
            return FutureBuilder(
              future: top.data['categories'][0].get(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                return (snapshot.connectionState == ConnectionState.done)
                    ? BoxTopCard(
                        snapshot.data['name'],
                        top.data['image'],
                        listUsers: _top10,
                        user: top,
                        currUser: currUser,
                      )
                    : BoxTopCard(
                        '',
                        top.data['image'],
                        listUsers: _top10,
                        user: top,
                        currUser: currUser,
                      );
              },
            );
          } else {
            return BoxTopCard(
              '',
              top.data['image'],
              listUsers: _top10,
              user: top,
            );
          }
        }).toList(),
      ),
    );
  }

  getTop() async {
    var result =
        await Firestore.instance.collection('users').limit(10).getDocuments();
    setState(() {
      _top10 = result.documents;
    });
  }
}
