import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:free_time/src/ui/pages/profile/historia_servicios_page.dart';

class MyColors {
  static const pink = const Color.fromRGBO(232, 41, 170, 1);
  static const grayLight = const Color.fromRGBO(242, 242, 242, 1);
  static const tealLight = const Color.fromRGBO(64, 181, 233, 0.7);
  static const teal = const Color(0xFF64FFDA);
  static const gray = const Color(0xffEFEFF4);
  static const blue = const Color(0xff1585FF);
  static const write = const Color.fromRGBO(255, 255, 255, 1.0);
  static const backgroundSearch = const Color(0xFFF8F8F8);
  static const primaryColor = const Color(0xFF00C3D2);
}

// methods
void changeScreen(BuildContext context, Widget widget) {
  Navigator.push(context, MaterialPageRoute(builder: (context) => widget));
}

void changeScreenReplacement(BuildContext context, Widget widget) {
  Navigator.pushAndRemoveUntil(context,
      MaterialPageRoute(builder: (context) => widget), (settings) => false);
}
