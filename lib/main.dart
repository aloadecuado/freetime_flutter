import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:free_time/src/Values/texts_globals.dart';
import 'package:free_time/src/ui/components/custom_dialog.dart';
import 'package:free_time/src/ui/pages/service_detail.dart';
import 'package:free_time/src/utils/color.dart';
import 'package:provider/provider.dart';

import 'src/config.dart';
import 'src/providers/user_provider.dart';
import 'src/ui/pages/home_page.dart';
import 'src/ui/pages/login_page.dart';

class MessageHandler extends StatefulWidget {
  @override
  _MessageHandlerState createState() => _MessageHandlerState();
}

class _MessageHandlerState extends State<MessageHandler> {
  final Firestore _db = Firestore.instance;
  final FirebaseMessaging _fcm = FirebaseMessaging();
  StreamSubscription iosSubscription;

  TextGlobal _textGlobal = TextGlobal();
  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) {
      iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
        // save the token  OR subscribe to a topic here
      });

      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        if (message["data"]['TYPE'] == _textGlobal.SERVICE_RESUEST) {
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (_) {
                final user = Provider.of<UserProvider>(_);
                return CustomAlertDialog(
                  contentPadding: EdgeInsets.all(0.0),
                  content: Container(
                    height: 350.0,
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          height: 200.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/background/modal.png'),
                                  fit: BoxFit.cover),
                              borderRadius: BorderRadius.circular(20.0)),
                          child: Padding(
                              padding: EdgeInsets.all(20.0),
                              child: Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Text(
                                    'Envío de solicitud',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              )),
                        ),
                        Container(
                          child: Positioned(
                            top: 100.0,
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.8,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 30.0,
                                    ),
                                    Text(
                                      'Buenas noticias',
                                      style: TextStyle(
                                        fontSize: 16.0,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Text(
                                      'Haz recibido una solicitud de servicio',
                                      style: TextStyle(
                                        fontSize: 16.0,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Text(
                                      'Revisa el detalle de la solicitud.',
                                      style: TextStyle(
                                        fontSize: 16.0,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(
                                      height: 20.0,
                                    ),
                                    FlatButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0)),
                                      color: HexColor('#FF62A5'),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                        changeScreen(
                                            context,
                                            ServiceDetail(
                                                serviceId: message['data']
                                                    ['body']));
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16.0, vertical: 12.0),
                                        child: Text(
                                          'Ver Detalle',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17.0),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              });
        }
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO: optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO: optional
      },
    );
    _saveDeviceToken();
  }

  _saveDeviceToken() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    if (user != null) {
      // Get the current user

      // Get the token for this device
      String fcmToken = await _fcm.getToken();

      // Save it to Firestore
      if (fcmToken != null) {
        await _db
            .collection('users')
            .document(user.uid)
            .updateData({'token': fcmToken});
        // .collection('tokens')
        // .document(fcmToken);

        /* await tokens.setData({
          'token': fcmToken,
          'createdAt': FieldValue.serverTimestamp(), // optional
          'platform': Platform.operatingSystem // optional
        }); */
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScreensController();
  }
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(ChangeNotifierProvider.value(
    value: UserProvider.instance(),
    child: MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'), // English
        const Locale('es'), // Hebrew
        // Chinese *See Advanced Locales below*
        // ... other locales the app supports
      ],
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: MyColors.primaryColor,
        accentColor: MyColors.teal,
        fontFamily: "OpenSans",
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: MyColors.primaryColor,
          ),
        ),
      ),
      home: MessageHandler(),
    ),
  ));
}

class ScreensController extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, UserProvider user, _) {
        switch (user.status) {
          case Status.Uninitialized:
          case Status.Authenticating:
            return LoginPage();
          case Status.Authenticated:
            return HomePage(user: user);
          default:
            return LoginPage();
        }
      },
    );
  }
}